@echo off

cd tools
copy /Y ..\src\grammar.cxx .\
copy /Y ..\src\syntax.cxx .\
flex.exe -i -osyntax.c syntax.cxx
bison.exe -d -v -y -o grammar.c grammar.cxx
copy /Y syntax.c ..\src\
copy /Y grammar.c ..\src\
copy /Y grammar.h ..\src\
del grammar.c grammar.h syntax.c grammar.cxx syntax.cxx
cd ..
pause