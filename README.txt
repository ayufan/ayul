ayul

Jest to j�zyk skryptowy napisany na zaliczenie drugiego projektu z programowania strukturalnego w C. Kod zosta� wst�pnie przetestowany, ale oczywi�cie zabiera b��dy, to co najbardziej tu kuleje to dokumentacja, kt�ra jest bardzo powierzchowna, dlatego jak by si� kto� podj�� jej rozszerzenia, ew. napisania od nowa to bylbym bardzo wdzi�czny.

Struktura plik�w/katalog�w:

bin - pliki wykonywalne
doc - dokumentacja
scripts - przyk�adowe skrypty napisane w ayul
share - co� dla bizona
src - kod �r�d�owy j�zyka skryptowego wraz z �rodowiskiem
tools - narz�dzia potrzebne przy kompilacji �r�de�
ayul.doxy - plik konfiguracyjny do doxygena
ayul.h - g��wny plik nag��wkowy j�zyka, kt�ry udost�pnia api
ayul.sln - g��wny plik projektu dla MSVC++ 2005
bison.bat - skrypt bat do budowania lexera i parsera u�ywaj�c flexa i bisona
build.sh - skrypt bashowski do kompilowania pod *unixem
doc.bat - skrypt bat do budowania dokumentacji przy u�yciu doxygena
LICENSE.txt - licencja GPL
README.txt - to co czytasz

Autor:
ayufan
mail://ayufan(at)o2(dot)pl
http://ayufan.qx.pl
gg://9280028

Pisane w:
Microsoft Visual Studio 2005 Professional

Uruchomione na:
Windows XP HE
Solaris
Slackware 10.2

Czas:
3 tyg lu�nego pisania, a tak naprawd� wysz�o 1,5 tyg ostrego :)

Licencja:
GPL - (see LICENSE.txt)

Podzi�kowania:
w sumie narazie nie mam komu dzi�kowa� :P no mo�e Wachowi za pare "rozszerze�"

Pro�by:
Dokumentacja, dokumentacja, dokumentacja :) prosz� niech kto� j� za mnie napisze :P pomog� we wszystkim

Uwaga:
Kod nie jest optymalny, wiele mo�na w nim poprawi�, ale mi si� niechcia�o, jak kto� si� zainteresuje j�zykiem, to naniose odpowiednie poprawki w celu zwi�kszenia wydajno�ci :)

That's all !
ayufan

