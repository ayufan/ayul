//------------------------------------//
//
// ayul.h
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-1
//
//------------------------------------//

#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <assert.h>
#include <math.h>
#include <time.h>

//------------------------------------//
// Documentation

/*! \mainpage Strona g��wna

		\ref intro
		\ref lang
*/

/*!	\page intro Wst�p

		Powsta�o wiele r�znych j�zyk�w. 
		<b>ayul</b> jest kolejnym z nich, ale wprowadza istotne ulepszenia do programowania w C/C++, czy Delphi. 
		J�zyk w odr�nienu od wcze�niej wymienionych wykorzystuje tzw. system "mi�kkich typ�w". 

		/section Mi�kkie typy

		Wbudowane w j�zyk typy to warto�� logiczna (boolean, #AL_LITERAL_BOOLEAN), warto�� ca�kowita (integer, #AL_LITERAL_INTEGER), warto�� zmiennoprzecinkowa (float, #AL_LITERAL_VALUE), ci�g znak�w (string, #AL_LITERAL_STRING), tablica (#AL_LITERAL_TABLE), wska�nik (#AL_LITERAL_POINTER), definicja (#AL_LITERAL_DEF). Wszystkie warto�� s� automatycznie przystosowane do kontekstu wyra�enia z uwzgl�dnieniem prioritet�w operator�w:
		- ci�g znak�w
		- warto�� zmiennoprzecinkowa
		- warto�� ca�kowita
		- warto�� logiczna
*/

/*! \page lang J�zyk
		\section acc Specyfikator zasi�gu

		Wyst�puj� dwa specyfikatory zasi�gu <b>local</b> i <b>global</b>:
		- <b>local</b> - definicja jest dost�pna tylko w aktualnym kontekscie i w wszystkich pod sekcjach danej sekcji
		- <b>global</b> - definicja jest dost�pna we wszystkich kontekstach i we wszystkich sekcjach

		\section defvar Definicje zmiennych

		<\ref acc> <Nazwa 1 zmiennej> <, Nazwa n-tej zmiennej> [= <Warto�� dla n-zmiennych>]<br>
		W przypadku zmiennych <b>local</b> oznacza, �e zmienna jest dost�pna tylko w danej funkcji, <b>global</b> jest dost�pna we wszystkich funkcjach zdefiniowanych w danej funkcji
		\subsection defvarnp Przyk�ady
		- <i>local PrzykladowaZmienna = 10</i>
		- <i>global ZmiennaGlobalna</i>
		- <i>local PierwszaZmienna, DrugaZmienna</i>
		- <i>local PierwszaZmienna, DrugaZmienna = 10</i> (z�e przypisanie, poniewa� wymagane jest przypisanie dw�ch warto�ci)
		- <i>local PierwszaZmienna, DrugaZmienna = (10, 20)</i> (prawid�owe przypisanie)
		
		\section deffunc Definicje funkcji

		<\ref acc> [Opcjonalna nazwa funkcji] ( [Opcjonalne parametry oddzielone przecinkiem] ) { [Cia�o funkcji] }<br>
		\subsection deffuncnp Przyk�ady
		- <i>local MojaFunkcja(){ }</i>
		- <i>local MojaFunkcja2(a, b, c){ }</i>
		- <i>local MojaZmiennaNaFunkcje = local(a, b, c){ }</i>

		\section instr Instrukcja
		\subsection while P�tla While
		while <warunek> <instrukcje><br>
		instrukcje s� wykonywana dopuki doputy prawdziwy jest warunek<br>
		- warunek = \ref expr
		- instrukcje = \ref instr
		\subsubsection instrwhilenp Przyk�ady
		- <i>while(i < 10) { i-- }</i>
		- <i>while a = JakasFunkcja() { CosCoZmienniCosWNaszejFunkcji = 9 }</i>

		\subsection dowhile P�tla Do...While
		do <instrukcje> while <warunek><br>
		najpierw jest wykonywana instrukcja, nast�pnie sprawdzany jest warunek i dopuki ten warunek jest prawdziwy wykonywana jest instrukcja
		- warunek = \ref expr
		- instrukcje = \ref instr
		\subsection instrdowhilenp Przyk�ady
		- <i>do i-- while i</i>
		- <i>do i+="a" while (#i < 10)</i>

		\subsection for P�tla For
		for ( <inicjalizacja> ; <warunek> ; <nastepnik> ) <instrukcje><br>
		najpierw jest wykonywana inicjalizacja p�tli, nast�pnie dopuki prawdziwy jest warunek wykonywane s� instrukcje, a po instrukcjach nast�pnik
		- inicjalizacja = \ref expr
		- warunek = \ref expr
		- nastepnik = \ref expr
		- instrukcje = \ref instr
		\subsubsection instrfornp Przyk�ady
		- <i>for(local i = 0; i < 10; i++) print(i, "\n")</i>
		- <i>for( ; i > 5 ; ) i = MojaFunkcja()</i>

		\subsection foreach P�tla ForEach
		foreach (<wyrazenietablicowe> as <wartosc>|(<klucz>, <wartosc>)) <instrukcje><br>
		dla ka�dego elementu tablicy wykonywane s� instrukcje z przypisanymi warto�ciami klucz i wartosc
		- wyrazenietablicowe = \ref expr
		- klucz = \ref expr
		- wartosc = \ref expr
		- instrukcje = \ref instr
		\subsubsection instrforeachnp Przyk�ady
		- <i>foreach(tablica as local wartosc) print("Wartosc: ", wartosc, "\n")</i>
		- <i>foreach(tablica as (local klucz, wartosc)) print("Tablica[", klucz, "] = ", wartosc, "\n")

		\subsection if Warunek If
		if <warunek> <prawda> [else <falsz>]<br>
		najpierw sprawdzana jest wartosc warunku, je�li ten jest prawdziwy wykonywane s� instrukcje prawdy, je�li fa�szywy instrukcje fa�szu
		- warunek = \ref expr
		- prawda = \ref instr
		- falsz = \ref instr

		\subsection expr Wyra�enia
		\subsubsection opmath Operacje matematyczne
		- operator + (dodwanie) /see alLiteralAdd
		- operator - (odejmowanie) /see alLiteralSub
		- operator * (mno�enie) /see alLiteralMul
		- operator / (dzielenie) /see alLiteralDiv
		- operator % (reszta z dzielenia) /see alLiteralMod
		- operator ^ (pot�ga) /see alLiteralPow

		\subsubsection opass Operacje przypisania
		- operator = (przypisanie)
		- operator += (przypisanie z dodaniem) /see alLiteralAdd
		- operator -= (przypisanie z odejmowaniem) /see alLiteralSub
		- operator *= (przypisanie z mno�eniem) /see alLiteralMul
		- operator /= (przypisanie z dzieleniem) /see alLiteralDiv
		- operator %= (przypisanie reszty z dzielenia) /see alLiteralMod
		- operator ^= (przypisanie z pot�gowaniem) /see alLiteralPow

		\subsubsection opcmp Operacje por�wnania
		- operator == (warto�ci s� r�wne) /see alLiteralEq
		- operator != (warto�ci s� r�ne) /see alLiteralNEq
		- operator < (warto�� jest mniejsza) /see alLiteralLess
		- operator <= (warto�� jest mniejsza b�d� r�wna) /see alLiteralLessEq
		- operator > (warto�� jest wi�ksza) /see alLiteralGr
		- operator >= (warto�� jest wi�ksza b�d� r�wna) /see alLiteralGrEq
		- operator === (r�wne co do typu i warto�ci) /see alLiteralEqq
		- operator && (logiczna koniukcja)
		- operator || (logiczna alternatywa)

		\subsubsection exprtab Wyra�enia tablicowe
		<tablica> [ <klucz> ]<br>
		<tablica> . <klucz, ale jako identyfikator><br>
		spowoduje pobranie warto�ci w podanej kom�rce tablicy, w przypadku, gdy taki klucz nie istnieje, spowoduje to utworzenie go, w przypadku gdy tablica nie istnieje i jest mo�liwe jej utworzenie spowoduje to jej utworzenie
		- tablica = /ref expr
		- klucz = /ref expr

		\subsubsection cb continue, break
		continue - spowoduje wywo�anie nast�pnej iteracji p�tli<br>
		break - spowoduje wyj�cie z p�tli

		\subsubsection ret return
		return \ref expr - spowoduje zwr�cenie przez funkcje warto�ci wyra�enia
		return ; - spowoduje zako�czenie wywo�ywania funkcji bez zwr�conych warto�ci

		\subsubsection concatexpr ��czenie wyra�e�
		<wyrazenie1>, <wyrazenie2>, ..., <n-te wyrazenie> - spowoduje po��czenie wynik�w wszystkich wyra�e�

		\subsubsection callexpr Wywo�ywanie funkcji
		<wyrazenie> ( [parametry] ) - spowoduje wywo�anie funkcji okre�lonej przez <wyra�enie> z <parametry>
		- wyrazenie = \ref expr
		- parametr = \ref expr

		\subsubsection unary Operatory unarne
		! \ref expr - spowoduje zanegowanie wyra�enia<br>
		++ \ref expr - spowoduje zwi�kszenie warto�ci wyra�enia o 1<br>
		-- \ref expr - spowoduje zmniejszenie warto�ci wyra�enia o 1<br>
		+ \ref expr - spowoduje zwr�cenie warto�ci wyra�enia<br>
		- \ref expr - spowoduje odwr�cenie warto�ci wyra�enia<br>

		\subsubsection post Operatory post
		\ref expr ++ - spowoduje zwi�kszenie warto�ci wyra�enia o 1 i zwrocenie poprzedniej wartosci wyra�enia<br>
		\ref expr -- - spowoduje zmniejszenie warto�ci wyra�enia o 1 i zwrocenie poprzedniej wartosci wyra�enia<br>

		\subsubsection tri Operatory tr�jargumentowe
		<warunek> ? <prawda> : <falsz> - /see if, tylko �e zwraca warto�� wyra�enia

		\section Prioritety operator�w
		- \ref post
		- \ref unary
		- \ref opmath, tylko �e % i ^
		- \ref opmath, tylko �e * i /
		- \ref opcmp, bez && i ||
		- \ref opcmp, tylko &&
		- \ref opcmp, tylko ||
		- \ref tri
		- \ref opass
		- \ref concatexpr

		\section literal Litera�y
		- true/false - warto�� logiczna
		- [0-9]+ - warto�� ca�kowita
		- [0-9]*"."[0-9]+ - warto�� zmiennoprzecinkowa
		- \"[^\"]*\" - ci�g znak�w
		- [a-zA-Z_][a-zA-Z0-9_]* - identyfikator

		\section this This
		w momencie wywo�ywania funkcji lokalnej (local) z instancji jakiej� konkretnej klasy w tej funkcji mamy do dyspozycji warto�� this
		\subsection thisnp Przyk�ad
		- tablica.Zmienna = 10<br>tablica.Funkcja = local() { print(this.Zmienna, "\n") }<br>tablica.Funkcja()
*/

//------------------------------------//
// Min/Max Definitions

/// Zwraca mniejsz� warto�� z dw�ch warto�ci
/// @param a	por�wnywana warto��
/// @param b	por�wnywana warto��
/// @returns mniejsza warto�� z dw�ch podanych
#define alMin(a,b)			((a)<(b)?(a):(b))

/// Zwraca wi�ksz� warto�� z dw�ch warto�ci
/// @param a	por�wnywana warto��
/// @param b	por�wnywana warto��
/// @returns wi�ksz� warto�� z dw�ch podanych
#define alMax(a,b)			((a)>(b)?(a):(b))

//------------------------------------//
// Some nasty VisualC++ Errors

#ifdef _MSC_VER
// Same nasty Visual C++ 2005 warning: go away !
#pragma warning(disable : 4996)

// Treat some warnings as errors (helps a lot in debugging :P)
#pragma warning(error : 4013 4244 4267)

/// Same nasty bug (there is no strcasecmp in VisualC++)
#define strcasecmp		strcmpi
#endif

//------------------------------------//
// ayul About

/// Nazwa projektu
#define AL_TITLE				"ayul"

/// Informacje o autorze
#define AL_AUTHOR				"ayufan (ayufan(at)o2(dot)pl)"

/// Numer wersji
#define AL_VERSION			"0.9.1"

//------------------------------------//
// Main TypeDefs

/// Definicja litera�u, przechowuje on informacje o warto�ci danej zmiennej
typedef struct alLiteral_s		alLiteral; 

/// Definicja typu litera�u, przechowuje on informacje o typie
typedef enum alLiteralType_e	alLiteralType;

/// Definicja warto�ci logicznej
typedef enum alBoolean_e			alBoolean;

/// Definicja ga��zi drzewa wyra�e�
typedef struct alNode_s				alNode; 

/// Defincja drzewa wyra�e�
typedef struct alTree_s				alTree;

/// Definicja definicji
typedef struct alDef_s				alDef;

/// Definicja stanu maszyny
typedef struct alState_s			alState;

/// Definicja stanu statusu maszyny
typedef enum alStateStatus_e	alStateStatus;

/// Definicja tablicy
typedef struct alTable_s			alTable;

/// Definicja warto�ci tablicy
typedef struct alValue_s			alValue;

/// Definicja bloku pami�ci
typedef struct alData_s				alData;

/// Definicja oznaczenia bloku pami�ci
typedef enum alMark_e					alMark;

//------------------------------------//
// Pointer To Function Definitions

/// Definicja wska�nika na funkcje dla definicji "proxy", uruchamianej przez skrypty
/// @param	state	wska�nik na stan maszyny
/// @returns			zwraca zwr�conych przez funkcj� parametr�w
typedef int	(*alCallback)(alState *state);

/// Definicja wska�nika na funkcje dla menad�era pami�ci, kt�ra okre�la spos�b oznaczania danego obszaru pami�ci
/// @param	data	adres obszaru pami�ci
/// @returns			zwraca wynik operacji (0 je�li brak b��d�w)
typedef int (*alCollect)(void *data);

/// Definicja wska�nika na funkcje dla menad�era pami�ci, kt�ra okre�la spos�b zwalniania danego obszaru pami�ci
/// @param	data	adres obszaru pami�ci
/// @returns			zwraca wynik operacji (0 je�li brak b��d�w)
typedef int (*alFree)(void *data);

//------------------------------------//
// String Definition

/// Defincja podstawowego typu okre�laj�cego ci�g znak�w
typedef char alString[256];

//------------------------------------//
// Literal Type Constants

/// Enumeracja przechowuj�ca typy litera��w
enum alLiteralType_e {
	/// Defincja typu automatycznego, czyli bez warto�ci
	AL_LITERAL_AUTO	= 0,

	/// Definicja typu posiadaj�cego dwa stany false lub true (dost�p przez alLiteral::Integer)
	AL_LITERAL_BOOLEAN,

	/// Definicja typu przechowuj�cego liczby ca�kowitego (dost�p przez alLiteral::Integer)
	AL_LITERAL_INTEGER,

	/// Definicja typu przechowuj�cego liczby zmienno przecinkowe (dost�p przez alLiteral::Value)
	AL_LITERAL_VALUE,

	/// Definicja typu przechowuj�cego wska�nik na ci�g znak�w (dost�p przez alLiteral::String)
	AL_LITERAL_STRING,

	/// Definicja typu przechowuj�cego wska�nik na obszar pami�ci (dost�p przez alLiteral::Pointer)
	AL_LITERAL_POINTER,

	/// Definicja typu przechowuj�cego wska�nik defincj� typu (dost�p przez alLiteral::Def)
	AL_LITERAL_DEF,

	/// Definicja typu przechowuj�cego wska�nik na inny litera� (dost�p przez alLiteral::Literal)
	AL_LITERAL_LITERAL,

	/// Definicja typu przechowuj�cego wska�nik na tablic� (dost�p przez alLiteral::Table)
	AL_LITERAL_TABLE,

	/// Definicja warto�ci okre�laj�cej maksymaln� ilo�� typ�w
	AL_LITERAL_MAX
};

//------------------------------------//
// Boolean Type Constants

/// Enumeracja przechowuj�ca mo�liwe warto�ci logiczne
enum alBoolean_e {
	/// Warto�� logiczna okre�laj�ca fa�sz
	alFalse = 0,

	/// Warto�� logiczna okre�laj�ca prawd�
	alTrue = 1
};

//------------------------------------//
// State Status Constants

/// Enumaracja okre�la mo�liwe stany maszyny wykonuj�cej skrypty
enum alStateStatus_e {
	/// Stan maszyny normalny (bez b��du)
	AL_STATUS_NONE = 0,

	/// Stan maszyny w momencie wywo�ania return (zarezerwowane, do wewn�trznego u�ycia)
	AL_STATUS_RETURN,

	/// Stan maszyny w momencie wywo�ania continue (zarezerwowane, do wewn�trznego u�ycia)
	AL_STATUS_CONTINUE,

	/// Stan maszyny w momencie wywo�ania break (zarezerwowane, do wewn�trznego u�ycia)
	AL_STATUS_BREAK,

	/// B��d og�lny wyst�puje, gdy nie wyst�puj� b��dy szczeg�owe
	AL_STATUS_ERROR = -1,

	/// B��d wyst�puje w momencie, pr�by wykonania kodu jeszcze nie obs�ugiwanego
	AL_STATUS_NOT_YET_SUPPORTED_ERROR = -100,

	/// B��d wyst�puje w momencie obliczania warto�ci wyra�enia, gdy wyra�enie jest nie jednoznaczne, tzn. nie zwraca TYLKO jednej warto�ci
	AL_STATUS_EXPRESSION_ERROR,

	/// B��d wyst�puje w momencie, gdzie nast�puje przypisanie do warto�ci, a brakuje l-warto�ci
	AL_STATUS_LVALUE_ERROR,

	/// B��d wyst�puje w momencie, gdy maszyna chce doda� zmienn� na stos, a z jakich� powod�w jest to nie mo�liwe, najprawdopodobniej jest to wtedy b��d wewn�trzny kompilatora, bo taka sytuacja nie powinna mie� miejsca
	AL_STATUS_SYMBOL_ERROR,

	/// B��d wyst�puje w momencie, pr�by wywo�ania definicji, gdy warto�� na stosie nie jest definicj�
	AL_STATUS_DEFINITION_ERROR,
	
	/// B��d wyst�puje w momencie, pr�by wywo�ania defincji, gdy do podanej definicji nie jest przypisana funkcja, kt�ra nale�y wywo�a�
	AL_STATUS_EXECUTE_ERROR,

	/// B��d wyst�puje w momencie, pr�by wrzucenia kolejnych warto�ci na stos, ale na nim nie ma miejsca na nie
	AL_STATUS_STACK_OVERFLOW_ERROR,

	/// B��d wyst�puje w momencie, pr�by zdj�cia warto�ci ze stosu, gdy na tym stosie nie ma tych warto�ci
	AL_STATUS_STACK_UNDERFLOW_ERROR,

	/// B��d wyst�puje w momencie, u�ycia instrukcji break i continue poza p�tlami, w momencie, gdy definicja ko�czy swoje dzia�anie ze statusem AL_STATUS_CONTINUE lub AL_STATUS_BREAK
	AL_STATUS_LOOP_ERROR,

	/// B��d wyst�puje w momencie, pr�by wykonania operacji jedno i dwuargumentowych na danych do tego nie przystosowanych
	AL_STATUS_INVALID_OPERATION_ERROR,

	/// B��d wyst�puje w momencie, wykorzystywanie wbudowanej warto�ci argv, gdy indeksujemy t� warto�� poza zakres wrzuconych argument�w lub w momencie gdy warto��, kt�r� indeksujemy nie jest liczb� ca�kowit�
	AL_STATUS_INVALID_ARGV_ERROR,

	/// B��d wyst�puje w momencie, gdy oczekiwany typ nie jest tablic�
	AL_STATUS_TABLE_ERROR,

	/// B��d wyst�puje w momencie, gdy nie mo�na pobra� warto�ci kom�rki z tablicy
	AL_STATUS_TABLE_VALUE_ERROR,

	/// B��d wyst�puje w momencie, "inicjalizacji zmiennych", gdy ilo�� zmiennych jest r�na od ilo�ci warto�ci do przypisania do tych zmiennych
	AL_STATUS_INITIALIZATION_ERROR,

	/// B��d zwracany przez funkcje u�ytkownika, w przypadku wykrycia, �e argumenty przekazane do tej funkcji s� b��dne
	AL_STATUS_INVALID_ARGUMENT_ERROR,

	/// B��d wyst�puje w momencie, pr�by obliczenia ilo�ci element�w danego litera�u, gdy litera� jest "nieobliczalny", czyli nie jest tablica lub nie jest ci�giem znak�w
	AL_STATUS_COUNT_ERROR
};

//------------------------------------//
// Memory Mark Constants

/// Definicja okre�laj�ca oznaczenie bloku pami�ci
enum alMark_e {
	/// Blok nie u�ywany
	AL_MARK_NONE = 0,

	/// Blok u�yty
	AL_MARK_USED,

	/// Blok u�yty i sprawdzony
	AL_MARK_CHECKED,

	/// Blok g��wny, kt�ry nie mo�e by� zwolniony
	AL_MARK_ROOT
};

//------------------------------------//
// Literal Structure

/// Struktura przechowuj�ca informacje o literale, czyli jego typ, warto�� o raz przypisan� do litera�u warto�� "this"
struct alLiteral_s {
	/// Pole przechowuj�ce typ litera�u
	alLiteralType			Type : 16;

	/// Pole przechowuj�ce informacje o referencji (warto�� zarezerwowana do wewn�trznego u�ycia)
	unsigned					ReferenceCount : 8;

	union {
		/// Pole przechowuj�ce warto�� logiczn� (AL_LITERAL_BOOLEAN)
		alBoolean		Boolean;

		/// Pole przechowuj�ce warto�� ca�kowit� (AL_LITERAL_INTEGER)
		int					Integer;

		/// Pole przechowuj�ce warto�� zmiennoprzecinkow� (AL_LITERAL_VALUE)
		float				Value;

		/// Pole przechowuj�ce ci�g znak�w (AL_LITERAL_STRING)
		char*				String;

		/// Pole przechowuj�ce wska�nik (AL_LITERAL_POINTER)
		void*				Pointer;

		/// Pole przechowuj�ce wska�nik na definicj� (AL_LITERAL_DEF)
		alDef*			Def;

		/// Pole przechowuj�ce wska�nik na litera� (AL_LITERAL_LITERAL)
		alLiteral*	Literal;

		/// Pole przechowuj�ce wska�nik na tablic� (AL_LITERAL_TABLE)
		alTable*		Table;
	};

	/// Pole przechowuj�ce warto�� this, przypisan� do danego litera�u
	alTable*			This;
};

//------------------------------------//
// State Functions

/// Tworzy stan nowej maszyny na kt�rej b�dzie mo�na uruchamia� kod
/// @param tree				wcze�niej skompilowane drzewo wyra�e�
/// @param stackSize	maksymalna ilo�� element�w na stosie maszyny
/// @returns					wska�nik na nowo utworzony stan maszyny
alState*			alStateNew(alTree *tree, int stackSize);

/// Niszczy stan maszyny
/// @param	state			wska�nik na maszyn�
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateDestroy(alState *state);

/// Pobiera ilo�� argument�w przekazanych do aktualnie wywo�anej defincji
/// @param	state			wska�nik na stan maszyny
/// @returns					zwraca ilo�� przekazanych argument�w do definicji (poprawna warto�� >= 0)
int						alStateGetArgs(alState *state);

/// Pobiera ilo�� zwr�conych parametr�w przez ostatnio wywo�an� funkcj�
/// @param	state			wka�nik na stan maszyny
/// @returns					zwraca ilo�� zwr�conych parametr�w (poprawna warto�� >= 0)
int						alStateGetReturns(alState *state);

/// Pobiera przypisan� tablic� do danego wywo�ania "this"
/// @param	state			wka�nik na stan maszyny
/// @returns					zwraca wska�nik na tablic� przypisan� do wywo�ania
alTable*			alStateGetThis(alState *state);

/// Zwraca kod statusu dla stanu danej maszyny
/// @param	state			wska�nik na stan maszyny
/// @returns					kod statusu dla danej maszyny
alStateStatus	alStateGetStatus(alState *state);

/// Zwraca nazw� pliku w momencie napotkania b��du w uruchamianym kodzie
/// @param	state			wska�nik na stan maszyny
/// @param	file			bufor do kt�rego mamy zapisa� nazw� pliku
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateGetErrorFile(alState *state, alString file);

/// Zwraca numer linji w pliku w momencie napotkania b�edy w uruchamianym kodzie
/// @param	state			wska�nik na stan maszyny
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateGetErrorLine(alState *state);

/// Zwraca nazw� definicji w kt�rej ten b��d nast�pi� w momencie napotkania b��du
/// @param	state			wska�nik na stan maszyny
/// @param	def				bufor do kt�rego mamy zapisa� nazw� definicji
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateGetErrorDef(alState *state, alString def);

/// Ustawia nowy status dla stanu maszyny
/// @param	state			wska�nik na stan maszyny
/// @param	status		nowy status
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateSetStatus(alState *state, alStateStatus status);

/// Pobiera warto�� definicji, kt�ra jest zmienn� globaln�
/// @param	state			wska�nik na stan maszyny
/// @param	def				wska�nik na definicj�, kt�rej mamy pobra� warto��
/// @param	value			wska�nik na litera� do kt�rego mamy zapisa� warto��
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateGetGlobalLiteral(alState *state, alDef *def, alLiteral *value);

/// Ustawia warto�� defincji, kt�ra jest zmienn� globaln�
/// @param	state			wska�nik na stan maszyny
/// @param	def				wska�nik na definicj�, kt�rej mamy przypisa� warto��
/// @param	value			warto�� jak� mamy przypisa� definicji
/// @returns					zwraca wynik operacji (0 je�li brak b��du)
int						alStateSetGlobalLiteral(alState *state, alDef *def, alLiteral value);

//------------------------------------//
// Literal Functions

/// Tworzy pusty litera� (null)
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreate();

/// Tworzy litera� b�d�cy przechowuj�cy warto�� logiczn� 0 lub 1
/// @param	flag	warto�� logiczna jak� przechowuje litera�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateBoolean(alBoolean flag);

/// Tworzy litera� b�d�cy liczb� ca�kowit�
/// @param	value	warto�� ca�kowita jak� przechowuje litera�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateInteger(int value);

/// Tworzy litera� b�d�cy liczb� zmiennoprzecinkow�
/// @param	value	warto�� zmiennoprzecinkowa, jak� przechowuje litera�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateValue(float value);

/// Tworzy litera� przechowuj�cy ci�g znak�w
/// @param	name	ci�g znak�w, jak� przechowuje litera�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateString(const char *name);

/// Tworzy litera� przechowuj�cy wska�nik
/// @param	data	wska�nik, jak� przechowuje litera�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreatePointer(void *data);

/// Tworzy litera� przechowuj�cy referencj� na inny litera�
/// @param	value	wska�nik na inny litera�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateLiteral(alLiteral *value);

/// Tworzy litera� przechowuj�cy informacje o definicji
/// @param	def		wska�nik na definicj�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateDef(alDef *def);

/// Tworzy litera� przechowuj�cy tablic�
/// @param	table	wska�nik na tablic�
/// @returns			zwraca warto�� utworzonego litera�u
alLiteral			alLiteralCreateTable(alTable *table);

/// Przekonwertowuje warto�� litera�u do warto��i logicznej
/// - alLiteralType::AL_LITERAL_BOOLEAN: alLiteral::Boolean
/// - alLiteralType::AL_LITERAL_INTEGER: alLiteral::Integer != 0
/// - alLiteralType::AL_LITERAL_VALUE: alLiteral::Value != 0
/// - alLiteralType::AL_LITERAL_STRING: !empty(#alLiteral::String)
/// - alLiteralType::AL_LITERAL_POINTER: alLiteral::Pointer != NULL
/// - alLiteralType::AL_LITERAL_DEF: alLiteral::Def != NULL
/// - alLiteralType::AL_LITERAL_TABLE: !empty(#alLiteral::Table)
/// - inaczej: alFalse
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (warto�� logiczn�)
alBoolean			alLiteralToBoolean(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do warto��i ca�kowitej
/// - alLiteralType::AL_LITERAL_BOOLEAN: int(#alLiteral::Boolean)
/// - alLiteralType::AL_LITERAL_INTEGER: alLiteral::Integer
/// - alLiteralType::AL_LITERAL_VALUE: int(#alLiteral::Value)
/// - alLiteralType::AL_LITERAL_STRING: atoi(#alLiteral::String)
/// - alLiteralType::AL_LITERAL_TABLE: count(#alLiteral::Table)
/// - inaczej: 0
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (warto�� ca�kowita)
int						alLiteralToInteger(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do warto��i zmiennoprzecinkowej
/// - alLiteralType::AL_LITERAL_BOOLEAN: float(#alLiteral::Boolean)
/// - alLiteralType::AL_LITERAL_INTEGER: float(#alLiteral::Integer)
/// - alLiteralType::AL_LITERAL_VALUE: alLiteral::Value
/// - alLiteralType::AL_LITERAL_STRING: atof(#alLiteral::String)
/// - inaczej: 0.0
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (warto�� zmiennoprzecinkowa)
float					alLiteralToValue(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do ci�gu znak�w
/// - alLiteralType::AL_LITERAL_BOOLEAN: alLiteral::Boolean ? "true" : "false"
/// - alLiteralType::AL_LITERAL_INTEGER: itoa(#alLiteral::Integer)
/// - alLiteralType::AL_LITERAL_VALUE: ftoa(#alLiteral::Value)
/// - alLiteralType::AL_LITERAL_STRING: alLiteral::String
/// - alLiteralType::AL_LITERAL_POINTER: 0xHEX(#alLiteral::Pointer)
/// - alLiteralType::AL_LITERAL_DEF: "{Def}"
/// - alLiteralType::AL_LITERAL_TABLE: "{Table}"
/// - alLiteralType::AL_LITERAL_LITERAL: "{Literal}"
/// - inaczej: ""
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (ci�g znak�w)
char*					alLiteralToString(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do wska�nika
/// - alLiteralType::AL_LITERAL_POINTER: alLiteral::Pointer
/// - inaczej: NULL
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (wska�nik)
void*					alLiteralToPointer(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do wska�nika na definicj�
/// - alLiteralType::AL_LITERAL_DEF: alLiteral::Def
/// - inaczej: NULL
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (wska�nik na definicj�)
alDef*				alLiteralToDef(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do wska�nika na litera�
/// - alLiteralType::AL_LITERAL_LITERAL: alLiteral::Literal
/// - inaczej: NULL
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (wska�nik na litera�)
alLiteral*		alLiteralToLiteral(alLiteral *value);

/// Przekonwertowuje warto�� litera�u do wska�nika na tablic�
/// - alLiteralType::AL_LITERAL_TABLE: alLiteral::Table
/// - inaczej: NULL
/// @param	value	wska�nik na litera�
/// @returns			zwraca wynik operacji (wska�nik na tablic�)
alTable*			alLiteralToTable(alLiteral *value);

alLiteral			alLiteralUnaryNeg(alLiteral *value);
alLiteral			alLiteralUnaryAdd(alLiteral *value);
alLiteral			alLiteralUnarySub(alLiteral *value);

alLiteral			alLiteralAdd(alLiteral *value1, alLiteral *value2);
alLiteral			alLiteralSub(alLiteral *value1, alLiteral *value2);
alLiteral			alLiteralMul(alLiteral *value1, alLiteral *value2);
alLiteral			alLiteralDiv(alLiteral *value1, alLiteral *value2);
alLiteral			alLiteralMod(alLiteral *value1, alLiteral *value2);
alLiteral			alLiteralPow(alLiteral *value1, alLiteral *value2);

int						alLiteralEq(alLiteral *value1, alLiteral *value2);
int						alLiteralNEq(alLiteral *value1, alLiteral *value2);
int						alLiteralLess(alLiteral *value1, alLiteral *value2);
int						alLiteralLessEq(alLiteral *value1, alLiteral *value2);
int						alLiteralGr(alLiteral *value1, alLiteral *value2);
int						alLiteralGrEq(alLiteral *value1, alLiteral *value2);
int						alLiteralEqq(alLiteral *value1, alLiteral *value2);

/// Oznacza obszar pamieci u�ywany przez litera�
/// @param	value	wska�nik na litera�
void					alLiteralMark(alLiteral *value);

//------------------------------------//
// Table Functions

/// Tworzy now� tablic�
/// @returns		wska�nik na nowo utworzon� tablic�
alTable*			alTableNew();

/// Niszczy tablic�
/// @param	table	wska�nik na tablic�
/// @returns			wynik operacji (0 je�li brak b��du)
int						alTableDestroy(alTable* table);

/// Pobiera warto�� (bez u�ycia metafunkcji) znajduj�c� si� pod podanym kluczem w tablicy (w przypadku, gdy dany klucz nie istnieje, funkcja automatycznie go tworzy)
/// @param	table	wska�nik na tablic�
/// @param	key		klucz dla kt�rego nale�y pobra� warto�� (w przypadku, gdy litera� jest AL_LITERAL_AUTO, funkcja twrzony nowy element na pierwszym znalezionym wolnym miejscu w tablicy)
/// @returns			adres kom�rki znajduj�cej si� w tablicy (NULL w przypadku b��du)
alLiteral*		alTableGetRawValue(alTable *table, alLiteral key);

/// Pobiera warto�� (z u�ycia metafunkcji) znajduj�c� si� pod podanym kluczem w tablicy (w przypadku, gdy dany klucz nie istnieje, funkcja automatycznie go tworzy)
/// @param	table	wska�nik na tablic�
/// @param	key		klucz dla kt�rego nale�y pobra� warto�� (w przypadku, gdy litera� jest AL_LITERAL_AUTO, funkcja twrzony nowy element na pierwszym znalezionym wolnym miejscu w tablicy)
/// @param	state	wska�nik na stan maszyny, na kt�rej mamy uruchomi� metafunkcje
/// @returns			adres kom�rki znajduj�cej si� w tablicy (NULL w przypadku b��du)
alLiteral			alTableGetValue(alTable *table, alLiteral key, alState *state);

/// Usuwa z tablicy warto�� pod podanym kluczem
/// @param	table	wska�nik na tablic�
/// @param	key		klucz dla kt�rego mamy usun�� warto��
/// @returns			zwraca wynik operacji (0 gdy usuni�to, 1 gdy taka kom�rka nie istnieje)
int						alTableRemove(alTable *table, alLiteral key);

/// Zwraca ilo�� element�w znajduj�cych si� w tablicy
/// @param	table	wska�nik na tablic�
/// @returns			ilo�� element�w tablicy (< 0 w przypadku wyst�pienia b��du)
int						alTableCount(alTable *table);

/// Resetuje wewn�trzny enumerator tablicy
/// @param	table	wska�nik na tablic�
/// @returns			zwraca wynik operacji (0 je�li brak b��du)
int						alTableReset(alTable *table);

/// Pobiera kolejny element tablicy
/// @param	table	wska�nik na tablic�
/// @param	key		wska�nik na litera�, do kt�rego mamy zapisa� klucz dla danego elementu (klucz mo�e by� r�wa NULL)
/// @param	value	wska�nik na litera�, do kt�rego mamy zapisa� warto�� dla danego elementu (warto�� mo�e by� r�wa NULL)
/// @returns			zwraca wynik operacji (0 je�li pobrano kolejny element, 1 w przypadku osi�gni�cia ko�ca tablicy)
int						alTableEach(alTable *table, alLiteral *key, alLiteral *value);

//------------------------------------//
// Memory Functions

/// Alokuje zarz�dzaln� pami��
/// @param	buf			poprzednii obszar pami�ci, kt�ry chcemy zmieni� (mo�e by� NULL)
/// @param	size		wielko�� alokacji
/// @param	collect	wska�nik na funkcje wywo�ywana podczas oznaczania obszar�w pami�ci
/// @param	free		wska�nik na funkcj� wywo�ywan� podczas zwalnia obszaru pami�ci
/// @returns				nowy obszar pami�ci o podanych do funkcji parametrach
void*					alMemoryAlloc(void *buf, int size, alCollect collect, alFree free);

/// Zwalnia zarz�dzaln� pami��
/// @param	buf			obszar pami�ci, kt�ry chcemy zwolni�
/// @returns				zwraca wynik operacji (0 je�li brak b��du)
int						alMemoryFree(void *buf);

/// Pobiera oznaczenie obszaru pami�ci
/// @param	buf			obszar pami�ci
/// @returns				zwraca oznaczenie dla danego obszaru pami�ci
alMark				alMemoryGetMark(void *buf);

/// Ustawia oznaczenie alMark::AL_MARK_NONE
/// @param	buf			obszar pami�ci
/// @returns				zwraca wynik operacji (0 je�li brak b��du)
int						alMemorySetNone(void *buf);

/// Ustawia oznaczenie alMark::AL_MARK_ROOT
/// @param	buf			obszar pami�ci
/// @returns				zwraca wynik operacji (0 je�li brak b��du)
int						alMemorySetRoot(void *buf);

/// Zmiena oznaczenie z alMark::AL_MARK_NONE na alMark::AL_MARK_USED oraz zapisuje dany blok do sprawdzania (funkcji u�ywa si� do oznaczania u�ywanych obszar�w pami�ci w funkcji alCollect)
/// @param	buf			obszar pami�ci
/// @returns				zwraca wynik operacji (0 je�li brak b��du)
int						alMemoryMarkAsUsed(void *buf);

/// Sprawdza i automatycznie usuwa nie u�ywane obszary pami�ci
/// @returns				zwraca ilo�� usuni�tych obszar�w pami�ci
int						alMemoryCollect();

/// Pobiera ilo�� utworzonych obszar�w pami�ci
/// @returns				zwraca ilo�� utworzonych obszar�w pami�ci
int						alMemoryCount();

/// Duplikuje ci�g znak�w do pami�ci zarz�dzalnej
/// @param	text		ci�g znak�w
/// @returns				wska�nik na zduplikowany ci�g znak�w, utworzony w pami�ci zarz�dzalnej
char*					alMemoryStrDup(const char *text);

//------------------------------------//
// Stack Manipulation Functions

/// Pobiera element znajduj�cy si� na stosie
/// @param	state	wska�nik na stan maszyny
/// @param	top		indeks elementu ze stosu
/// @returns			wska�nik na wybrany element (NULL w przypadku b��du)
alLiteral*		alPeek(alState *state, int top);

/// Pobiera element znajduj�cy si� na stosie argument�w
/// @param	state	wska�nik na stan maszyny
/// @param	argv	indeks argumentu
/// @returns			wska�nik na wybrany element (NULL w przypadku b��du)
alLiteral*		alPeekArgv(alState *state, int argv);

/// Sprawdza czy element na stosie jest danego typu
/// @param	state	wska�nik na stan maszyny
/// @param	top		indeks elementu ze stosu
/// @param	type	typ elementu
/// @returns			zwraca warto�� logiczn�
alBoolean			alIsOfType(alState *state, int top, int type);

/// Sprawdza czy element na stosie argument�w jest danego typu
/// @param	state	wska�nik na stan maszyny
/// @param	argv	indeks argumentu
/// @param	type	typ elementu
/// @returns			zwraca warto�� logiczn�
alBoolean			alIsOfTypeArgv(alState *state, int argv, int type);

/// Wrzuca na szczyt stosu now� warto��
/// @param	state	wska�nik na stan maszyny
/// @param	value	warto��
/// @returns			zwraca wynik operacji (0 je�li brak b��du) (w przypadku gdy na stosie brakuje miejsca ustawia alStateStatus::AL_STATUS_STACK_OVERFLOW_ERROR)
int						alPush(alState *state, alLiteral value);

/// Zdejmuje warto�� ze stosu
/// @param	state	wska�nik na stan maszyny
/// @param	top		indeks elementu ze stosu
/// @returns			zwraca warto�� ze stosu (w przypadku, gdy na stosie nie ma takiej warto�ci alStateStatus::AL_STATUS_STACK_UNDERFLOW_ERROR)
alLiteral			alPop(alState *state, int top);

/// Usuwa ze stosu warto�ci
/// @param	state	wska�nik na stan maszyny
/// @param	top		indeks elementu ze stosu
/// @param	count	ilo�� element�w do usuni�cia
/// @returns			zwraca ilo�� usuni�tych element�w (w przypadku, gdy na stosie nie ma takich warto�ci zwraca < 0 i ustawia alStateStatus::AL_STATUS_STACK_UNDERFLOW_ERROR)
int						alRemove(alState *state, int top, int count);

//------------------------------------//
// Tree Functions

/// Tworzy nowe drzewo wyra�e�
/// @returns			wska�nik na nowe drzewo wyra�e� (NULL w przypadku b��du)
alTree*				alTreeNew();

/// Niszczy drzewo wyra�e�
/// @param	tree	wska�nik na drzewo wyra�e�
/// @returns			wynik operacji (0 je�li brak b��du)
int						alTreeDestroy(alTree *tree);

/// Aktywuje drzewo wyra�e�
/// @param	tree	wska�nik na drzewo wyra�e� (NULL deaktywuje aktywne)
void					alTreeActivate(alTree *tree);

/// Pobiera aktywne drzewo wyra�e�
/// @returns			wska�nik na aktywne drzewo wyra�e� (NULL w przypadku braku aktywnego)
alTree*				alTreeGetActive();

/// Dodaje globaln� definicj�, b�d�c� zmienn� do aktywnego drzewa wyra�e�
/// @param	name	ci�g znak�w okre�laj�cy nazw� tworzonej definicji
/// @returns			wska�nik na nowo utworzon� definicj� (NULL w przypadku b��du)
alDef*				alTreeAddGlobalDef(const char *name);

/// Dodaje globaln� definicj�, przechowuj�c� funkcj� do aktywnego drzewa wyra�e�
/// @param	name	ci�g znak�w okre�laj�cy nazw� tworzonej definicji
/// @param	code	wska�nik na funkcj�
/// @returns			wska�nik na nowo utworzon� definicj� (NULL w przypadku b��du)
alDef*				alTreeAddGlobalCallbackDef(const char *name, alCallback code);

/// �aduje kod znajduj�cy si� w pliku do aktywnego drzewa wyra�e�
/// @param	name	ci�g znak�w okre�laj�cy nazw� pliku
/// @returns			zwraca wynik operacji (0 je�li brak b��du, < 0 je�li wyst�pi�y b��dy)
int						alTreeLoadInto(const char *name);

/// Buduje aktywne drzewo wyra�e�
/// @returns			zwraca wynik operacji (0 je�li brak b��du, < 0 je�li wyst�pi�y b��dy)
int						alTreeBuild();

/// Wyszukuje definicj� globaln� o podanej nazwie w drzewie wyra�e�
/// @param	tree	wska�nik na drzewo wyra�e�
/// @param	name	ci�g znak�w okre�laj�cy nazw� definicji
/// @returns			zwraca wska�nik na znalezion� definicj� (NULL w przypadku, gdy taka nie istnieje lub wyst�pi�y b��dy)
alDef*				alTreeFindGlobalDef(alTree *tree, const char *name);

//------------------------------------//
// Def Functions

/// Pobiera nazw� definicji
/// @param	def			wska�nik na definicj�
/// @param	name		bufor do kt�rego mamy zapisa� nazw� defincji
/// @returns				zwraca wynik operacji (0 je�li brak b��du)
int						alDefGetName(alDef *def, alString name);

/// Pobiera warto�� logiczn�, czy dana definicja jest funkcj�
/// @param	def				wska�nik na definicj�
/// @returns					zwraca warto�� logiczn�
alBoolean			alDefIsCallback(alDef *def);

/// Pobiera warto�� logiczn�, czy dana definicja jest definicj� globaln�
/// @param	def				wska�nik na definicj�
/// @returns					zwraca warto�� logiczn�
alBoolean			alDefIsGlobal(alDef *def);

//------------------------------------//
// Execute Functions

/// Wywo�uje definicj� na maszynie
/// @param	state			wska�nik na stan maszyny, na kt�rej mamy wykona� definicj�
/// @param	def				wska�nik na wykonywan� definicj�
/// @param	this_			wska�nik na przypisan� instancj� tablicy do danego wywo�ania "this"
/// @param	nargs			ilo�� argument�w przekazanych do wywo�ania
/// @returns					zwraca wynik operacji (alStateStatus)
alStateStatus	alExecute(alState *state, alDef *def, alTable *this_, int nargs);

//------------------------------------//
// Stack Manipulation Macros

/// Sprawdza czy na stosie znajduje si� warto�� logiczna \see alIsOfType
#define alIsOfBoolean(state, top)					alIsOfType(state, top, AL_LITERAL_BOOLEAN)

/// Sprawdza czy na stosie znajduje si� warto�� ca�kowita \see alIsOfType
#define alIsOfInteger(state, top)					alIsOfType(state, top, AL_LITERAL_INTEGER)

/// Sprawdza czy na stosie znajduje si� warto�� zmiennoprzecinkowa \see alIsOfType
#define alIsOfValue(state, top)						alIsOfType(state, top, AL_LITERAL_VALUE)

/// Sprawdza czy na stosie znajduje si� ci�g znak�w \see alIsOfType
#define alIsOfString(state, top)					alIsOfType(state, top, AL_LITERAL_STRING)

/// Sprawdza czy na stosie znajduje si� wska�nik \see alIsOfType
#define alIsOfPointer(state, top)					alIsOfType(state, top, AL_LITERAL_POINTER)

/// Sprawdza czy na stosie znajduje si� wska�nik na definicj� \see alIsOfType
#define alIsOfDef(state, top)							alIsOfType(state, top, AL_LITERAL_DEF)

/// Sprawdza czy na stosie znajduje si� wska�nik na litera� \see alIsOfType
#define alIsOfLiteral(state, top)					alIsOfType(state, top, AL_LITERAL_LITERAL)

/// Sprawdza czy na stosie znajduje si� wska�nik na tablic� \see alIsOfType
#define alIsOfTable(state, top)						alIsOfType(state, top, AL_LITERAL_TABLE)

//------------------------------------//

/// Sprawdza czy na stosie argument�w znajduje si� warto�� logiczna \see alIsOfTypeArgv
#define alIsOfBooleanArgv(state, argv)		alIsOfTypeArgv(state, argv, AL_LITERAL_BOOLEAN)

/// Sprawdza czy na stosie argument�w znajduje si� warto�� ca�kowita \see alIsOfTypeArgv
#define alIsOfIntegerArgv(state, argv)		alIsOfTypeArgv(state, argv, AL_LITERAL_INTEGER)

/// Sprawdza czy na stosie argument�w znajduje si� warto�� zmiennoprzecinkowa \see alIsOfTypeArgv
#define alIsOfValueArgv(state, argv)			alIsOfTypeArgv(state, argv, AL_LITERAL_VALUE)

/// Sprawdza czy na stosie argument�w znajduje si� ci�g znak�w \see alIsOfTypeArgv
#define alIsOfStringArgv(state, argv)			alIsOfTypeArgv(state, argv, AL_LITERAL_STRING)

/// Sprawdza czy na stosie argument�w znajduje si� wska�nik \see alIsOfTypeArgv
#define alIsOfPointerArgv(state, argv)		alIsOfTypeArgv(state, argv, AL_LITERAL_POINTER)

/// Sprawdza czy na stosie argument�w znajduje si� wska�nik na definicj� \see alIsOfTypeArgv
#define alIsOfDefArgv(state, argv)				alIsOfTypeArgv(state, argv, AL_LITERAL_DEF)

/// Sprawdza czy na stosie argument�w znajduje si� wska�nik na litera� \see alIsOfTypeArgv
#define alIsOfLiteralArgv(state, argv)		alIsOfTypeArgv(state, argv, AL_LITERAL_LITERAL)

/// Sprawdza czy na stosie argument�w znajduje si� wska�nik na tablic� \see alIsOfTypeArgv
#define alIsOfTableArgv(state, argv)			alIsOfTypeArgv(state, argv, AL_LITERAL_TABLE)

//------------------------------------//

/// Wrzuca na stos warto�� logiczn� \see alPush \see alLiteralCreateBoolean
#define alPushBoolean(state, value)				alPush(state, alLiteralCreateBoolean(value))

/// Wrzuca na stos warto�� ca�kowit� \see alPush \see alLiteralCreateInteger
#define alPushInteger(state, value)				alPush(state, alLiteralCreateInteger(value))

/// Wrzuca na stos warto�� zmiennoprzecinkow� \see alPush \see alLiteralCreateValue
#define alPushValue(state, value)					alPush(state, alLiteralCreateValue(value))

/// Wrzuca na stos ci�g znak�w \see alPush \see alLiteralCreateString
#define alPushString(state, value)				alPush(state, alLiteralCreateString(value))

/// Wrzuca na stos wska�nik \see alPush \see alLiteralCreatePointer
#define alPushPointer(state, value)				alPush(state, alLiteralCreatePointer(value))

/// Wrzuca na stos wska�nik na definicj� \see alPush \see alLiteralCreateDef
#define alPushDef(state, value)						alPush(state, alLiteralCreateDef(value))

/// Wrzuca na stos wska�nik na litera� \see alPush \see alLiteralCreateLiteral
#define alPushLiteral(state, value)				alPush(state, alLiteralCreateLiteral(value))

/// Wrzuca na stos wska�nik na tablic� \see alPush \see alLiteralCreateTable
#define alPushTable(state, value)					alPush(state, alLiteralCreateTable(value))

//------------------------------------//

/// Pobiera ze stosu warto�� logiczn� \see alPeek \see alLiteralToBoolean
#define alPeekBoolean(state, top)					alLiteralToBoolean(alPeek(state, top))

/// Pobiera ze stosu warto�� ca�kowit� \see alPeek \see alLiteralToInteger
#define alPeekInteger(state, top)					alLiteralToInteger(alPeek(state, top))

/// Pobiera ze stosu warto�� zmiennoprzecinkow� \see alPeek \see alLiteralToValue
#define alPeekValue(state, top)						alLiteralToValue(alPeek(state, top))

/// Pobiera ze stosu ci�g znak�w \see alPeek \see alLiteralToString
#define alPeekString(state, top)					alLiteralToString(alPeek(state, top))

/// Pobiera ze stosu wska�nik \see alPeek \see alLiteralToPointer
#define alPeekPointer(state, top)					alLiteralToPointer(alPeek(state, top))

/// Pobiera ze stosu wska�nik na definicj� \see alPeek \see alLiteralToDef
#define alPeekDef(state, top)							alLiteralToDef(alPeek(state, top))

/// Pobiera ze stosu wska�nik na litera� \see alPeek \see alLiteralToLiteral
#define alPeekLiteral(state, top)					alLiteralToLiteral(alPeek(state, top))

/// Pobiera ze stosu wska�nik na tablic� \see alPeek \see alLiteralToTable
#define alPeekTable(state, top)						alLiteralToTable(alPeek(state, top))

//------------------------------------//

/// Pobiera ze stosu argument�w warto�� logiczn� \see alPeekArgv \see alLiteralToBoolean
#define alPeekBooleanArgv(state, argv)		alLiteralToBoolean(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w warto�� ca�kowit� \see alPeekArgv \see alLiteralToInteger
#define alPeekIntegerArgv(state, argv)		alLiteralToInteger(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w warto�� zmiennoprzecinkow� \see alPeekArgv \see alLiteralToValue
#define alPeekValueArgv(state, argv)			alLiteralToValue(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w ci�g znak�w \see alPeekArgv \see alLiteralToString
#define alPeekStringArgv(state, argv)			alLiteralToString(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w wska�nik \see alPeekArgv \see alLiteralToPointer
#define alPeekPointerArgv(state, argv)		alLiteralToPointer(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w wska�nik na defincj� \see alPeekArgv \see alLiteralToDef
#define alPeekDefArgv(state, argv)				alLiteralToDef(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w wska�nik na litera� \see alPeekArgv \see alLiteralToLiteral
#define alPeekLiteralArgv(state, argv)		alLiteralToLiteral(alPeekArgv(state, argv))

/// Pobiera ze stosu argument�w wska�nik na tablic� \see alPeekArgv \see alLiteralToTable
#define alPeekTableArgv(state, argv)			alLiteralToTable(alPeekArgv(state, argv))
