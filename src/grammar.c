/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "2.1"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_GLOBAL = 258,
     T_LOCAL = 259,
     T_TO = 260,
     T_COUNT = 261,
     T_THIS = 262,
     T_FOREACH = 263,
     T_IS = 264,
     T_AS = 265,
     T_ARGC = 266,
     T_ARGV = 267,
     T_IF = 268,
     T_ELSE = 269,
     T_WHILE = 270,
     T_DO = 271,
     T_FOR = 272,
     T_RETURN = 273,
     T_CONTINUE = 274,
     T_BREAK = 275,
     T_NULL = 276,
     T_BEGIN = 277,
     T_T_BEGIN = 278,
     T_END = 279,
     T_T_END = 280,
     T_OPEN = 281,
     T_CLOSE = 282,
     T_S_OPEN = 283,
     T_S_CLOSE = 284,
     T_STATEMENT = 285,
     T_ACCESSOR = 286,
     T_OR = 287,
     T_AND = 288,
     T_NEGATION = 289,
     T_GREATER = 290,
     T_LESS = 291,
     T_GREATER_EQUAL = 292,
     T_LESS_EQUAL = 293,
     T_NOT_EQUAL = 294,
     T_EQUALITY = 295,
     T_EQUAL = 296,
     T_POWER = 297,
     T_MODULUS = 298,
     T_DIVIDE = 299,
     T_MULTIPLICATION = 300,
     T_SUBTRACTION = 301,
     T_ADDITION = 302,
     T_OTHERWISE = 303,
     T_TEST = 304,
     T_SEPARATOR = 305,
     T_POWER_ASSIGNMENT = 306,
     T_MODULUS_ASSIGNMENT = 307,
     T_DIVIDE_ASSIGNMENT = 308,
     T_MULTIPLY_ASSIGNMENT = 309,
     T_SUBTRACTIVE_ASSIGNMENT = 310,
     T_ADDITIVE_ASSIGNMENT = 311,
     T_ASSIGNMENT = 312,
     T_INCREMENTATION = 313,
     T_DECREMENTATION = 314,
     T_IDENTIFIER = 315,
     T_INTEGER = 316,
     T_BOOLEAN = 317,
     T_FLOAT = 318,
     T_STRING = 319
   };
#endif
/* Tokens.  */
#define T_GLOBAL 258
#define T_LOCAL 259
#define T_TO 260
#define T_COUNT 261
#define T_THIS 262
#define T_FOREACH 263
#define T_IS 264
#define T_AS 265
#define T_ARGC 266
#define T_ARGV 267
#define T_IF 268
#define T_ELSE 269
#define T_WHILE 270
#define T_DO 271
#define T_FOR 272
#define T_RETURN 273
#define T_CONTINUE 274
#define T_BREAK 275
#define T_NULL 276
#define T_BEGIN 277
#define T_T_BEGIN 278
#define T_END 279
#define T_T_END 280
#define T_OPEN 281
#define T_CLOSE 282
#define T_S_OPEN 283
#define T_S_CLOSE 284
#define T_STATEMENT 285
#define T_ACCESSOR 286
#define T_OR 287
#define T_AND 288
#define T_NEGATION 289
#define T_GREATER 290
#define T_LESS 291
#define T_GREATER_EQUAL 292
#define T_LESS_EQUAL 293
#define T_NOT_EQUAL 294
#define T_EQUALITY 295
#define T_EQUAL 296
#define T_POWER 297
#define T_MODULUS 298
#define T_DIVIDE 299
#define T_MULTIPLICATION 300
#define T_SUBTRACTION 301
#define T_ADDITION 302
#define T_OTHERWISE 303
#define T_TEST 304
#define T_SEPARATOR 305
#define T_POWER_ASSIGNMENT 306
#define T_MODULUS_ASSIGNMENT 307
#define T_DIVIDE_ASSIGNMENT 308
#define T_MULTIPLY_ASSIGNMENT 309
#define T_SUBTRACTIVE_ASSIGNMENT 310
#define T_ADDITIVE_ASSIGNMENT 311
#define T_ASSIGNMENT 312
#define T_INCREMENTATION 313
#define T_DECREMENTATION 314
#define T_IDENTIFIER 315
#define T_INTEGER 316
#define T_BOOLEAN 317
#define T_FLOAT 318
#define T_STRING 319




/* Copy the first part of user declarations.  */
#line 1 "grammar.cxx"

	
//------------------------------------//
//
// grammar.cxx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-3-31
//
//------------------------------------//

#include "stdafx.h"

#ifdef _MSC_VER
// Treat some previous errors as warnings
#pragma warning(disable : 4244 4267)
#endif

#define YYERROR_VERBOSE
//#define YYDEBUG			1
#define YYPARSE_PARAM	data
#define alloca			malloc

#define alCurrentNode			alCurrentNodes[alCurrentNodeIndex]
#define alPushCurNode(x)	if(alCurrentNodeIndex < MAX_STACK){alCurrentNodes[++alCurrentNodeIndex] = (x);} else {yyerror("node stack overflow"); YYABORT;}
#define alPopCurNode			alCurrentNodes[alCurrentNodeIndex--]
#define alCurrentDef			alCurrentDefs[alCurrentDefIndex]
#define alPushCurDef(x)		if(alCurrentDefIndex < MAX_STACK){alCurrentDefs[++alCurrentDefIndex] = (x);} else {yyerror("def stack overflow"); YYABORT;}
#define alPopCurDef				alCurrentDefs[alCurrentDefIndex--]



/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 35 "grammar.cxx"
typedef union YYSTYPE {
	int				i;
	float			f;
	char*			s;
	alString	id;

	alNode*		n;
	alDef*		d;

	alValue*	vn;
} YYSTYPE;
/* Line 196 of yacc.c.  */
#line 258 "grammar.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 219 of yacc.c.  */
#line 270 "grammar.c"

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T) && (defined (__STDC__) || defined (__cplusplus))
# include <stddef.h> /* INFRINGES ON USER NAME SPACE */
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#ifndef YY_
# if YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if defined (__STDC__) || defined (__cplusplus)
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#     define YYINCLUDED_STDLIB_H
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2005 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM ((YYSIZE_T) -1)
#  endif
#  ifdef __cplusplus
extern "C" {
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if (! defined (malloc) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if (! defined (free) && ! defined (YYINCLUDED_STDLIB_H) \
	&& (defined (__STDC__) || defined (__cplusplus)))
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifdef __cplusplus
}
#  endif
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (defined (YYSTYPE_IS_TRIVIAL) && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short int yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short int) + sizeof (YYSTYPE))			\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined (__GNUC__) && 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  YYSIZE_T yyi;				\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short int yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  3
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   758

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  65
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  46
/* YYNRULES -- Number of rules. */
#define YYNRULES  123
/* YYNRULES -- Number of states. */
#define YYNSTATES  223

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   319

#define YYTRANSLATE(YYX)						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short int yyprhs[] =
{
       0,     0,     3,     5,     7,     9,    11,    13,    15,    17,
      19,    21,    25,    30,    34,    39,    41,    43,    48,    55,
      58,    61,    63,    65,    67,    69,    71,    73,    75,    77,
      79,    81,    83,    86,    89,    91,    94,    97,   100,   103,
     105,   109,   113,   115,   119,   123,   125,   129,   133,   135,
     139,   143,   147,   151,   155,   159,   163,   165,   168,   170,
     174,   176,   180,   182,   188,   190,   194,   198,   202,   206,
     210,   214,   218,   220,   224,   228,   230,   232,   233,   235,
     236,   238,   240,   242,   246,   249,   254,   255,   265,   267,
     271,   275,   279,   283,   287,   288,   298,   301,   303,   305,
     306,   307,   312,   315,   316,   317,   323,   324,   331,   332,
     340,   341,   352,   353,   362,   364,   365,   368,   371,   373,
     374,   379,   381,   384
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const yysigned_char yyrhs[] =
{
      66,     0,    -1,   110,    -1,    62,    -1,    61,    -1,    63,
      -1,    64,    -1,    21,    -1,    60,    -1,     7,    -1,    67,
      -1,    26,    81,    27,    -1,    68,    26,   106,    27,    -1,
      68,    31,    60,    -1,    68,    28,   106,    29,    -1,    93,
      -1,    11,    -1,    12,    28,    81,    29,    -1,    12,    28,
     106,     5,   106,    29,    -1,     9,    68,    -1,     6,    68,
      -1,    19,    -1,    20,    -1,    96,    -1,   107,    -1,    98,
      -1,   100,    -1,   102,    -1,   104,    -1,    87,    -1,    68,
      -1,    69,    -1,    70,    58,    -1,    70,    59,    -1,    70,
      -1,    58,    71,    -1,    59,    71,    -1,    47,    71,    -1,
      46,    71,    -1,    71,    -1,    72,    43,    72,    -1,    72,
      42,    72,    -1,    72,    -1,    73,    45,    73,    -1,    73,
      44,    73,    -1,    73,    -1,    74,    47,    74,    -1,    74,
      46,    74,    -1,    74,    -1,    75,    41,    75,    -1,    75,
      40,    75,    -1,    75,    39,    75,    -1,    75,    38,    75,
      -1,    75,    37,    75,    -1,    75,    36,    75,    -1,    75,
      35,    75,    -1,    75,    -1,    34,    76,    -1,    76,    -1,
      77,    33,    77,    -1,    77,    -1,    78,    32,    78,    -1,
      78,    -1,    79,    49,    79,    48,    79,    -1,    79,    -1,
      80,    57,    80,    -1,    80,    56,    80,    -1,    80,    55,
      80,    -1,    80,    54,    80,    -1,    80,    53,    80,    -1,
      80,    52,    80,    -1,    80,    51,    80,    -1,    80,    -1,
      81,    50,    81,    -1,    82,    50,    60,    -1,    60,    -1,
      82,    -1,    -1,    60,    -1,    -1,     4,    -1,     3,    -1,
      60,    -1,    86,    50,    60,    -1,    85,    86,    -1,    85,
      86,    57,    81,    -1,    -1,    85,    84,    88,    26,    83,
      27,    22,   110,    24,    -1,    80,    -1,    62,    57,    80,
      -1,    61,    57,    80,    -1,    63,    57,    80,    -1,    64,
      57,    80,    -1,    60,    57,    80,    -1,    -1,    85,    60,
      26,    90,    83,    27,    22,   110,    24,    -1,    89,    91,
      -1,    89,    -1,    91,    -1,    -1,    -1,    23,    94,    92,
      25,    -1,    14,   108,    -1,    -1,    -1,    13,    97,    80,
     108,    95,    -1,    -1,    15,    26,    99,    81,    27,   108,
      -1,    -1,    16,   108,    15,    26,   101,    81,    27,    -1,
      -1,    17,    26,   103,   106,    30,    81,    30,   106,    27,
     108,    -1,    -1,     8,    26,   105,    81,    10,    81,    27,
     108,    -1,    81,    -1,    -1,    18,    30,    -1,    18,    81,
      -1,    81,    -1,    -1,    22,   109,   110,    24,    -1,    30,
      -1,   110,   108,    -1,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short int yyrline[] =
{
       0,    71,    71,    74,    77,    80,    83,    86,    89,    92,
      97,   100,   103,   106,   110,   113,   116,   119,   122,   125,
     128,   131,   134,   137,   140,   143,   146,   149,   152,   157,
     160,   165,   168,   171,   176,   179,   182,   185,   188,   193,
     196,   199,   204,   207,   210,   215,   218,   221,   226,   229,
     232,   235,   238,   241,   244,   247,   252,   255,   260,   263,
     268,   271,   276,   279,   287,   290,   293,   296,   299,   302,
     305,   308,   313,   316,   321,   324,   329,   331,   335,   338,
     343,   346,   351,   354,   359,   362,   365,   365,   376,   384,
     392,   400,   408,   416,   424,   424,   440,   445,   450,   453,
     458,   458,   467,   470,   475,   475,   488,   488,   500,   500,
     512,   512,   526,   526,   539,   542,   547,   550,   555,   558,
     558,   565,   570,   573
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "T_GLOBAL", "T_LOCAL", "T_TO", "T_COUNT",
  "T_THIS", "T_FOREACH", "T_IS", "T_AS", "T_ARGC", "T_ARGV", "T_IF",
  "T_ELSE", "T_WHILE", "T_DO", "T_FOR", "T_RETURN", "T_CONTINUE",
  "T_BREAK", "T_NULL", "T_BEGIN", "T_T_BEGIN", "T_END", "T_T_END",
  "T_OPEN", "T_CLOSE", "T_S_OPEN", "T_S_CLOSE", "T_STATEMENT",
  "T_ACCESSOR", "T_OR", "T_AND", "T_NEGATION", "T_GREATER", "T_LESS",
  "T_GREATER_EQUAL", "T_LESS_EQUAL", "T_NOT_EQUAL", "T_EQUALITY",
  "T_EQUAL", "T_POWER", "T_MODULUS", "T_DIVIDE", "T_MULTIPLICATION",
  "T_SUBTRACTION", "T_ADDITION", "T_OTHERWISE", "T_TEST", "T_SEPARATOR",
  "T_POWER_ASSIGNMENT", "T_MODULUS_ASSIGNMENT", "T_DIVIDE_ASSIGNMENT",
  "T_MULTIPLY_ASSIGNMENT", "T_SUBTRACTIVE_ASSIGNMENT",
  "T_ADDITIVE_ASSIGNMENT", "T_ASSIGNMENT", "T_INCREMENTATION",
  "T_DECREMENTATION", "T_IDENTIFIER", "T_INTEGER", "T_BOOLEAN", "T_FLOAT",
  "T_STRING", "$accept", "ayul", "literal_value", "base_expression",
  "declaration_expression", "postfix_expression", "unary_expression",
  "operator3_expression", "operator2_expression", "operator_expression",
  "compare5_expression", "compare4_expression", "compare3_expression",
  "compare2_expression", "compare_expression", "assignment_expression",
  "expression", "function_argument_list",
  "function_optional_argument_list", "function_optional_name",
  "access_specifier", "definition_list", "definition", "@1",
  "table_element", "@2", "table_elements", "optional_table_elements",
  "table_expression", "@3", "if_optional_else_statement", "if_statement",
  "@4", "while_statement", "@5", "do_while_statement", "@6",
  "for_statement", "@7", "foreach_statement", "@8", "optional_expression",
  "return_statement", "statement", "@9", "statement_list", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short int yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,    65,    66,    67,    67,    67,    67,    67,    67,    67,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    68,
      68,    68,    68,    68,    68,    68,    68,    68,    68,    69,
      69,    70,    70,    70,    71,    71,    71,    71,    71,    72,
      72,    72,    73,    73,    73,    74,    74,    74,    75,    75,
      75,    75,    75,    75,    75,    75,    76,    76,    77,    77,
      78,    78,    79,    79,    80,    80,    80,    80,    80,    80,
      80,    80,    81,    81,    82,    82,    83,    83,    84,    84,
      85,    85,    86,    86,    87,    87,    88,    87,    89,    89,
      89,    89,    89,    89,    90,    89,    91,    91,    92,    92,
      94,    93,    95,    95,    97,    96,    99,    98,   101,   100,
     103,   102,   105,   104,   106,   106,   107,   107,   108,   109,
     108,   108,   110,   110
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     3,     4,     3,     4,     1,     1,     4,     6,     2,
       2,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     2,     2,     1,     2,     2,     2,     2,     1,
       3,     3,     1,     3,     3,     1,     3,     3,     1,     3,
       3,     3,     3,     3,     3,     3,     1,     2,     1,     3,
       1,     3,     1,     5,     1,     3,     3,     3,     3,     3,
       3,     3,     1,     3,     3,     1,     1,     0,     1,     0,
       1,     1,     1,     3,     2,     4,     0,     9,     1,     3,
       3,     3,     3,     3,     0,     9,     2,     1,     1,     0,
       0,     4,     2,     0,     0,     5,     0,     6,     0,     7,
       0,    10,     0,     8,     1,     0,     2,     2,     1,     0,
       4,     1,     2,     0
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned char yydefact[] =
{
     123,     0,     2,     1,    81,    80,     0,     9,     0,     0,
      16,     0,   104,     0,     0,     0,     0,    21,    22,     7,
     119,   100,     0,   121,     0,     0,     0,     0,     0,     8,
       4,     3,     5,     6,    10,    30,    31,    34,    39,    42,
      45,    48,    56,    58,    60,    62,    64,    72,   118,    79,
      29,    15,    23,    25,    26,    27,    28,    24,   122,    20,
     112,    19,   115,     0,   106,     0,   110,   116,   117,   123,
      99,     0,    57,    38,    37,    35,    36,   115,   115,     0,
      32,    33,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    82,    86,    84,     0,
     114,     0,     0,     0,     0,   115,     0,     8,     4,     3,
       5,     6,    88,    79,    97,    98,     0,    11,   114,     0,
       0,    13,    41,    40,    44,    43,    47,    46,    55,    54,
      53,    52,    51,    50,    49,    59,    61,     0,    71,    70,
      69,    68,    67,    66,    65,    73,     0,     0,     0,     0,
      17,   115,   103,     0,   108,     0,   120,     0,     0,     0,
       0,     0,    82,    96,   101,    12,    14,     0,    77,    83,
      85,     0,     0,     0,   105,     0,     0,     0,    93,    90,
      89,    91,    92,    94,    63,    75,    76,     0,     0,    18,
     102,   107,     0,     0,    77,     0,     0,     0,   109,   115,
       0,    74,   123,   113,     0,     0,     0,     0,   123,    87,
     111,     0,    95
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short int yydefgoto[] =
{
      -1,     1,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,   196,   197,   107,
      49,   108,    50,   156,   124,   204,   125,   126,    51,    70,
     184,    52,    63,    53,   113,    54,   186,    55,   115,    56,
     109,   111,    57,    58,    69,     2
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -96
static const short int yypact[] =
{
     -96,    61,   446,   -96,   -96,   -96,    17,   -96,    19,    17,
     -96,    59,   -96,    37,   446,    65,   508,   -96,   -96,   -96,
     -96,   -96,   570,   -96,   570,   694,   694,   694,   694,   -96,
     -96,   -96,   -96,   -96,   -96,    31,   -96,   -46,   -96,   -23,
      30,    43,    32,   -96,    25,    63,    48,    52,    49,    66,
     -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,    31,
     -96,    31,   570,   570,   -96,    86,   -96,   -96,    49,   -96,
     632,   -19,   -96,   -96,   -96,   -96,   -96,   570,   570,    69,
     -96,   -96,   694,   694,   694,   694,   694,   694,   694,   694,
     694,   694,   694,   694,   694,   570,   570,   570,   570,   570,
     570,   570,   570,   570,   570,   570,   104,   -96,   -40,   570,
      -2,   126,   198,   570,   106,   570,   260,    76,    77,    78,
      80,    81,    52,    79,   632,   -96,   115,   -96,    49,   114,
     113,   -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,
     -96,   -96,   -96,   -96,   -96,   -96,   -96,    45,    52,    52,
      52,    52,    52,    52,    52,   -96,   117,    87,   570,    -3,
     -96,   570,   132,   -11,   -96,   119,   -96,   570,   570,   570,
     570,   570,   125,   -96,   -96,   -96,   -96,   570,    92,   -96,
     -96,   570,   124,   446,   -96,   446,   570,   570,    52,    52,
      52,    52,    52,   -96,   -96,   -96,   105,   127,    -9,   -96,
     -96,   -96,    -6,    16,    92,    96,   135,   446,   -96,   570,
     131,   -96,   -96,   -96,   133,   137,   322,   446,   -96,   -96,
     -96,   384,   -96
};

/* YYPGOTO[NTERM-NUM].  */
static const short int yypgoto[] =
{
     -96,   -96,   -96,     5,   -96,   -96,    58,    35,    40,    41,
      22,   138,    68,    70,   -95,   -48,   -13,   -96,   -43,   -96,
     -64,   -96,   -96,   -96,   -96,   -96,    46,   -96,   -96,   -96,
     -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,   -96,
     -96,   -73,   -96,   -14,   -96,   -68
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -79
static const short int yytable[] =
{
      65,   116,   147,    68,   129,   130,   123,   181,   127,    71,
     157,    59,    80,    81,    61,   112,   185,   158,   207,    82,
      83,   208,   122,     6,     7,     8,     9,   160,    10,    11,
      12,   105,    13,    14,    15,    16,    17,    18,    19,   105,
      21,   105,   165,    22,   105,    60,   209,   105,   105,   110,
     148,   149,   150,   151,   152,   153,   154,    77,    95,    78,
     123,     3,    79,    64,   128,   128,   105,    88,    89,    90,
      91,    92,    93,    94,    84,    85,   122,    29,    30,    31,
      32,    33,   194,    73,    74,    75,    76,    62,   182,    86,
      87,    66,   155,   177,    97,    96,   159,    97,   162,   105,
     163,   114,   128,    98,    99,   100,   101,   102,   103,   104,
     138,   139,   140,   141,   142,   143,   144,   132,   133,   188,
     189,   190,   191,   192,   134,   135,   106,   136,   137,   131,
     -78,   161,   164,   167,   168,   169,   214,   170,   171,   172,
     174,   175,   176,   178,   216,   180,   183,   179,   128,   187,
     221,   193,   195,   199,   206,   205,   211,   212,   215,   218,
     217,   210,    72,   145,     0,     0,   146,     0,   198,   200,
     173,   201,     0,   202,   203,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   213,     0,     0,   128,     0,     0,     0,
       0,     4,     5,   220,     6,     7,     8,     9,     0,    10,
      11,    12,     0,    13,    14,    15,    16,    17,    18,    19,
      20,    21,     0,     0,    22,     0,     0,     0,    23,     0,
       0,     0,    24,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    25,    26,     0,     0,     0,    98,
      99,   100,   101,   102,   103,   104,    27,    28,    29,    30,
      31,    32,    33,     4,     5,     0,     6,     7,     8,     9,
       0,    10,    11,    12,     0,    13,    14,    15,    16,    17,
      18,    19,    20,    21,   166,     0,    22,     0,     0,     0,
      23,     0,     0,     0,    24,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    25,    26,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    27,    28,
      29,    30,    31,    32,    33,     4,     5,     0,     6,     7,
       8,     9,     0,    10,    11,    12,     0,    13,    14,    15,
      16,    17,    18,    19,    20,    21,   219,     0,    22,     0,
       0,     0,    23,     0,     0,     0,    24,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    25,    26,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      27,    28,    29,    30,    31,    32,    33,     4,     5,     0,
       6,     7,     8,     9,     0,    10,    11,    12,     0,    13,
      14,    15,    16,    17,    18,    19,    20,    21,   222,     0,
      22,     0,     0,     0,    23,     0,     0,     0,    24,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      25,    26,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    27,    28,    29,    30,    31,    32,    33,     4,
       5,     0,     6,     7,     8,     9,     0,    10,    11,    12,
       0,    13,    14,    15,    16,    17,    18,    19,    20,    21,
       0,     0,    22,     0,     0,     0,    23,     0,     0,     0,
      24,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    25,    26,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    27,    28,    29,    30,    31,    32,
      33,     4,     5,     0,     6,     7,     8,     9,     0,    10,
      11,    12,     0,    13,    14,    15,    16,    17,    18,    19,
       0,    21,     0,     0,    22,     0,     0,     0,    67,     0,
       0,     0,    24,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    25,    26,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    27,    28,    29,    30,
      31,    32,    33,     4,     5,     0,     6,     7,     8,     9,
       0,    10,    11,    12,     0,    13,    14,    15,    16,    17,
      18,    19,     0,    21,     0,     0,    22,     0,     0,     0,
       0,     0,     0,     0,    24,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    25,    26,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    27,    28,
      29,    30,    31,    32,    33,     4,     5,     0,     6,     7,
       8,     9,     0,    10,    11,    12,     0,    13,    14,    15,
      16,    17,    18,    19,     0,    21,     0,     0,    22,     0,
       0,     0,     0,     0,     0,     0,    24,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    25,    26,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      27,    28,   117,   118,   119,   120,   121,     4,     5,     0,
       6,     7,     8,     9,     0,    10,    11,    12,     0,    13,
      14,    15,    16,    17,    18,    19,     0,    21,     0,     0,
      22,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      25,    26,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    27,    28,    29,    30,    31,    32,    33
};

static const short int yycheck[] =
{
      14,    69,    97,    16,    77,    78,    70,    10,    27,    22,
      50,     6,    58,    59,     9,    63,    27,    57,    27,    42,
      43,    27,    70,     6,     7,     8,     9,    29,    11,    12,
      13,    50,    15,    16,    17,    18,    19,    20,    21,    50,
      23,    50,   115,    26,    50,    26,    30,    50,    50,    62,
      98,    99,   100,   101,   102,   103,   104,    26,    33,    28,
     124,     0,    31,    26,    77,    78,    50,    35,    36,    37,
      38,    39,    40,    41,    44,    45,   124,    60,    61,    62,
      63,    64,   177,    25,    26,    27,    28,    28,   161,    46,
      47,    26,   105,    48,    49,    32,   109,    49,   112,    50,
     113,    15,   115,    51,    52,    53,    54,    55,    56,    57,
      88,    89,    90,    91,    92,    93,    94,    82,    83,   167,
     168,   169,   170,   171,    84,    85,    60,    86,    87,    60,
      26,     5,    26,    57,    57,    57,   209,    57,    57,    60,
      25,    27,    29,    26,   212,   158,    14,    60,   161,    30,
     218,    26,    60,    29,    27,    50,    60,    22,    27,    22,
      27,   204,    24,    95,    -1,    -1,    96,    -1,   181,   183,
     124,   185,    -1,   186,   187,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   207,    -1,    -1,   209,    -1,    -1,    -1,
      -1,     3,     4,   217,     6,     7,     8,     9,    -1,    11,
      12,    13,    -1,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    -1,    -1,    26,    -1,    -1,    -1,    30,    -1,
      -1,    -1,    34,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    46,    47,    -1,    -1,    -1,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,     3,     4,    -1,     6,     7,     8,     9,
      -1,    11,    12,    13,    -1,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    -1,    26,    -1,    -1,    -1,
      30,    -1,    -1,    -1,    34,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    58,    59,
      60,    61,    62,    63,    64,     3,     4,    -1,     6,     7,
       8,     9,    -1,    11,    12,    13,    -1,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    -1,    26,    -1,
      -1,    -1,    30,    -1,    -1,    -1,    34,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      58,    59,    60,    61,    62,    63,    64,     3,     4,    -1,
       6,     7,     8,     9,    -1,    11,    12,    13,    -1,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    -1,
      26,    -1,    -1,    -1,    30,    -1,    -1,    -1,    34,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    58,    59,    60,    61,    62,    63,    64,     3,
       4,    -1,     6,     7,     8,     9,    -1,    11,    12,    13,
      -1,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      -1,    -1,    26,    -1,    -1,    -1,    30,    -1,    -1,    -1,
      34,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    46,    47,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    58,    59,    60,    61,    62,    63,
      64,     3,     4,    -1,     6,     7,     8,     9,    -1,    11,
      12,    13,    -1,    15,    16,    17,    18,    19,    20,    21,
      -1,    23,    -1,    -1,    26,    -1,    -1,    -1,    30,    -1,
      -1,    -1,    34,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    46,    47,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    58,    59,    60,    61,
      62,    63,    64,     3,     4,    -1,     6,     7,     8,     9,
      -1,    11,    12,    13,    -1,    15,    16,    17,    18,    19,
      20,    21,    -1,    23,    -1,    -1,    26,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    34,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    46,    47,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    58,    59,
      60,    61,    62,    63,    64,     3,     4,    -1,     6,     7,
       8,     9,    -1,    11,    12,    13,    -1,    15,    16,    17,
      18,    19,    20,    21,    -1,    23,    -1,    -1,    26,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    34,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    46,    47,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      58,    59,    60,    61,    62,    63,    64,     3,     4,    -1,
       6,     7,     8,     9,    -1,    11,    12,    13,    -1,    15,
      16,    17,    18,    19,    20,    21,    -1,    23,    -1,    -1,
      26,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      46,    47,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    58,    59,    60,    61,    62,    63,    64
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,    66,   110,     0,     3,     4,     6,     7,     8,     9,
      11,    12,    13,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    26,    30,    34,    46,    47,    58,    59,    60,
      61,    62,    63,    64,    67,    68,    69,    70,    71,    72,
      73,    74,    75,    76,    77,    78,    79,    80,    81,    85,
      87,    93,    96,    98,   100,   102,   104,   107,   108,    68,
      26,    68,    28,    97,    26,   108,    26,    30,    81,   109,
      94,    81,    76,    71,    71,    71,    71,    26,    28,    31,
      58,    59,    42,    43,    44,    45,    46,    47,    35,    36,
      37,    38,    39,    40,    41,    33,    32,    49,    51,    52,
      53,    54,    55,    56,    57,    50,    60,    84,    86,   105,
      81,   106,    80,    99,    15,   103,   110,    60,    61,    62,
      63,    64,    80,    85,    89,    91,    92,    27,    81,   106,
     106,    60,    72,    72,    73,    73,    74,    74,    75,    75,
      75,    75,    75,    75,    75,    77,    78,    79,    80,    80,
      80,    80,    80,    80,    80,    81,    88,    50,    57,    81,
      29,     5,   108,    81,    26,   106,    24,    57,    57,    57,
      57,    57,    60,    91,    25,    27,    29,    48,    26,    60,
      81,    10,   106,    14,    95,    27,   101,    30,    80,    80,
      80,    80,    80,    26,    79,    60,    82,    83,    81,    29,
     108,   108,    81,    81,    90,    50,    27,    27,    27,    30,
      83,    60,    22,   108,   106,    27,   110,    27,    22,    24,
     108,   110,    24
};

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab


/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    {								\
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;							\
    }								\
while (0)


#define YYTERROR	1
#define YYERRCODE	256


/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#define YYRHSLOC(Rhs, K) ((Rhs)[K])
#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)				\
    do									\
      if (N)								\
	{								\
	  (Current).first_line   = YYRHSLOC (Rhs, 1).first_line;	\
	  (Current).first_column = YYRHSLOC (Rhs, 1).first_column;	\
	  (Current).last_line    = YYRHSLOC (Rhs, N).last_line;		\
	  (Current).last_column  = YYRHSLOC (Rhs, N).last_column;	\
	}								\
      else								\
	{								\
	  (Current).first_line   = (Current).last_line   =		\
	    YYRHSLOC (Rhs, 0).last_line;				\
	  (Current).first_column = (Current).last_column =		\
	    YYRHSLOC (Rhs, 0).last_column;				\
	}								\
    while (0)
#endif


/* YY_LOCATION_PRINT -- Print the location on the stream.
   This macro was not mandated originally: define only if we know
   we won't break user code: when these are the locations we know.  */

#ifndef YY_LOCATION_PRINT
# if YYLTYPE_IS_TRIVIAL
#  define YY_LOCATION_PRINT(File, Loc)			\
     fprintf (File, "%d.%d-%d.%d",			\
              (Loc).first_line, (Loc).first_column,	\
              (Loc).last_line,  (Loc).last_column)
# else
#  define YY_LOCATION_PRINT(File, Loc) ((void) 0)
# endif
#endif


/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr,					\
                  Type, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short int *bottom, short int *top)
#else
static void
yy_stack_print (bottom, top)
    short int *bottom;
    short int *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned long int yylno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu), ",
             yyrule - 1, yylno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname[yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname[yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      size_t yyn = 0;
      char const *yyp = yystr;

      for (;;)
	switch (*++yyp)
	  {
	  case '\'':
	  case ',':
	    goto do_not_strip_quotes;

	  case '\\':
	    if (*++yyp != '\\')
	      goto do_not_strip_quotes;
	    /* Fall through.  */
	  default:
	    if (yyres)
	      yyres[yyn] = *yyp;
	    yyn++;
	    break;

	  case '"':
	    if (yyres)
	      yyres[yyn] = '\0';
	    return yyn;
	  }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

#endif /* YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);


# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yymsg, yytype, yyvaluep)
    const char *yymsg;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The look-ahead symbol.  */
int yychar;

/* The semantic value of the look-ahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()
    ;
#endif
#endif
{
  
  int yystate;
  int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Look-ahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short int yyssa[YYINITDEPTH];
  short int *yyss = yyssa;
  short int *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short int *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow (YY_("memory exhausted"),
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short int *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyexhaustedlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a look-ahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to look-ahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a look-ahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid look-ahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the look-ahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 71 "grammar.cxx"
    { }
    break;

  case 3:
#line 74 "grammar.cxx"
    {
		(yyval.n) = alNewLiteralNode(AL_LITERAL_BOOLEAN, &(yyvsp[0].i));
	}
    break;

  case 4:
#line 77 "grammar.cxx"
    {
		(yyval.n) = alNewLiteralNode(AL_LITERAL_INTEGER, &(yyvsp[0].i));
	}
    break;

  case 5:
#line 80 "grammar.cxx"
    {
		(yyval.n) = alNewLiteralNode(AL_LITERAL_VALUE, &(yyvsp[0].f));
	}
    break;

  case 6:
#line 83 "grammar.cxx"
    {
 		(yyval.n) = alNewLiteralNode(AL_LITERAL_STRING, &(yyvsp[0].s));
	}
    break;

  case 7:
#line 86 "grammar.cxx"
    {
		(yyval.n) = alNewLiteralNode(AL_LITERAL_AUTO, NULL);
	}
    break;

  case 8:
#line 89 "grammar.cxx"
    {
		(yyval.n) = alNewSymbolNode((yyvsp[0].id));
	}
    break;

  case 9:
#line 92 "grammar.cxx"
    {
		(yyval.n) = alNewThisNode();
	}
    break;

  case 10:
#line 97 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 11:
#line 100 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[-1].n);
	}
    break;

  case 12:
#line 103 "grammar.cxx"
    {
		(yyval.n) = alNewCallNode((yyvsp[-3].n), (yyvsp[-1].n));
	}
    break;

  case 13:
#line 106 "grammar.cxx"
    {
		char *id = strdup((yyvsp[0].id));
		(yyval.n) = alNewOfNode((yyvsp[-2].n), alNewLiteralNode(AL_LITERAL_STRING, &id));
	}
    break;

  case 14:
#line 110 "grammar.cxx"
    {
		(yyval.n) = alNewOfNode((yyvsp[-3].n), (yyvsp[-1].n));
	}
    break;

  case 15:
#line 113 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 16:
#line 116 "grammar.cxx"
    {
		(yyval.n) = alNewArgcNode();
	}
    break;

  case 17:
#line 119 "grammar.cxx"
    {
		(yyval.n) = alNewArgvNode((yyvsp[-1].n), (yyvsp[-1].n));
	}
    break;

  case 18:
#line 122 "grammar.cxx"
    {
		(yyval.n) = alNewArgvNode((yyvsp[-3].n), (yyvsp[-1].n));
	}
    break;

  case 19:
#line 125 "grammar.cxx"
    {
		(yyval.n) = alNewIsNode((yyvsp[0].n));
	}
    break;

  case 20:
#line 128 "grammar.cxx"
    {
		(yyval.n) = alNewCountNode((yyvsp[0].n));
	}
    break;

  case 21:
#line 131 "grammar.cxx"
    {
		(yyval.n) = alNewContinueNode();
	}
    break;

  case 22:
#line 134 "grammar.cxx"
    {
		(yyval.n) = alNewBreakNode();
	}
    break;

  case 23:
#line 137 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 24:
#line 140 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 25:
#line 143 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 26:
#line 146 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 27:
#line 149 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 28:
#line 152 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 29:
#line 157 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 30:
#line 160 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 31:
#line 165 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 32:
#line 168 "grammar.cxx"
    {
		(yyval.n) = alNewPostfixNode(T_INCREMENTATION, (yyvsp[-1].n));
	}
    break;

  case 33:
#line 171 "grammar.cxx"
    {
		(yyval.n) = alNewPostfixNode(T_DECREMENTATION, (yyvsp[-1].n));
	}
    break;

  case 34:
#line 176 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 35:
#line 179 "grammar.cxx"
    {
		(yyval.n) = alNewUnaryNode(T_INCREMENTATION, (yyvsp[0].n));
	}
    break;

  case 36:
#line 182 "grammar.cxx"
    {
		(yyval.n) = alNewUnaryNode(T_DECREMENTATION, (yyvsp[0].n));
	}
    break;

  case 37:
#line 185 "grammar.cxx"
    {
		(yyval.n) = alNewUnaryNode(T_ADDITION, (yyvsp[0].n));
	}
    break;

  case 38:
#line 188 "grammar.cxx"
    {
		(yyval.n) = alNewUnaryNode(T_SUBTRACTION, (yyvsp[0].n));
	}
    break;

  case 39:
#line 193 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 40:
#line 196 "grammar.cxx"
    {
		(yyval.n) = alNewOperatorNode(T_MODULUS, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 41:
#line 199 "grammar.cxx"
    {
		(yyval.n) = alNewOperatorNode(T_POWER, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 42:
#line 204 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 43:
#line 207 "grammar.cxx"
    {
		(yyval.n) = alNewOperatorNode(T_MULTIPLICATION, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 44:
#line 210 "grammar.cxx"
    {
		(yyval.n) = alNewOperatorNode(T_DIVIDE, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 45:
#line 215 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 46:
#line 218 "grammar.cxx"
    {
		(yyval.n) = alNewOperatorNode(T_ADDITION, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 47:
#line 221 "grammar.cxx"
    {
		(yyval.n) = alNewOperatorNode(T_SUBTRACTION, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 48:
#line 226 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 49:
#line 229 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_EQUAL, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 50:
#line 232 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_EQUALITY, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 51:
#line 235 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_NOT_EQUAL, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 52:
#line 238 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_LESS_EQUAL, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 53:
#line 241 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_GREATER_EQUAL, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 54:
#line 244 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_LESS, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 55:
#line 247 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_GREATER, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 56:
#line 252 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 57:
#line 255 "grammar.cxx"
    {
		(yyval.n) = alNewUnaryNode(T_NEGATION, (yyvsp[0].n));
	}
    break;

  case 58:
#line 260 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 59:
#line 263 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_AND, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 60:
#line 268 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 61:
#line 271 "grammar.cxx"
    {
		(yyval.n) = alNewCompareNode(T_OR, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 62:
#line 276 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 63:
#line 279 "grammar.cxx"
    {
		(yyval.n) = alNewIfNode();
		(yyval.n)->If.Expr = (yyvsp[-4].n);
		(yyval.n)->If.True = (yyvsp[-2].n);
		(yyval.n)->If.False = (yyvsp[0].n);
	}
    break;

  case 64:
#line 287 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 65:
#line 290 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 66:
#line 293 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_ADDITIVE_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 67:
#line 296 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_SUBTRACTIVE_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 68:
#line 299 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_MULTIPLY_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 69:
#line 302 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_DIVIDE_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 70:
#line 305 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_MODULUS_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 71:
#line 308 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode(T_POWER_ASSIGNMENT, (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 72:
#line 313 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 73:
#line 316 "grammar.cxx"
    {
		alAddAtEndOfNode((yyval.n) = (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 74:
#line 321 "grammar.cxx"
    {
		alNewDefArgument(alCurrentDef, (yyvsp[0].id));
	}
    break;

  case 75:
#line 324 "grammar.cxx"
    {
		alNewDefArgument(alCurrentDef, (yyvsp[0].id));
	}
    break;

  case 76:
#line 329 "grammar.cxx"
    {
	}
    break;

  case 77:
#line 331 "grammar.cxx"
    {
	}
    break;

  case 78:
#line 335 "grammar.cxx"
    {
		strncpy((yyval.id), (yyvsp[0].id), sizeof((yyval.id)));
	}
    break;

  case 79:
#line 338 "grammar.cxx"
    {
		(yyval.id)[0] = 0;
	}
    break;

  case 80:
#line 343 "grammar.cxx"
    {
		(yyval.i) = alCurrentAccessSpecifier = T_LOCAL;
	}
    break;

  case 81:
#line 346 "grammar.cxx"
    {
		(yyval.i) = alCurrentAccessSpecifier = T_GLOBAL;
	}
    break;

  case 82:
#line 351 "grammar.cxx"
    {
		(yyval.n) = alNewDefNode(alNewNodeDef(alCurrentNode, alCurrentAccessSpecifier, (yyvsp[0].id)));
	}
    break;

  case 83:
#line 354 "grammar.cxx"
    {
		alAddAtEndOfNode((yyval.n) = (yyvsp[-2].n), alNewDefNode(alNewNodeDef(alCurrentNode, alCurrentAccessSpecifier, (yyvsp[0].id))));
	}
    break;

  case 84:
#line 359 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 85:
#line 362 "grammar.cxx"
    {
		(yyval.n) = alNewAssignmentNode((yyvsp[-3].i), (yyvsp[-2].n), (yyvsp[0].n));
	}
    break;

  case 86:
#line 365 "grammar.cxx"
    {
		alPushCurDef((yyval.d) = alNewNodeDef(alCurrentNode, (yyvsp[-1].i), (yyvsp[0].id)));
		alPushCurNode((yyval.d)->Code = alNewNullNode());
	}
    break;

  case 87:
#line 369 "grammar.cxx"
    {
		assert(alPopCurDef == (yyvsp[-6].d));
		assert(alPopCurNode == (yyvsp[-6].d)->Code);
		(yyval.n) = alNewDefNode((yyvsp[-6].d));
	}
    break;

  case 88:
#line 376 "grammar.cxx"
    {
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreate();
		(yyval.vn)->ValueNode = (yyvsp[0].n);
	}
    break;

  case 89:
#line 384 "grammar.cxx"
    {
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreateBoolean((yyvsp[-2].i));
		(yyval.vn)->ValueNode = (yyvsp[0].n);
	}
    break;

  case 90:
#line 392 "grammar.cxx"
    {
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreateInteger((yyvsp[-2].i));
		(yyval.vn)->ValueNode = (yyvsp[0].n);
	}
    break;

  case 91:
#line 400 "grammar.cxx"
    {
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreateValue((yyvsp[-2].f));
		(yyval.vn)->ValueNode = (yyvsp[0].n);
	}
    break;

  case 92:
#line 408 "grammar.cxx"
    {
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreateString((yyvsp[-2].s));
		(yyval.vn)->ValueNode = (yyvsp[0].n);
	}
    break;

  case 93:
#line 416 "grammar.cxx"
    {
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreateString(strdup((yyvsp[-2].id)));
		(yyval.vn)->ValueNode = (yyvsp[0].n);
	}
    break;

  case 94:
#line 424 "grammar.cxx"
    {
		alPushCurDef((yyval.d) = alNewNodeDef(alCurrentNode, (yyvsp[-2].i), NULL));
		alPushCurNode((yyval.d)->Code = alNewNullNode());
	}
    break;

  case 95:
#line 428 "grammar.cxx"
    {
		assert(alPopCurDef == (yyvsp[-5].d));
		assert(alPopCurNode == (yyvsp[-5].d)->Code);
		(yyval.vn) = malloc(sizeof(alValue));
		if(!(yyval.vn))
			YYABORT;
		memset((yyval.vn), 0, sizeof(alValue));
		(yyval.vn)->Key = alLiteralCreateString(strdup((yyvsp[-7].id)));
		(yyval.vn)->ValueNode = alNewDefNode((yyvsp[-5].d));
	}
    break;

  case 96:
#line 440 "grammar.cxx"
    {
		(yyval.vn) = (yyvsp[-1].vn);
		(yyval.vn)->Next = (yyvsp[0].vn);
		(yyvsp[0].vn)->Prev = (yyval.vn);
	}
    break;

  case 97:
#line 445 "grammar.cxx"
    {
		(yyval.vn) = (yyvsp[0].vn);
	}
    break;

  case 98:
#line 450 "grammar.cxx"
    {
		(yyval.vn) = (yyvsp[0].vn);
	}
    break;

  case 99:
#line 453 "grammar.cxx"
    {
		(yyval.vn) = NULL;
	}
    break;

  case 100:
#line 458 "grammar.cxx"
    { 
		alPushCurNode((yyval.n) = alNewTableNode());
	}
    break;

  case 101:
#line 461 "grammar.cxx"
    {
		assert(alPopCurNode == ((yyval.n) = (yyvsp[-2].n)));
		(yyval.n)->Table.Values = (yyvsp[-1].vn);
	}
    break;

  case 102:
#line 467 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 103:
#line 470 "grammar.cxx"
    {
		(yyval.n) = NULL;
	}
    break;

  case 104:
#line 475 "grammar.cxx"
    { 
		(yyval.n) = alNewNullNode();
		alPushCurNode((yyval.n)->Null.Expr = alNewIfNode());
	}
    break;

  case 105:
#line 479 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[-3].n);
		assert(alPopCurNode == (yyval.n)->Null.Expr);
		(yyval.n)->Null.Expr->If.Expr = (yyvsp[-2].n);
		(yyval.n)->Null.Expr->If.True = alNewEndNode((yyvsp[-1].n));
		(yyval.n)->Null.Expr->If.False = alNewEndNode((yyvsp[0].n));
	}
    break;

  case 106:
#line 488 "grammar.cxx"
    { 
		(yyval.n) = alNewNullNode();
		alPushCurNode((yyval.n)->Null.Expr = alNewWhileNode());
	}
    break;

  case 107:
#line 492 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[-3].n);
		assert(alPopCurNode == (yyval.n)->Null.Expr);
		(yyval.n)->Null.Expr->While.Expr = (yyvsp[-2].n);
		(yyval.n)->Null.Expr->While.Loop = alNewEndNode((yyvsp[0].n));
	}
    break;

  case 108:
#line 500 "grammar.cxx"
    { 
		(yyval.n) = alNewNullNode();
		alPushCurNode((yyval.n)->Null.Expr = alNewDoWhileNode());
	}
    break;

  case 109:
#line 504 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[-2].n);
		assert(alPopCurNode == (yyval.n)->Null.Expr);
		(yyval.n)->Null.Expr->DoWhile.Loop = alNewEndNode((yyvsp[-5].n));
		(yyval.n)->Null.Expr->DoWhile.Expr = (yyvsp[-1].n);
	}
    break;

  case 110:
#line 512 "grammar.cxx"
    { 
		(yyval.n) = alNewNullNode();
		alPushCurNode((yyval.n)->Null.Expr = alNewForNode());
	}
    break;

  case 111:
#line 516 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[-7].n);
		assert(alPopCurNode == (yyval.n)->Null.Expr);
		(yyval.n)->Null.Expr->For.Init = alNewEndNode((yyvsp[-6].n));
		(yyval.n)->Null.Expr->For.Expr = (yyvsp[-4].n);
		(yyval.n)->Null.Expr->For.Next = alNewEndNode((yyvsp[-2].n));
		(yyval.n)->Null.Expr->For.Loop = alNewEndNode((yyvsp[0].n));
	}
    break;

  case 112:
#line 526 "grammar.cxx"
    { 
		(yyval.n) = alNewNullNode();
		alPushCurNode((yyval.n)->Null.Expr = alNewForEachNode());
	}
    break;

  case 113:
#line 530 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[-5].n);
		assert(alPopCurNode == (yyval.n)->Null.Expr);
		(yyval.n)->Null.Expr->ForEach.Table = (yyvsp[-4].n);
		(yyval.n)->Null.Expr->ForEach.As = (yyvsp[-2].n);
		(yyval.n)->Null.Expr->ForEach.Loop = alNewEndNode((yyvsp[0].n));
	}
    break;

  case 114:
#line 539 "grammar.cxx"
    {
		(yyval.n) = (yyvsp[0].n);
	}
    break;

  case 115:
#line 542 "grammar.cxx"
    {
		(yyval.n) = NULL;
	}
    break;

  case 116:
#line 547 "grammar.cxx"
    {
		(yyval.n) = alNewReturnNode(NULL);
	}
    break;

  case 117:
#line 550 "grammar.cxx"
    {
		(yyval.n) = alNewReturnNode((yyvsp[0].n));
	}
    break;

  case 118:
#line 555 "grammar.cxx"
    {
		(yyval.n) = alNewEndNode((yyvsp[0].n));
	}
    break;

  case 119:
#line 558 "grammar.cxx"
    { 
		(yyval.n) = alNewNullNode(); 
		alPushCurNode((yyval.n)->Null.Expr = alNewNullNode());
	}
    break;

  case 120:
#line 562 "grammar.cxx"
    {
		assert(alPopCurNode == ((yyval.n) = (yyvsp[-2].n))->Null.Expr);
	}
    break;

  case 121:
#line 565 "grammar.cxx"
    {
		(yyval.n) = NULL;
	}
    break;

  case 122:
#line 570 "grammar.cxx"
    {
		alAddAtEndOfNode(alCurrentNode, (yyvsp[0].n));
	}
    break;

  case 123:
#line 573 "grammar.cxx"
    {
	}
    break;


      default: break;
    }

/* Line 1126 of yacc.c.  */
#line 2511 "grammar.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  int yytype = YYTRANSLATE (yychar);
	  YYSIZE_T yysize0 = yytnamerr (0, yytname[yytype]);
	  YYSIZE_T yysize = yysize0;
	  YYSIZE_T yysize1;
	  int yysize_overflow = 0;
	  char *yymsg = 0;
#	  define YYERROR_VERBOSE_ARGS_MAXIMUM 5
	  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
	  int yyx;

#if 0
	  /* This is so xgettext sees the translatable formats that are
	     constructed on the fly.  */
	  YY_("syntax error, unexpected %s");
	  YY_("syntax error, unexpected %s, expecting %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s");
	  YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s");
#endif
	  char *yyfmt;
	  char const *yyf;
	  static char const yyunexpected[] = "syntax error, unexpected %s";
	  static char const yyexpecting[] = ", expecting %s";
	  static char const yyor[] = " or %s";
	  char yyformat[sizeof yyunexpected
			+ sizeof yyexpecting - 1
			+ ((YYERROR_VERBOSE_ARGS_MAXIMUM - 2)
			   * (sizeof yyor - 1))];
	  char const *yyprefix = yyexpecting;

	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  int yyxbegin = yyn < 0 ? -yyn : 0;

	  /* Stay within bounds of both yycheck and yytname.  */
	  int yychecklim = YYLAST - yyn;
	  int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
	  int yycount = 1;

	  yyarg[0] = yytname[yytype];
	  yyfmt = yystpcpy (yyformat, yyunexpected);

	  for (yyx = yyxbegin; yyx < yyxend; ++yyx)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      {
		if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
		  {
		    yycount = 1;
		    yysize = yysize0;
		    yyformat[sizeof yyunexpected - 1] = '\0';
		    break;
		  }
		yyarg[yycount++] = yytname[yyx];
		yysize1 = yysize + yytnamerr (0, yytname[yyx]);
		yysize_overflow |= yysize1 < yysize;
		yysize = yysize1;
		yyfmt = yystpcpy (yyfmt, yyprefix);
		yyprefix = yyor;
	      }

	  yyf = YY_(yyformat);
	  yysize1 = yysize + yystrlen (yyf);
	  yysize_overflow |= yysize1 < yysize;
	  yysize = yysize1;

	  if (!yysize_overflow && yysize <= YYSTACK_ALLOC_MAXIMUM)
	    yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg)
	    {
	      /* Avoid sprintf, as that infringes on the user's name space.
		 Don't have undefined behavior even if the translation
		 produced a string with the wrong number of "%s"s.  */
	      char *yyp = yymsg;
	      int yyi = 0;
	      while ((*yyp = *yyf))
		{
		  if (*yyp == '%' && yyf[1] == 's' && yyi < yycount)
		    {
		      yyp += yytnamerr (yyp, yyarg[yyi++]);
		      yyf += 2;
		    }
		  else
		    {
		      yyp++;
		      yyf++;
		    }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    {
	      yyerror (YY_("syntax error"));
	      goto yyexhaustedlab;
	    }
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror (YY_("syntax error"));
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

      if (yychar <= YYEOF)
        {
	  /* Return failure if at end of input.  */
	  if (yychar == YYEOF)
	    YYABORT;
        }
      else
	{
	  yydestruct ("Error: discarding", yytoken, &yylval);
	  yychar = YYEMPTY;
	}
    }

  /* Else will try to reuse look-ahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (0)
     goto yyerrorlab;

yyvsp -= yylen;
  yyssp -= yylen;
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;


      yydestruct ("Error: popping", yystos[yystate], yyvsp);
      YYPOPSTACK;
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  *++yyvsp = yylval;


  /* Shift the error token. */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEOF && yychar != YYEMPTY)
     yydestruct ("Cleanup: discarding lookahead",
		 yytoken, &yylval);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
		  yystos[*yyssp], yyvsp);
      YYPOPSTACK;
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 576 "grammar.cxx"


