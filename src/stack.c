//------------------------------------//
//
// stack.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-7
//
//------------------------------------//

#include "stdafx.h"

alLiteral *alPeek(alState *state, int top) {
	int at;
	
	// Oblicz pozycj� na stosie
	at = state->StackSize - top - 1;

	// Pobierz warto�� na stosie
	return 0 <= at && at < state->StackSize ? &state->Stack[at] : NULL;
}

alLiteral *alPeekArgv(alState *state, int argv) {
	int at;

	// Oblicz pozycj� na stosie
	at = state->At - argv - 1;

	// Pobierz warto�� na stosie
	return 0 <= at && at < state->StackSize ? &state->Stack[at] : NULL;
}

alBoolean alIsOfType(alState *state, int top, int type) {
	alLiteral *value;

	// Pobierz warto�� znajduj�c� sie na stosie)
	if(value = alPeek(state, top)) {
		// Por�wnaj typy
		return value->Type == type;
	}
	return alFalse;
}

alBoolean alIsOfTypeArgv(alState *state, int argv, int type) {
	alLiteral *value;

	// Pobierz warto�� znajduj�c� sie na stosie)
	if(value = alPeekArgv(state, argv)) {
		// Por�wnaj typy
		return value->Type == type;
	}
	return alFalse;
}

int alPush(alState *state, alLiteral value) {
	// Sprawd�, pojemno�� stosu
	if(state->StackSize >= state->MaxStackSize) {
		state->Status = AL_STATUS_STACK_OVERFLOW_ERROR;
		return -1;
	}

	// Dodaj warto�� na stos
	state->Stack[state->StackSize++] = value;
	return 0;
}

alLiteral alPop(alState *state, int top) {
	alLiteral*	valuep;
	alLiteral		value;

	// Pobierz warto�� na podanym miejscu
	valuep = alPeek(state, top);
	value = valuep ? *valuep : alLiteralCreate();

	// Zdejmij jedn� warto��
	alRemove(state, top, 1);
	return value;
}

int alRemove(alState *state, int top, int count) {
	int i, at;

	// Sprawd�, ile element�w mamy do usuni�cia
	if(count <= 0)
		return 0;

	// Sprawd�, czy mie�cimy si� w stosie
	if(top < 0 || top > state->StackSize || count > state->StackSize - top) {
		state->Status = AL_STATUS_STACK_UNDERFLOW_ERROR;
		return -1;
	}

	// Oblicz pocz�tkow� pozycj� na stosie
	at = state->StackSize - top - count;

	// Przenie� na nowe miejsce
	for(i = 0; i < top; i++) 
		state->Stack[at + i] = state->Stack[at + i + count];

	// Zmniejsz ilo�� element�w na stosie
	state->StackSize -= count;
	return count;
}

