//------------------------------------//
//
// def.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-20
//
//------------------------------------//

#include "stdafx.h"

int alDefGetName(alDef *def, alString name) {
	if(!def)
		return -1;

	// Skopiuj nazw�
	strncpy(name, def->Name, sizeof(alString));
	return 0;
}

alBoolean alDefIsCallback(alDef *def) {
	if(!def)
		return -1;
	return (def->Code || def->Code2) ? alTrue : alFalse;
}

alBoolean alDefIsGlobal(alDef *def) {
	if(!def)
		return -1;

	// Definicja globalna
	return def->Type == T_GLOBAL ? alTrue : alFalse;
}
