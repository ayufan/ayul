//------------------------------------//
//
// ayule.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayule
// Date: 2006-4-20
//
//------------------------------------//

#include "../ayul.h"

//------------------------------------//
// Simple Defintions for ayul

int print_def(alState *state) {
	int i;

	for(i = 0; i < alStateGetArgs(state); i++) {
		char *buf = alPeekStringArgv(state, i);
		if(buf) {
			fwrite(buf, 1, strlen(buf), stdout);
		}
	}
	return 0;
}

int ask_def(alState *state) {
	char buffer[20480];
	int i;

	fgets(buffer, sizeof(buffer), stdin);
	// Trim from right side
	for(i = (int)strlen(buffer) - 1; i >= 0; i--) {
		if(buffer[i] <= 32)
			buffer[i] = 0;
	}
	alPushString(state, alMemoryStrDup(buffer));
	return 1;
}

int system_def(alState *state) {
	char *command = alPeekStringArgv(state, 0);

	if(command == NULL)
		return -1;
	alPushInteger(state, system(command));
	return 1;
}

int clear_def(alState *state) {
#ifdef WIN32
	system("cls");
#else
	system("clear");
#endif
	return 0;
}

int pause_def(alState *state) {
#ifdef WIN32
	system("pause");
#else
	alString buf;

	fgets(buf, sizeof(alString), stdin);
#endif
	return 0;
}

int toboolean_def(alState *state) {
	int i;

	for(i = 0; i < alStateGetArgs(state); i++) {
		alPushBoolean(state, alPeekBooleanArgv(state, i));
	}
	return i;
}

int tointeger_def(alState *state) {
	int i;

	for(i = 0; i < alStateGetArgs(state); i++) {
		alPushInteger(state, alPeekIntegerArgv(state, i));
	}
	return i;
}

int tovalue_def(alState *state) {
	int i;

	for(i = 0; i < alStateGetArgs(state); i++) {
		alPushValue(state, alPeekValueArgv(state, i));
	}
	return i;
}

int tostring_def(alState *state) {
	int i;

	for(i = 0; i < alStateGetArgs(state); i++) {
		char *buf = alPeekStringArgv(state, i);
		if(buf) {
			alPushString(state, buf);
		}
		else
			alPushString(state, "");
	}
	return i;
}

int rand_def(alState *state) {
	if(alStateGetArgs(state) != 1) {
		return AL_STATUS_INVALID_ARGUMENT_ERROR;
	}

	// Wylosuj liczb� i wrzu� j� na stos
	alPushInteger(state, rand() % alPeekIntegerArgv(state, 0));
	return 1;
}

void aleInit() {
	// Podstawowe definicje systemowe
	alTreeAddGlobalCallbackDef("print", print_def);
	alTreeAddGlobalCallbackDef("ask", ask_def);
	alTreeAddGlobalCallbackDef("system", system_def);
	alTreeAddGlobalCallbackDef("clear", clear_def);
	alTreeAddGlobalCallbackDef("pause", pause_def);
	alTreeAddGlobalCallbackDef("rand", rand_def);

	// Podstawowe definicje do typ�w danych
	alTreeAddGlobalCallbackDef("toboolean", toboolean_def);
	alTreeAddGlobalCallbackDef("tointeger", tointeger_def);
	alTreeAddGlobalCallbackDef("tovalue", tovalue_def);
	alTreeAddGlobalCallbackDef("tostring", tostring_def);
}

//------------------------------------//
// Main

int main(int argc, char *argv[]) {
	int result = 0, i;
	alState *state;
	alDef *main;
	alString buf;

	// Informacje o �rodowisku
	printf(AL_TITLE " sandbox by " AL_AUTHOR " version " AL_VERSION "\n");

	// Sprawd� ilo�� argument�w
	if(argc <= 1) {
		printf("\n\nUsage: %s [Code1.a] [Code2.a] [Code3.a] ...\n\n", argv[0]);
#ifdef WIN32
	system("pause");
#endif
		return -1;
	}

	printf("...\n");

	// Utw�rz nowe drzewko wyra�e�
	alTreeNew();

	// Dodaj definicje do drzewa
	aleInit();

	// Za�aduj pliki
	for(i = 1; i < argc; i++) {
		// Komunikat
		printf("Parsing %s...\n", argv[i]);
		
		// Za�aduj do drzewa
		if(alTreeLoadInto(argv[i]) < 0) {
			result = -1;
		}
	}
	if(result)
		goto end;

	printf("Building...\n");
	// Kompiluj drzewo
	result = alTreeBuild();
	if(result)
		goto end;

	// Utw�rz stan
	state = alStateNew(alTreeGetActive(), 0);
	if(!state) {
		printf("Couldn't create state !\n");
		goto end;
	}

	// Znajd� g��wn� funkcj�
	main = alTreeFindGlobalDef(alTreeGetActive(), NULL);
	if(!main || !alDefIsCallback(main)) {
		printf("Couldn't find main def !\n");
		goto end_state;
	}

	printf("Executing...\n");

	// Uruchom funkcj� (bez argument�w)
	alExecute(state, main, NULL, 0);

	// Sprawd� wynik
	if(alStateGetStatus(state) <= AL_STATUS_ERROR) {
		printf("Execution stopped with errors\n");

		// Wypisz informacje o b��dzie
		printf("code %i", alStateGetStatus(state));
		if(alStateGetErrorFile(state, buf) >= 0)
			printf(" at %s", buf);
		if(alStateGetErrorLine(state) >= 0)
			printf(" (%i)", alStateGetErrorLine(state));
		if(alStateGetErrorDef(state, buf) >= 0)
			printf(" def %s", buf);
		printf("\n");
		result = -1;
	}
	else {
		printf("Returned %i arg(s)\n", alStateGetReturns(state));

		// Wy�wietl zwr�cone warto�ci
		for(i = 0; i < alStateGetReturns(state); i++) {
			printf("Return(%i): %s\n", i, alLiteralToString(alPeek(state, alStateGetReturns(state) - i - 1)));
		}

		// Zdejmij ze stosu warto�ci
		alRemove(state, 0, alStateGetReturns(state));
	}

end_state:
	// Zniszcz stan
	alStateDestroy(state);

	// Uruchom GarbageCollectora
	printf("gc(count): %i\n", alMemoryCount());
	printf("gc(collect): %i\n", alMemoryCollect());
	printf("gc(left): %i\n", alMemoryCount());

end:
	// Poka� komunikat z b��dem
	if(result < 0) {
		printf("Found %i error(s). Stopping !\n", -result);
	}

	// Zniszcz drzewo
	alTreeDestroy(alTreeGetActive());
#ifdef WIN32
	system("pause");
#endif
	return result;
}
