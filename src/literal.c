//------------------------------------//
//
// literal.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-14
//
//------------------------------------//

#include "stdafx.h"

alLiteral alLiteralCreate() {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	return newLiteral;
}

alLiteral alLiteralCreateBoolean(alBoolean flag) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_BOOLEAN;
	newLiteral.Integer = flag;
	return newLiteral;
}

alLiteral alLiteralCreateInteger(int value) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_INTEGER;
	newLiteral.Integer = value;
	return newLiteral;
}

alLiteral alLiteralCreateValue(float value) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_VALUE;
	newLiteral.Value = value;
	return newLiteral;
}

alLiteral alLiteralCreateString(const char *name) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_STRING;
	newLiteral.String = (char*)name;
	return newLiteral;
}

alLiteral alLiteralCreatePointer(void *data) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_POINTER;
	newLiteral.Pointer = data;
	return newLiteral;
}

alLiteral alLiteralCreateDef(alDef *def) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_DEF;
	newLiteral.Def = def;
	return newLiteral;
}

alLiteral alLiteralCreateLiteral(alLiteral *value) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_LITERAL;
	newLiteral.Literal = value;
	return newLiteral;
}

alLiteral alLiteralCreateTable(alTable *table) {
	alLiteral newLiteral;

	memset(&newLiteral, 0, sizeof(newLiteral));
	newLiteral.Type = AL_LITERAL_TABLE;
	newLiteral.Table = table;
	return newLiteral;
}

void alLiteralMark(alLiteral *value) {
	if(!value)
		return;

	// Oznacz wska�nik
	if(value->Type == AL_LITERAL_POINTER || value->Type == AL_LITERAL_STRING || value->Type == AL_LITERAL_TABLE || value->Type == AL_LITERAL_LITERAL)
		alMemoryMarkAsUsed(value->Pointer);

	// Oznacz litera�
	if(value->Type == AL_LITERAL_LITERAL)
		alLiteralMark(value->Literal);

	// Sprawd� this
	if(value->This)
		alMemoryMarkAsUsed(value->This);
}

alBoolean alLiteralToBoolean(alLiteral *value) {
	switch(value != NULL ? value->Type : AL_LITERAL_AUTO) {
		case AL_LITERAL_BOOLEAN:
			return value->Boolean;

		case AL_LITERAL_INTEGER:
			return value->Integer != 0;

		case AL_LITERAL_VALUE:
			return fabs(value->Value) < FLOAT_EPS;

		case AL_LITERAL_STRING:
			return value->String && value->String[0];

		case AL_LITERAL_POINTER:
			return value->Pointer != NULL;

		case AL_LITERAL_DEF:
			return value->Def != NULL;

		case AL_LITERAL_TABLE:
			return alTableCount(value->Table) != 0;
	}

	// Domy�lnie false
	return 0;
}

int alLiteralToInteger(alLiteral *value) {
	switch(value != NULL ? value->Type : AL_LITERAL_AUTO) {
		case AL_LITERAL_BOOLEAN:
		case AL_LITERAL_INTEGER:
			return value->Integer;

		case AL_LITERAL_VALUE:
			return (int)value->Value;

		case AL_LITERAL_STRING:
			return atoi(value->String);

		case AL_LITERAL_TABLE:
			return alTableCount(value->Table);
	}

	// Domy�lnie 0
	return 0;
}

float alLiteralToValue(alLiteral *value) {
	switch(value != NULL ? value->Type : AL_LITERAL_AUTO) {
		case AL_LITERAL_BOOLEAN:
		case AL_LITERAL_INTEGER:
			return (float)value->Integer;

		case AL_LITERAL_VALUE:
			return value->Value;

		case AL_LITERAL_STRING:
			return (float)atof(value->String);
	}

	// Domy�lnie 0
	return 0;
}

char *alLiteralToString(alLiteral *value) {
	alString buf;

	switch(value != NULL ? value->Type : AL_LITERAL_AUTO) {
		case AL_LITERAL_AUTO:
			return "";

		case AL_LITERAL_BOOLEAN:
			return value->Integer ? "true" : "false";

		case AL_LITERAL_INTEGER:
			sprintf(buf, "%i", value->Integer);
			return alMemoryStrDup(buf);

		case AL_LITERAL_VALUE:
			sprintf(buf, "%f", value->Value);
			return alMemoryStrDup(buf);

		case AL_LITERAL_STRING:
			return value->String;

		case AL_LITERAL_POINTER:
			sprintf(buf, "0x%08X", value->Pointer);
			return alMemoryStrDup(buf);

		case AL_LITERAL_DEF:
			return "{Def}";

		case AL_LITERAL_TABLE:
			return "{Table}";

		case AL_LITERAL_LITERAL:
			return "{Literal}";
	}

	// Domy�lnie 0
	return "";
}

void *alLiteralToPointer(alLiteral *value) {
	return value ? value->Pointer : NULL;
}

alDef *alLiteralToDef(alLiteral *value) {
	if(value && value->Type == AL_LITERAL_DEF)
		return value->Def;
	return NULL;
}

alLiteral *alLiteralToLiteral(alLiteral *value) {
	if(value && value->Type == AL_LITERAL_LITERAL)
		return value->Literal;
	return value;
}

alTable *alLiteralToTable(alLiteral *value) {
	if(value && value->Type == AL_LITERAL_TABLE)
		return value->Table;
	return NULL;
}

alLiteralType alLiteralTypeWithHighestPriority(alLiteralType type1, alLiteralType type2) {
	static alLiteralType priorities[AL_LITERAL_MAX][AL_LITERAL_MAX] = 
	{
		{AL_LITERAL_AUTO,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_BOOLEAN,	AL_LITERAL_INTEGER,	AL_LITERAL_VALUE,		AL_LITERAL_STRING,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_INTEGER,	AL_LITERAL_INTEGER,	AL_LITERAL_VALUE,		AL_LITERAL_STRING,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_VALUE,		AL_LITERAL_VALUE,		AL_LITERAL_VALUE,		AL_LITERAL_STRING,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_STRING,		AL_LITERAL_STRING,		AL_LITERAL_STRING,		AL_LITERAL_STRING,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO},
		{AL_LITERAL_AUTO,		AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO,			AL_LITERAL_AUTO}
	};

	// Sprawd�, czy poprawnie wprowadzono dane
	if(type1 < 0 || type1 >= AL_LITERAL_MAX || type2 < 0 || type2 >= AL_LITERAL_MAX)
		return -1;

	// Pobierz warto�� z tabeli
	return priorities[type1][type2];
}

alLiteral alLiteralConvert(alLiteral *value, alLiteralType newType) {
	// Sprawd� dane wej�ciowe
	if(!value)
		return alLiteralCreate();

	// Je�li typ jest taki jak oczekiwany to kopiujemy
	if(value->Type == newType)
		return *value;

	// Staraj si� przekonwertowa� dane
	switch(newType) {
		case AL_LITERAL_BOOLEAN:
			return alLiteralCreateBoolean(alLiteralToBoolean(value));
		
		case AL_LITERAL_INTEGER:
			return alLiteralCreateInteger(alLiteralToInteger(value));
		
		case AL_LITERAL_VALUE:
			return alLiteralCreateValue(alLiteralToValue(value));
	
		case AL_LITERAL_STRING:
			return alLiteralCreateString(alLiteralToString(value));

		case AL_LITERAL_POINTER:
			return alLiteralCreatePointer(alLiteralToPointer(value));

		case AL_LITERAL_DEF:
			return alLiteralCreateDef(alLiteralToDef(value));

		case AL_LITERAL_TABLE:
			return alLiteralCreateTable(alLiteralToTable(value));

		default:
			return alLiteralCreate();
	}
}

alLiteral alLiteralUnaryNeg(alLiteral *value) {
	return alLiteralCreateBoolean(!alLiteralToBoolean(value));
}

alLiteral alLiteralUnaryAdd(alLiteral *value) {
	if(value)
		return *value;
	return alLiteralCreate();
}

alLiteral alLiteralUnarySub(alLiteral *value) {
	// Oblicz warto��
	switch(value ? value->Type : AL_LITERAL_AUTO) {
		case AL_LITERAL_BOOLEAN:
			return alLiteralCreateBoolean(!(value->Integer != 0));

		case AL_LITERAL_INTEGER:
			return alLiteralCreateInteger(-value->Integer);

		case AL_LITERAL_VALUE:
			return alLiteralCreateValue(-value->Value);

		default:
			return alLiteralCreate();
	}
}

alLiteral alLiteralAdd(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n, valuen;
	alLiteralType desiredType;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Oblicz warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			valuen = alLiteralCreateBoolean(value1n.Integer != 0 || value2n.Integer != 0);
			break;

		case AL_LITERAL_INTEGER:
			valuen = alLiteralCreateInteger(value1n.Integer + value2n.Integer);
			break;

		case AL_LITERAL_VALUE:
			valuen = alLiteralCreateValue(value1n.Value + value2n.Value);
			break;

		case AL_LITERAL_STRING:
			if(value1n.String && value2n.String) {
				char *newbuf;
				
				// Utw�rz nowy bufor
				newbuf = (char*)alMemoryAlloc(NULL, (int)strlen(value1n.String) + (int)strlen(value2n.String) + 1, NULL, NULL);

				// Skopiuj dane
				strcpy(newbuf, value1n.String);
				strcat(newbuf, value2n.String);

				// Zapisz
				valuen = alLiteralCreateString(newbuf);
			}
			else if(value1n.String) {
				valuen = alLiteralCreateString(value1n.String);
			}
			else if(value2n.String) {
				valuen = alLiteralCreateString(value2n.String);
			}
			break;

		default:
			valuen = alLiteralCreate();
			break;
	}

	// Zwr�� obliczon� warto��
	return valuen;
}

alLiteral alLiteralSub(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n, valuen;
	alLiteralType desiredType;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Oblicz warto��
	switch(desiredType) {
		case AL_LITERAL_INTEGER:
			valuen = alLiteralCreateInteger(value1n.Integer - value2n.Integer);
			break;

		case AL_LITERAL_VALUE:
			valuen = alLiteralCreateValue(value1n.Value - value2n.Value);
			break;

		default:
			valuen = alLiteralCreate();
			break;
	}

	// Zwr�� obliczon� warto��
	return valuen;
}

alLiteral alLiteralMul(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n, valuen;
	alLiteralType desiredType;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Oblicz warto��
	switch(desiredType) {
		case AL_LITERAL_INTEGER:
			valuen = alLiteralCreateInteger(value1n.Integer * value2n.Integer);
			break;

		case AL_LITERAL_VALUE:
			valuen = alLiteralCreateValue(value1n.Value * value2n.Value);
			break;

		default:
			valuen = alLiteralCreate();
			break;
	}

	// Zwr�� obliczon� warto��
	return valuen;
}

alLiteral alLiteralDiv(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n, valuen;
	alLiteralType desiredType;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Oblicz warto��
	switch(desiredType) {
		case AL_LITERAL_INTEGER:
			valuen = alLiteralCreateInteger(value1n.Integer / value2n.Integer);
			break;

		case AL_LITERAL_VALUE:
			valuen = alLiteralCreateValue(value1n.Value / value2n.Value);
			break;

		default:
			valuen = alLiteralCreate();
			break;
	}

	// Zwr�� obliczon� warto��
	return valuen;
}

alLiteral alLiteralMod(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n, valuen;
	alLiteralType desiredType;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Oblicz warto��
	switch(desiredType) {
		case AL_LITERAL_INTEGER:
			valuen = alLiteralCreateInteger(value1n.Integer % value2n.Integer);
			break;

		case AL_LITERAL_VALUE:
			valuen = alLiteralCreateValue(value1n.Value - (value1n.Value / value2n.Value) * value2n.Value);
			break;

		default:
			valuen = alLiteralCreate();
			break;
	}

	// Zwr�� obliczon� warto��
	return valuen;
}

alLiteral alLiteralPow(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n, valuen;
	alLiteralType desiredType;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Oblicz warto��
	switch(desiredType) {
		case AL_LITERAL_INTEGER:
			valuen = alLiteralCreateInteger((int)pow(value1n.Integer, value2n.Integer));
			break;

		case AL_LITERAL_VALUE:
			valuen = alLiteralCreateValue((float)pow(value1n.Value, value2n.Value));
			break;

		default:
			valuen = alLiteralCreate();
			break;
	}

	// Zwr�� obliczon� warto��
	return valuen;
}

int alLiteralEq(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n;
	alLiteralType desiredType;
	int result = 0;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Por�wnaj warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			result = (value1n.Integer != 0) == (value2n.Integer != 0);
			break;

		case AL_LITERAL_VALUE:
			result = fabs(value1n.Value - value2n.Value) < FLOAT_EPS;
			break;

		case AL_LITERAL_STRING:
			result = value1n.String == value2n.String || value1n.String && value2n.String && !strcmp(value1n.String, value2n.String);
			break;

		case AL_LITERAL_AUTO:
			result = 0;
			break;

		default:
			result = value1n.Integer == value2n.Integer;
	}

	// Zwr�� obliczon� warto��
	return result;
}

int alLiteralNEq(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n;
	alLiteralType desiredType;
	int result = 0;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Por�wnaj warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			result = (value1n.Integer != 0) != (value2n.Integer != 0);
			break;

		case AL_LITERAL_VALUE:
			result = fabs(value1n.Value - value2n.Value) >= FLOAT_EPS;
			break;

		case AL_LITERAL_STRING:
			result = value1n.String != value2n.String && (!value1n.String || !value2n.String || strcmp(value1n.String, value2n.String) != 0);
			break;

		case AL_LITERAL_AUTO:
			result = 1;
			break;

		default:
			result = value1n.Integer != value2n.Integer;
	}

	// Zwr�� obliczon� warto��
	return result;
}

int alLiteralLess(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n;
	alLiteralType desiredType;
	int result = 0;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Por�wnaj warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			result = (value1n.Integer != 0) < (value2n.Integer != 0);
			break;

		case AL_LITERAL_INTEGER:
			result = value1n.Integer < value2n.Integer;
			break;

		case AL_LITERAL_VALUE:
			result = value1n.Value < value2n.Value - FLOAT_EPS;
			break;

		case AL_LITERAL_STRING:
			result = value1n.String && value2n.String && strcmp(value1n.String, value2n.String) < 0;
			break;

		default:
			result = -1;
	}

	// Zwr�� obliczon� warto��
	return result;
}

int alLiteralLessEq(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n;
	alLiteralType desiredType;
	int result = 0;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Por�wnaj warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			result = (value1n.Integer != 0) <= (value2n.Integer != 0);
			break;

		case AL_LITERAL_INTEGER:
			result = value1n.Integer <= value2n.Integer;
			break;

		case AL_LITERAL_VALUE:
			result = value1n.Value <= value2n.Value + FLOAT_EPS;
			break;

		case AL_LITERAL_STRING:
			result = value1n.String && value2n.String && strcmp(value1n.String, value2n.String) <= 0;
			break;

		default:
			result = -1;
	}

	// Zwr�� obliczon� warto��
	return result;
}

int alLiteralGr(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n;
	alLiteralType desiredType;
	int result = 0;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Por�wnaj warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			result = (value1n.Integer != 0) > (value2n.Integer != 0);
			break;

		case AL_LITERAL_INTEGER:
			result = value1n.Integer > value2n.Integer;
			break;

		case AL_LITERAL_VALUE:
			result = value1n.Value + FLOAT_EPS > value2n.Value;
			break;

		case AL_LITERAL_STRING:
			result = value1n.String && value2n.String && strcmp(value1n.String, value2n.String) > 0;
			break;

		default:
			result = -1;
	}

	// Zwr�� obliczon� warto��
	return result;
}

int alLiteralGrEq(alLiteral *value1, alLiteral *value2) {
	alLiteral value1n, value2n;
	alLiteralType desiredType;
	int result = 0;

	// Wyznacz typ o najwi�kszym prioritecie
	desiredType = alLiteralTypeWithHighestPriority(value1->Type, value2->Type);

	// Przekonwertuj warto�ci do nowego typu
	value1n = alLiteralConvert(value1, desiredType);

	// Przekonwertuj warto�ci do nowego typu
	value2n = alLiteralConvert(value2, desiredType);

	// Por�wnaj warto��
	switch(desiredType) {
		case AL_LITERAL_BOOLEAN:
			result = (value1n.Integer != 0) >= (value2n.Integer != 0);
			break;

		case AL_LITERAL_INTEGER:
			result = value1n.Integer >= value2n.Integer;
			break;

		case AL_LITERAL_VALUE:
			result = value1n.Value - FLOAT_EPS >= value2n.Value;
			break;

		case AL_LITERAL_STRING:
			result = value1n.String && value2n.String && strcmp(value1n.String, value2n.String) >= 0;
			break;

		default:
			result = -1;
	}

	// Zwr�� obliczon� warto��
	return result;
}

int alLiteralEqq(alLiteral *value1, alLiteral *value2) {
	// Sprawd�, czy typy zmiennych si� zgadzaj�
	if(value1->Type != value2->Type)
		return 0;

	// Por�wnaj typy
	return alLiteralEq(value1, value2);
}
