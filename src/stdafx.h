//------------------------------------//
//
// stdafx.h
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-16
//
//------------------------------------//

#pragma once

//------------------------------------//
// Base Includes

#include "../ayul.h"
#include "grammar.h"

//------------------------------------//
// Base Defines

#define MAX_STACK					10240
#define MAX_TABLE_SIZE		1024000
#define FLOAT_EPS					0.00001f

//------------------------------------//
// Externs

extern alNode*			alCurrentNodes[MAX_STACK];
extern alDef*				alCurrentDefs[MAX_STACK];
extern int					alCurrentNodeIndex;
extern int					alCurrentDefIndex;
extern alTree*			alCurrentTree;
extern alString			alCurrentFile;
extern int					alCurrentLine;
extern int					alCurrentAccessSpecifier;
extern FILE*				yyin;

//------------------------------------//
// Node Type Constants

#define AL_NODE_NULL					0
#define AL_NODE_LITERAL				1
#define AL_NODE_SYMBOL				2
#define AL_NODE_IF						3
#define AL_NODE_WHILE					4
#define AL_NODE_DO_WHILE			5
#define AL_NODE_FOR						6
#define AL_NODE_OPERATOR			7
#define AL_NODE_ASSIGNMENT		8
#define AL_NODE_COMPARE				9
#define AL_NODE_BREAK					10
#define AL_NODE_CONTINUE			11
#define AL_NODE_OF						12
#define AL_NODE_CALL					13
#define AL_NODE_RETURN				14
#define AL_NODE_POSTFIX				15
#define AL_NODE_UNARY					16
#define AL_NODE_END						17
#define AL_NODE_ARGC					18
#define AL_NODE_ARGV					19
#define AL_NODE_IS						20
#define AL_NODE_FOREACH				21
#define AL_NODE_THIS					22
#define AL_NODE_TABLE					23
#define AL_NODE_COUNT					24

//------------------------------------//
// Memory Mark Magic Constants

#define AL_MAGIC1			0x23124324
#define AL_MAGIC2			0xFFEFECCA

struct alData_s {
	int					Magic1;
	alData*			Next;
	int					Magic2;
	alData*			Prev;
	alCollect		Collect;
	alFree			Free;
	alMark			Mark;
};

struct alState_s {
	alTree*				Tree;

	alDef*				Def;
	alNode*				Current;
	int						At;
	int						Args;
	int						Returns;

	alStateStatus	Status;
	alNode*				ErrorNode;
	alDef*				ErrorDef;

	alLiteral*		Stack;
	int						StackSize;
	int						MaxStackSize;
	
	alLiteral*		Global;
	int						GlobalSize;

	int*					AssGlobal;
	int						AssGlobalSize;
	
	alTable*			This;
};

struct alValue_s {
	alLiteral			Key;
	union {
		alLiteral			Value;
		alNode*				ValueNode;
	};
	alValue*			Prev;
	alValue*			Next;
};

struct alTable_s {
	alLiteral**		IndexedValues;
	int						IndexedValueCount;

	alValue				NonIndexedValues;
	int						NonIndexedValueCount;

	int						EachIndex;
	alValue*			EachValue;
};

struct alDef_s {
	int					Type;
	alString		Name;
	alString*		Args;
	int					NumArgs;
	alNode*			Belongs;
	alNode*			Code;					// Only defined if variable is a function
	alCallback	Code2;			// Only used if definition is in native code

	int					Offset;				// < 0 means global offset, > 0 means local offset, 0 not used
	int					Count;

	alDef*			Next;
};

struct alNode_s {
	int					Type;				// NODE_*
	alLiteral		Value;
	alDef*			Defs;

	// Node specified data
	union {
		struct {
			alNode*		Expr;
			alNode*		True;
			alNode*		False;
		} If;

		struct {
			alNode*		Expr;
			alNode*		Loop;
		} While;

		struct {
			alNode*		Loop;
			alNode*		Expr;
		} DoWhile;

		struct {
			alNode*		Init;
			alNode*		Expr;
			alNode*		Next;
			alNode*		Loop;
		} For;

		struct {
			alNode*		Left;
			alNode*		Right;
		} Operator, Assignment, Compare;

		struct {
			alNode*		Expr;
		} Null;

		struct {
			alNode*		Expr;
		} End;

		struct {
			alNode*		This;
			alNode*		Of;
		} Of;

		struct {
			alNode*		Expr;
			alNode*		Args;
		} Call;

		struct {
			alNode*		Expr;
		} Return;

		struct {
			alNode*		Right;
		} Unary;

		struct {
			alNode*		Left;
		} Postfix;

		struct {
			alNode*		From;
			alNode*		To;
			alNode*		Expr;
		} Argv;

		struct {
			alNode*		Expr;
		} Is;

		struct {
			alNode*		Table;
			alNode*		As;
			alNode*		Loop;
		} ForEach;

		struct {
			alNode*		Expr;
		} Count;
	};

	int Op;
	int	Offset;

	struct {
		alString	Name;
		alDef*		Value;
		int				Args;
	} Symbol;

	struct {
		alValue*	Values;
	} Table;

	alDef*				Belongs;
	alNode*				Parent;
	alNode*				Next;
	alNode*				First;

	// Parse info
	alString			File;
	int						Line;
};

struct alTree_s {
	alDef					Main;
	int						Count;
	int						AssCount;
};

// alNode Constructors
alNode *alNewNullNode();
alNode *alNewLiteralNode(int type, void *value);
alNode *alNewDefNode(alDef *def);
alNode *alNewSymbolNode(const char *name);
alNode *alNewIfNode();
alNode *alNewWhileNode();
alNode *alNewDoWhileNode();
alNode *alNewForNode();
alNode *alNewBreakNode();
alNode *alNewContinueNode();
alNode *alNewAssignmentNode(int op, alNode *left, alNode *right);
alNode *alNewOperatorNode(int op, alNode *left, alNode *right);
alNode *alNewCompareNode(int op, alNode *left, alNode *right);
alNode *alNewOfNode(alNode *this_, alNode *of);
alNode *alNewCallNode(alNode *expr, alNode *args);
alNode *alNewReturnNode(alNode *expr);
alNode *alNewPostfixNode(int op, alNode *left);
alNode *alNewUnaryNode(int op, alNode *right);
alNode *alNewEndNode(alNode *expr);
alNode *alNewArgcNode();
alNode *alNewArgvNode(alNode *from, alNode *to);
alNode *alNewIsNode(alNode *expr);
alNode *alNewForEachNode();
alNode *alNewThisNode();
alNode *alNewTableNode();
alNode *alNewCountNode(alNode *expr);

// alNode Destructors
void alDestroyNode(alNode *node);

// alNode Control Functions
void alAddAtEndOfNode(alNode *node, alNode *link);

// alNode Variable Constructors
alDef *alNewNodeDef(alNode *node, int type, const char *name);

// Variable Argument Constructors
char* alNewDefArgument(alDef *def, const char *name);

// Check tree
int alCheckNode_r(alNode *node, int level, int count, alDef *def, alTree *tree);

// Grammar/Syntax Functions
void	yyerror(char *message);
char *yyfile();
int		yyline();
int		yylex();
int		yyparse();
