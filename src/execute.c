//------------------------------------//
//
// execute.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-14
//
//------------------------------------//

#include "stdafx.h"

#define alCheckStatus()			if(state->Status != AL_STATUS_NONE) {if(state->Status == AL_STATUS_RETURN) nargs += state->Returns; state->Returns = 0; break;}
#define alSetErrorStatus(e)	{state->Status = e; break;}

int alExecute_r(alState *state, alDef *def, alNode *node);

int alCheckStack(alState *state) {
	int i;

	// Zdejmij wszystkie referencje znajduj�ce si� na stosie
	for(i = 0; i < state->Returns; i++) {
		alLiteral *value = alPeek(state, i);

		// Pobrano warto��?
		if(!value)
			continue;

		// Mamy do czynienia z jawn� referencj�?
		if(value->ReferenceCount) {
			value->ReferenceCount = 0;
		}
		// Mamy do czynienia z niejawn� referencj�?
		else if(value->Type == AL_LITERAL_LITERAL) {
			*value = *value->Literal;
		}
	}

	return state->Returns;
}

alLiteral alGetLiteralFromStack(alState *state) {
	if(state->Returns == 1) {
		// Wyzeruj ilo�� zwr�conych warto�ci
		state->Returns = 0;

		// Pobierz warto�� z szczytu stosu
		return alPop(state, 0);
	}
	// Z�a ilo�� argument�w, oczekiwali�my tylko jednego
	state->Status = AL_STATUS_EXPRESSION_ERROR;
	return alLiteralCreate();
	/*
	if(state->Returns == 0) {
		return alLiteralCreate();
	}
	else {
		// Pobierz pierwsza warto�� z stosu
		alLiteral newValue = alPop(state, state->Returns - 1);

		// Usu� reszt� niepotrzebnych argument�w ze stosu
		alRemove(state, 0, state->Returns - 1);
		state->Returns = 0;
		return newValue;
	}*/
}

int alEvalExpr(alState *state, alDef *def, alNode *expr, int *result) {
	// Wykonaj wyra�enie
	if(alExecute_r(state, def, expr) == AL_STATUS_NONE) {
		if(state->Returns <= 0) {
			state->Status = AL_STATUS_EXPRESSION_ERROR;
		}
		else {
			alLiteral newValue;

			// Popraw zawarto�� stosu
			alCheckStack(state);

			// Pobierz pierwsza warto�� z stosu
			newValue = alGetLiteralFromStack(state);

			// Oblicz warto�� wyra�enia
			*result = alLiteralToInteger(&newValue);
		}
	}
	return state->Status;
}

alStateStatus alExecute_r(alState *state, alDef *def, alNode *node) {
	int result, i;
	int nargs = 0;
	alNode *current;

	// Ustaw domyslny stan
	state->Status = AL_STATUS_NONE;
	state->ErrorNode = NULL;
	state->ErrorDef = NULL;

	for(current = node; current && state->Status == AL_STATUS_NONE; current = current->Next) {
		alDef *local;

		// Ustaw zmienne lokalne
		for(local = current->Defs; local; local = local->Next) {
			// Tego nie ma co ustawia�
			if(local->Offset < 0)
				continue;

			// Sprawd�, czy mamy do czynienia z definicj� lokaln�
			if(local->Type != T_LOCAL) {
				continue;
			}

			// Ustaw warto�� domy�ln�
			state->Stack[state->At + local->Offset] = alLiteralCreate();
		}

		// Przerwij wykonywanie kodu
		alCheckStatus();

		// Ustaw aktualnie wykonywana gal��
		state->Current = current;

		// Wykonaj gal�� drzewa wyra�e�
		switch(current->Type) {
			case AL_NODE_NULL:
				if(current->Null.Expr) {
					// Zako�cz wykonywanie, je�li nie wykonano poprawnie gal�zi
					alExecute_r(state, def, current->Null.Expr);
					alCheckStatus();
					nargs += state->Returns;
				}
				break;

			case AL_NODE_LITERAL:
				// Dodaj literal na stos
				alPush(state, current->Value);
				nargs++;
				break;

			case AL_NODE_ARGC:
				// Dodaj warto�� okre�laj�c� ilo�� przekazanych argument�w do funkcji
				alPushInteger(state, state->Args);
				nargs++;
				break;

			case AL_NODE_ARGV: {
					if(current->Argv.Expr) {
						alLiteral argv;
						int iargv;

						// Oblicz wyra�enie 
						alExecute_r(state, def, current->Argv.Expr);
						alCheckStatus();

						// Popraw zawarto�� stosu
						alCheckStack(state);

						// Pobierz warto�� obliczonego wyra�enia
						argv = alGetLiteralFromStack(state);
						iargv = alLiteralToInteger(&argv);

						// Sprawd�, warto�� obliczon� z wyra�enia
						if(iargv < 0 || iargv >= state->Args)
							alSetErrorStatus(AL_STATUS_INVALID_ARGV_ERROR)
						else {
							// Wrzu� na stos dany argument
							alPushLiteral(state, &state->Stack[state->At - iargv - 1]);
							nargs++;
						}
					}
					else {
						alLiteral value;
						int i, from, to;

						if(current->Argv.From) {
							// Oblicz wyra�enie 
							alExecute_r(state, def, current->Argv.From);
							alCheckStatus();

							// Popraw zawarto�� stosu
							alCheckStack(state);

							// Pobierz warto�� obliczonego wyra�enia
							value = alGetLiteralFromStack(state);

							// Sprawd� warto��
							if(value.Type != AL_LITERAL_INTEGER || value.Integer < 0 || value.Integer >= state->Args)
								alSetErrorStatus(AL_STATUS_INVALID_ARGV_ERROR);

							// Zapisz zakres
							from = value.Integer;
						}
						else {
							from = 0;
						}

						if(current->Argv.To) {
							// Oblicz wyra�enie 
							alExecute_r(state, def, current->Argv.To);
							alCheckStatus();

							// Popraw zawarto�� stosu
							alCheckStack(state);

							// Pobierz warto�� obliczonego wyra�enia
							value = alGetLiteralFromStack(state);

							// Sprawd� warto��
							if(value.Type != AL_LITERAL_INTEGER || value.Integer < 0 || value.Integer >= state->Args)
								alSetErrorStatus(AL_STATUS_INVALID_ARGV_ERROR);

							// Zapisz zakres
							to = value.Integer;
						}
						else {
							to = state->Args;
						}

						// Wrzu� na stos dane warto�ci z zakresu
						for(i = from; i < to; i++) {
							// Wrzu� na stos dany argument
							alPushLiteral(state, &state->Stack[state->At - i - 1]);
							nargs++;
						}

						// Wrzu� na stos dane warto�ci z zakresu w odwrotnej kolejno�ci
						for(i = from-1; i >= to; i--) {
							// Wrzu� na stos dany argument
							alPushLiteral(state, &state->Stack[state->At - i - 1]);
							nargs++;
						}
					}
				}
				break;

			case AL_NODE_THIS:
				if(state->This && def->Type == T_LOCAL) {
					// Wrzu� this !
					alPushTable(state, state->This);
				}
				else {
					// Wrzu� warto�� domy�ln�
					alPush(state, alLiteralCreate());
				}
				nargs++;
				break;

			case AL_NODE_TABLE: {
					alTable *table = alTableNew(0);
					alValue *itor;

					// Dodaj warto�ci
					for(itor = current->Table.Values; itor; itor = itor->Next) {
						alLiteral value;

						// Wywo�aj pobieranie warto�ci
						alExecute_r(state, def, itor->ValueNode);
						alCheckStatus();

						// Pobierz warto�� dla danego elementu tablicy
						value = alTableGetValue(table, itor->Key, state);
						if(value.Type != AL_LITERAL_LITERAL)
							alSetErrorStatus(AL_STATUS_TABLE_VALUE_ERROR);

						// Przypisz warto��
						*value.Literal = alGetLiteralFromStack(state);
					}

					if(state->Status == AL_STATUS_NONE) {
						// Wrzu� tablic� na stos
						alPushTable(state, table);
						nargs++;
					}
					else {
						// Zniszcz tablic�
						alTableDestroy(table);
					}
				}
				break;

			case AL_NODE_COUNT: {
					int i, count = 0;

					// Oblicz wyra�enie 
					alExecute_r(state, def, current->Is.Expr);
					alCheckStatus();

					// Popraw zawarto�� stosu
					alCheckStack(state);

					// Sprawd� poprawno�� wyra�e�
					for(i = 0; i < state->Returns; i++) {
						alLiteral *value;

						// Pobierz wyra�enie
						value = alPeek(state, count + state->Returns - i - 1);
						if(!value) 
							alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

						// Operacja jest okreslona tylko dla stringow i tablic
						switch(value ? value->Type : AL_LITERAL_AUTO) {
							case AL_LITERAL_STRING:
								alPushInteger(state, value->String ? (int)strlen(value->String) : 0);
								count++;
								break;

							case AL_LITERAL_TABLE:
								alPushInteger(state, alTableCount(value->Table));
								count++;
								break;

							default:
								alSetErrorStatus(AL_STATUS_COUNT_ERROR);
						}
					}
					alCheckStatus();

					// Zdejmij ze stosu pozosta�e warto��i
					alRemove(state, count, state->Returns);
					nargs += count;
				}
				break;

			case AL_NODE_IS: {
					int i, count = 0;

					// Oblicz wyra�enie 
					alExecute_r(state, def, current->Is.Expr);
					alCheckStatus();

					// Popraw zawarto�� stosu
					alCheckStack(state);

					// Sprawd� poprawno�� wyra�e�
					for(i = 0; i < state->Returns; i++) {
						alLiteral *value;

						// Pobierz wyra�enie
						value = alPeek(state, count + state->Returns - i - 1);
						if(!value) 
							alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

						// Je�li warto�� jest r�na od automatycznej, to ta zmienna jest zdefiniowana
						alPushBoolean(state, value->Type != AL_LITERAL_AUTO);
						count++;
					}
					alCheckStatus();

					// Zdejmij ze stosu pozosta�e warto��i
					alRemove(state, count, state->Returns);
					nargs += count;
				}
				break;

			case AL_NODE_SYMBOL:
				// Dodaj symbol na stos
				if(current->Symbol.Value) {
					if(current->Symbol.Value->Offset < 0) {
						// Wrzu� na stos
						alPushDef(state, current->Symbol.Value);
						nargs++;
					}
					else if(current->Symbol.Value->Type == T_GLOBAL) {
						// Wrzu� na stos
						alPushLiteral(state, &state->Global[current->Symbol.Value->Offset]);
						nargs++;
					}
					else if(current->Symbol.Value->Type == T_LOCAL) {
						// Wrzu� na stos
						alPushLiteral(state, &state->Stack[state->At + current->Symbol.Value->Offset]);
						nargs++;
					}
					else 
						alSetErrorStatus(AL_STATUS_SYMBOL_ERROR);
				}
				else if(current->Symbol.Args >= 0) {
					// W przypadku, gdy przekroczymy zakres ilo�ci przekazanych argument�w, wrzu� na stos FALSE
					if(current->Symbol.Args >= state->Args) {
						alPush(state, alLiteralCreate());
					}
					else {
						// Wrzu� na stos
						alPushLiteral(state, &state->Stack[state->At - current->Symbol.Args - 1]);
					}
					nargs++;
				}
				else 
					alSetErrorStatus(AL_STATUS_SYMBOL_ERROR);
				break;

				case AL_NODE_IF:
					// Oblicz warto�� wyra�enia
					alEvalExpr(state, def, current->If.Expr, &result);
					alCheckStatus();

					// Wykonaj podga��zie
					alExecute_r(state, def, result != 0 ? current->If.True : current->If.False);
					alCheckStatus();

					// Dodaj ilo�� parametr�w
					nargs += state->Returns;
				break;

				// TODO: Poprawka 
				case AL_NODE_WHILE:
					while(1) {
						// Sprawd�, warto�� wyra�enia
						alEvalExpr(state, def, current->While.Expr, &result);
						if(state->Status == AL_STATUS_CONTINUE)
							continue;
						else if(state->Status != AL_STATUS_NONE || !result) {
							nargs += state->Returns;
							break;
						}

						// Wykonaj zawarto�� p�tli
						alExecute_r(state, def, current->While.Loop);
						if(state->Status == AL_STATUS_RETURN) {
								nargs += state->Returns;
								break;
							}
						// Zdejmij ze stosu pozosta�e wyra�enia
						alRemove(state, 0, state->Returns);

						// Sprawd� stan wykonywania
						if(state->Status != AL_STATUS_NONE && state->Status != AL_STATUS_CONTINUE) {
							nargs += state->Returns;
							break;
						}
					}
					if(state->Status == AL_STATUS_BREAK || state->Status == AL_STATUS_CONTINUE)
						state->Status = AL_STATUS_NONE;
				break;

				// TODO: Poprawka
				case AL_NODE_DO_WHILE:
					while(1) {
						if(state->Status != AL_STATUS_CONTINUE) {
							// Wykonaj zawarto�� p�tli
							alExecute_r(state, def, current->DoWhile.Loop);
							if(state->Status == AL_STATUS_RETURN) {
								nargs += state->Returns;
								break;
							}

							// Zdejmij ze stosu pozosta�e wyra�enia
							alRemove(state, 0, state->Returns);

							// Sprawd� stan wykonywania
							if(state->Status != AL_STATUS_NONE && state->Status != AL_STATUS_CONTINUE)
								break;
						}

						// Sprawd�, warto�� wyra�enia
						alEvalExpr(state, def, current->DoWhile.Expr, &result);
						if(state->Status != AL_STATUS_CONTINUE && (state->Status != AL_STATUS_NONE || !result)) {
							nargs += state->Returns;
							break;						
						}
					}
					if(state->Status == AL_STATUS_BREAK || state->Status == AL_STATUS_CONTINUE)
						state->Status = AL_STATUS_NONE;
				break;
				
				// TODO: Poprawka
				case AL_NODE_FOR:
					// Wykonaj inicjalizacje p�tli
					alExecute_r(state, def, current->For.Init);
					if(state->Status == AL_STATUS_RETURN) {
						nargs += state->Returns;
						break;
					}

					// Zdejmij ze stosu pozosta�e wyra�enia
					alRemove(state, 0, state->Returns);

					// Sprawd� stan wykonywania
					if(state->Status != AL_STATUS_NONE)
						break;

					while(1) {
						// Sprawd�, warto�� wyra�enia
						alEvalExpr(state, def, current->For.Expr, &result);
						if(state->Status != AL_STATUS_CONTINUE && (state->Status != AL_STATUS_NONE || !result))
							break;

						if(state->Status != AL_STATUS_CONTINUE) {
							// Wykonaj zawarto�� p�tli
							alExecute_r(state, def, current->For.Loop);
							if(state->Status == AL_STATUS_RETURN) {
								nargs += state->Returns;
								break;
							}

							// Zdejmij ze stosu pozosta�e wyra�enia
							alRemove(state, 0, state->Returns);

							// Sprawd� stan wykonywania
							if(state->Status != AL_STATUS_NONE && state->Status != AL_STATUS_CONTINUE)
								break;
						}

						// Wykonaj nast�pnik p�tli
						alExecute_r(state, def, current->For.Next);
						if(state->Status == AL_STATUS_RETURN) {
							nargs += state->Returns;
							break;
						}

						// Zdejmij ze stosu pozosta�e wyra�enia
						alRemove(state, 0, state->Returns);

						// Sprawd� stan wykonywania
						if(state->Status != AL_STATUS_NONE && state->Status != AL_STATUS_CONTINUE)
							break;
					}
					if(state->Status == AL_STATUS_BREAK || state->Status == AL_STATUS_CONTINUE)
						state->Status = AL_STATUS_NONE;
				break;

				case AL_NODE_FOREACH: {
						alLiteral tableLiteral;
						alTable *table;
						alLiteral asKey, asValue;

						// Wykonaj wyra�enie tablicowe
						alExecute_r(state, def, current->ForEach.Table);
						alCheckStatus();

						// Sprawd� stos
						alCheckStack(state);

						// Pobierz tablic�
						tableLiteral = alGetLiteralFromStack(state);
						table = alLiteralToTable(&tableLiteral);

						// Sprawd�, czy pobrano tablic�
						if(!table)
							alSetErrorStatus(AL_STATUS_TABLE_ERROR);

						// Wykonaj wyra�enie AS
						alExecute_r(state, def, current->ForEach.As);
						alCheckStatus();

						// Je�li przekazano jedn� warto��, tzn. chcemy uzyska� informacje tylko o warto�ci
						if(state->Returns == 1) {
							asKey = alLiteralCreate();
							asValue = alPop(state, 0);
						}
						// Je�li przekazano dwie warto�ci, tzn. chcemy uzyska� informacje o kluczu i o warto�ci
						else if(state->Returns == 2) {
							asValue = alPop(state, 0);
							asKey = alPop(state, 0);
						}
						else {
							asKey = alLiteralCreate();
							asValue = alLiteralCreate();
						}

						// Sprawd� warto�ci przekazane jako "klucz" i "warto��"
						if(asValue.Type != AL_LITERAL_LITERAL || asKey.Type != AL_LITERAL_LITERAL && asKey.Type != AL_LITERAL_AUTO)
							alSetErrorStatus(AL_STATUS_LVALUE_ERROR);

						// Zresetuj tablic�
						alTableReset(table);

						// Iteruj tablic� dopuki s� elementy
						while(!alTableEach(table, asKey.Type == AL_LITERAL_LITERAL ? asKey.Literal : NULL, asValue.Literal) && state->Status == AL_STATUS_NONE) {
							// Wykonaj zawarto�� p�tli
							alExecute_r(state, def, current->ForEach.Loop);
							alCheckStatus();

							// Zdejmij ze stosu pozosta�e wyra�enia
							alRemove(state, 0, state->Returns);
						}

						// Ustaw nowy stan
						if(state->Status == AL_STATUS_BREAK || state->Status == AL_STATUS_CONTINUE)
							state->Status = AL_STATUS_NONE;
					}
					break;

				case AL_NODE_OPERATOR: {
						int largs, rargs, count;

						// Oblicz lew� stron� wyra�enia
						alExecute_r(state, def, current->Operator.Left);
						alCheckStatus();
						largs = alCheckStack(state);

						// Oblicz praw� stron� wyra�enia
						alExecute_r(state, def, current->Operator.Right);
						alCheckStatus();
						rargs = alCheckStack(state);

						// TODO : I co teraz robic? w przypadku gdy largs != rargs
						count = alMin(largs, rargs);

						// Oblicz wszystkie warto�ci
						for(i = 0; i < count; i++) {
							alLiteral *lvalue = alPeek(state, largs + rargs - 1);
							alLiteral *rvalue = alPeek(state, rargs - 1);
							alLiteral nvalue;

							// Sprawd�, czy mamy wszystkie potrzebne dane
							if(!lvalue || !rvalue) 
								alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

							// Oblicz warto��
							switch(current->Op) {
								case T_ADDITION:
									nvalue = alLiteralAdd(lvalue, rvalue);
									break;

								case T_SUBTRACTION:
									nvalue = alLiteralSub(lvalue, rvalue);
									break;
							
								case T_MULTIPLICATION:
									nvalue = alLiteralMul(lvalue, rvalue);
									break;

								case T_DIVIDE:
									nvalue = alLiteralDiv(lvalue, rvalue);
									break;

								case T_MODULUS:
									nvalue = alLiteralMod(lvalue, rvalue);
									break;

								case T_POWER:
									nvalue = alLiteralPow(lvalue, rvalue);
									break;

								default:
									nvalue = alLiteralCreate();
									break;
							}

							// Sprawd�, czy napewno dobrze obliczono warto�ci
							if(nvalue.Type == AL_LITERAL_AUTO)
								alSetErrorStatus(AL_STATUS_INVALID_OPERATION_ERROR);

							// Dodaj warto�� na stos
							alPush(state, nvalue);
						}
						alCheckStatus();

						// Zdejmij ze stosu argumenty operatora
						alRemove(state, count, largs + rargs);

						// Zwieksz ilosc zwracanych argumentow
						nargs += count;
					}
					break;

				case AL_NODE_ASSIGNMENT: {
						int largs, rargs, count, valid;

						// Sprawd�, czy warto�� zosta�a ju� przypisana
						if(current->Op == T_GLOBAL) {
							if(current->Offset < state->AssGlobalSize) {
								if(state->AssGlobal[current->Offset])
									break;
								state->AssGlobal[current->Offset] = 1;
							}
						}

						// Oblicz praw� stron� wyra�enia
						alExecute_r(state, def, current->Operator.Right);
						alCheckStatus();
						rargs = alCheckStack(state);

						// Oblicz lew� stron� wyra�enia
						alExecute_r(state, def, current->Operator.Left);
						alCheckStatus();
						largs = state->Returns;

						// Sprawd� ilo�� argument�w z lewej strony i z prawej strony
						if(largs != rargs && (current->Op == T_GLOBAL || current->Op == T_LOCAL)) 
							alSetErrorStatus(AL_STATUS_INITIALIZATION_ERROR);

						// TODO : I co teraz robic? w przypadku gdy largs != rargs
						count = alMin(largs, rargs);

						// Oblicz wszystkie warto�ci
						for(i = 0; i < count; i++) {
							alLiteral *lvalue = alPeek(state, largs - 1);
							alLiteral *rvalue = alPeek(state, largs + rargs - 1);

							// Sprawd�, czy mamy wszystkie potrzebne dane
							if(!lvalue || !rvalue)
								alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

							if(lvalue->Type != AL_LITERAL_LITERAL)
								alSetErrorStatus(AL_STATUS_LVALUE_ERROR);

							// Ustaw domy�ln� flag�
							valid = 0;

							// Oblicz warto��
							switch(current->Op) {
								case T_GLOBAL:
								case T_LOCAL:
								case T_ASSIGNMENT:
									*lvalue->Literal = *rvalue;
									valid = 1;
									break;

								case T_ADDITIVE_ASSIGNMENT:
									*lvalue->Literal = alLiteralAdd(lvalue->Literal, rvalue);
									break;

								case T_SUBTRACTIVE_ASSIGNMENT:
									*lvalue->Literal = alLiteralSub(lvalue->Literal, rvalue);
									break;
							
								case T_MULTIPLY_ASSIGNMENT:
									*lvalue->Literal = alLiteralMul(lvalue->Literal, rvalue);
									break;

								case T_DIVIDE_ASSIGNMENT:
									*lvalue->Literal = alLiteralDiv(lvalue->Literal, rvalue);
									break;

								case T_MODULUS_ASSIGNMENT:
									*lvalue->Literal = alLiteralMod(lvalue->Literal, rvalue);
									break;

								case T_POWER_ASSIGNMENT:
									*lvalue->Literal = alLiteralPow(lvalue->Literal, rvalue);
									break;

								default:
									*lvalue->Literal = alLiteralCreate();
									break;
							}

							// Sprawd�, czy napewno dobrze obliczono warto�ci
							if(!valid && lvalue->Literal->Type == AL_LITERAL_AUTO)
								alSetErrorStatus(AL_STATUS_INVALID_OPERATION_ERROR);

							// Dodaj warto�� na stos
							alPush(state, *lvalue);
						}
						alCheckStatus();

						// Zdejmij ze stosu argumenty operatora
						alRemove(state, count, largs + rargs);

						// Zwieksz ilosc zwracanych argumentow
						nargs += count;
					}
					break;

				case AL_NODE_COMPARE: {
						int result;

						if(current->Op == T_OR || current->Op == T_AND) {
							// Oblicz lew� stron� wyra�enia
							alEvalExpr(state, def, current->Compare.Left, &result);
							alCheckStatus();

							// Sprawd� warunek
							if(current->Op == T_OR) {
								// Lewa strona jest prawdziwa, czyli nie musimy sprawdza� prawej strony
								if(result > 0) {
									alPushBoolean(state, 1);
									nargs++;
									break;
								}
							}
							else {
								// Lewa strona jest fa�szywa, czyli nie mo�emy sprawdza� prawej strony
								if(result <= 0) {
									alPushBoolean(state, 0);
									nargs++;
									break;
								}
							}

							// Oblicz lew� stron� wyra�enia
							alEvalExpr(state, def, current->Compare.Right, &result);
							alCheckStatus();

							// Dodaj wynik
							alPushBoolean(state, result > 0);
							nargs++;
						}
						else {
							int largs, rargs, count;

							// Oblicz lew� stron� wyra�enia
							alExecute_r(state, def, current->Compare.Left);
							alCheckStatus();
							largs = alCheckStack(state);

							// Oblicz praw� stron� wyra�enia
							alExecute_r(state, def, current->Compare.Right);
							alCheckStatus();
							rargs = alCheckStack(state);

							// TODO : I co teraz robic? w przypadku gdy largs != rargs
							count = alMin(largs, rargs);

							// Oblicz wszystkie warto�ci
							for(i = 0; i < count; i++) {
								alLiteral *lvalue = alPeek(state, largs + rargs - 1);
								alLiteral *rvalue = alPeek(state, rargs - 1);

								// Sprawd�, czy mamy wszystkie potrzebne dane
								if(!lvalue || !rvalue) 
									alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

								// Oblicz warto��
								switch(current->Op) {
									case T_EQUAL:
										result = alLiteralEq(lvalue, rvalue);
										break;

									case T_EQUALITY:
										result = alLiteralEqq(lvalue, rvalue);
										break;

									case T_NOT_EQUAL:
										result = alLiteralNEq(lvalue, rvalue);
										break;

									case T_LESS_EQUAL:
										result = alLiteralLessEq(lvalue, rvalue);
										break;

									case T_GREATER_EQUAL:
										result = alLiteralGrEq(lvalue, rvalue);
										break;

									case T_LESS:
										result = alLiteralLess(lvalue, rvalue);
										break;

									case T_GREATER:
										result = alLiteralGr(lvalue, rvalue);
										break;

									default:
										result = -1;
										break;
								}

								// Sprawd�, czy napewno dobrze obliczono warto�ci
								if(result < 0) 
									alSetErrorStatus(AL_STATUS_INVALID_OPERATION_ERROR);

								// Dodaj warto�� na stos
								alPushBoolean(state, result);
							}
							alCheckStatus();

							// Zdejmij ze stosu argumenty operatora
							alRemove(state, count, largs + rargs);

							// Zwieksz ilosc zwracanych argumentow
							nargs += count;
						}
					}
					break;

				case AL_NODE_BREAK:
					state->Status = AL_STATUS_BREAK;
					break;

				case AL_NODE_CONTINUE:
					state->Status = AL_STATUS_CONTINUE;
					break;

				case AL_NODE_OF: {
						alLiteral this_, *thisp, key, value;

						// Oblicz wyra�enie
						alExecute_r(state, def, current->Of.This);
						alCheckStatus();

						// Dzia�anie na tablicy (je�li element jest pusty to utw�rz now� tablic�
						this_ = alGetLiteralFromStack(state);

						// Pobierz referencje
						thisp = alLiteralToLiteral(&this_);

						// Sprawd�, czy mamy do czynienia z tablic�
						if(thisp->Type != AL_LITERAL_TABLE) {
							// Je�li mamy typ pusty utw�rz now� tablic�, ale tylko gdy mamy dost�pn� referencj�
							if(thisp->Type == AL_LITERAL_AUTO && &this_ != thisp) {
								// Utw�rz now� warto��
								*thisp = alLiteralCreateTable(alTableNew());
							}

							// Sprawd�, czy mamy do czynienia z b��dem
							if(thisp->Type != AL_LITERAL_TABLE) 
								alSetErrorStatus(AL_STATUS_TABLE_ERROR);
						}

						// Sprawd�, typ dzia�ania
						if(current->Of.Of) {
							// Oblicz klucz
							alExecute_r(state, def, current->Of.Of);
							alCheckStatus();

							// Popraw stos
							alCheckStack(state);

							// Pobierz klucz
							key = alGetLiteralFromStack(state);
							alCheckStatus();
						}
						else {
							// Warto�� domy�lna
							key = alLiteralCreate();
						}

						// Pobierz warto��
						value = alTableGetValue(thisp->Table, key, state);

						// Zapisz "this"
						alLiteralToLiteral(&value)->This = thisp->Table;

						// Wrzu� na stos adres kom�rki tablicy
						alPush(state, value);
						nargs++;
					}
					break;
				
				case AL_NODE_CALL: {
						alLiteral expr;
						int args;

						// Wrzu� argumenty
						alExecute_r(state, def, current->Call.Args);
						alCheckStatus();
						args = alCheckStack(state);

						// Odwroc kolejnosc argumentow
						for(i = 0; i < args / 2; i++) {
							alLiteral *value = alPeek(state, i);
							alLiteral *value2 = alPeek(state, args - i - 1);
							alLiteral temp;

							temp = value[0];
							value[0] = value2[0];
							value2[0] = temp;
						}

						// Wyznacz wyra�enie
						alExecute_r(state, def, current->Call.Expr);
						alCheckStatus();

						// Popraw stos dla wyra�enia
						alCheckStack(state);

						// Pobierz wyra�enie
						expr = alGetLiteralFromStack(state);

						// Sprawd�, czy wyra�enie mo�na wykona�
						if(expr.Type != AL_LITERAL_DEF || !expr.Def) 
							alSetErrorStatus(AL_STATUS_DEFINITION_ERROR);

						// Wywo�aj funkcj�
						alExecute(state, expr.Def, expr.This, args);
						alCheckStatus();

						// Dodaj argumenty funkcji
						nargs += state->Returns;
					}
					break;

				case AL_NODE_RETURN:
					alExecute_r(state, def, current->Return.Expr);
					alCheckStatus();

					// Popraw stos dla wyra�enia
					alCheckStack(state);

					if(state->Status == AL_STATUS_NONE) {
						// Pomin wszystkie poprzednie argumenty
						alRemove(state, 0, nargs);

						// Ustaw nowy stan
						state->Status = AL_STATUS_RETURN;
						nargs = state->Returns;
					}
					break;

				case AL_NODE_POSTFIX: {
						int count = 0;

						// Oblicz wyra�enie
						alExecute_r(state, def, current->Postfix.Left);
						alCheckStatus();

						// Wykonaj dzia�ania
						for(i = 0; i < state->Returns; i++) {
							alLiteral *value = alPeek(state, state->Returns - i - 1 + count), *valuenp = NULL;
							alLiteral valuen;

							// Sprawd�, czy mamy odpowiednie dane
							if(!value)
								alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

							switch(current->Op) {
								case T_INCREMENTATION:
									if(value->Type != AL_LITERAL_LITERAL)
										alSetErrorStatus(AL_STATUS_LVALUE_ERROR);

									valuen = alLiteralCreateInteger(1);
									valuen = alLiteralAdd(value->Literal, &valuen);
									valuenp = value->Literal;
									break;

								case T_DECREMENTATION:
									if(value->Type != AL_LITERAL_LITERAL)
										alSetErrorStatus(AL_STATUS_LVALUE_ERROR);

									valuen = alLiteralCreateInteger(1);
									valuen = alLiteralSub(value->Literal, &valuen);
									valuenp = value->Literal;
									break;

								default:
									valuen = alLiteralCreate();
							}
							alCheckStatus();

							// Sprawd� zwr�con� warto��
							if(valuen.Type == AL_LITERAL_AUTO || !valuenp)
								alSetErrorStatus(AL_STATUS_INVALID_OPERATION_ERROR);

							// Wrzu� poprzedni� warto��
							alPush(state, *valuenp);
							count++;

							// Zapisz warto��
							*valuenp = valuen;
						}
						alCheckStatus();

						// Dodaj argumenty
						nargs += count;

						// Usu� ze stosu warto�ci
						alRemove(state, count, state->Returns);
					}
					break;

				case AL_NODE_UNARY: {
						int count = 0;

						// Oblicz wyra�enie
						alExecute_r(state, def, current->Unary.Right);
						alCheckStatus();

						// Wykonaj dzia�ania
						for(i = 0; i < state->Returns; i++) {
							alLiteral *value = alPeek(state, state->Returns - i - 1 + count), *valuenp = NULL;
							alLiteral valuen;

							// Sprawd�, czy mamy odpowiednie dane
							if(!value)
								alSetErrorStatus(AL_STATUS_STACK_UNDERFLOW_ERROR);

							switch(current->Op) {
								// Negacja
								case T_NEGATION:
									valuen = alLiteralUnaryNeg(alLiteralToLiteral(value));
									break;

								// +
								case T_ADDITION:
									valuen = alLiteralUnaryAdd(alLiteralToLiteral(value));
									break;

								// -
								case T_SUBTRACTION:
									valuen = alLiteralUnarySub(alLiteralToLiteral(value));
									break;

								case T_INCREMENTATION:
									if(value->Type != AL_LITERAL_LITERAL)
										alSetErrorStatus(AL_STATUS_LVALUE_ERROR);

									valuen = alLiteralCreateInteger(1);
									valuen = alLiteralAdd(value->Literal, &valuen);
									valuenp = value->Literal;
									break;

								case T_DECREMENTATION:
									if(value->Type != AL_LITERAL_LITERAL)
										alSetErrorStatus(AL_STATUS_LVALUE_ERROR);

									valuen = alLiteralCreateInteger(1);
									valuen = alLiteralSub(value->Literal, &valuen);
									valuenp = value->Literal;
									break;

								default:
									valuen = alLiteralCreate();
							}
							alCheckStatus();

							// Sprawd� zwr�con� warto��
							if(valuen.Type == AL_LITERAL_AUTO)
								alSetErrorStatus(AL_STATUS_INVALID_OPERATION_ERROR);

							// Wrzu� warto�� na stos
							if(valuenp) {
								// Referencj�
								*valuenp = valuen;
								alPushLiteral(state, valuenp);
								count++;
							}
							else {
								// Warto��
								alPush(state, valuen);
								count++;
							}
						}
						alCheckStatus();

						// Dodaj argumenty
						nargs += count;

						// Usu� ze stosu warto�ci
						alRemove(state, count, state->Returns);
					}
					break;

				case AL_NODE_END:
					// Wykonaj wyra�enie
					alExecute_r(state, def, current->End.Expr);
					alCheckStatus();

					// Usu� wszystkie zmienne z stosu, wrzucone przez to wyra�enie
					alRemove(state, 0, state->Returns);
					break;
		}
	}

	// Zapisz informacje o b��dzie
	if(state->Status <= AL_STATUS_ERROR) {
		if(!state->ErrorNode) {
			state->ErrorNode = state->Current;
		}
		if(!state->ErrorDef) {
			state->ErrorDef = state->Def;
		}
		nargs = 0;
	}

	// Zapisz ilosc argument�w
	state->Returns = nargs;
	return state->Status;
}

alStateStatus alExecute(alState *state, alDef *def, alTable *this_, int nargs) {
	alDef *prevDef;
	int prevArgs, prevAt;
	alNode *prevCurrent;
	alTable *prevThis;

	// Zarezerwuj stos na zmienne lokalne
	if(state->StackSize + def->Count > state->MaxStackSize) {
		state->Status = AL_STATUS_STACK_OVERFLOW_ERROR;
		return state->Status;
	}

	// Zapisz stan
	state->Returns = nargs;
	state->Status = AL_STATUS_NONE;

	// Zapisz poprzednii stan
	prevDef = state->Def;
	prevAt = state->At;
	prevArgs = state->Args;
	prevCurrent = state->Current;
	prevThis = state->This;

	// Zapisz informacje o wywolaniu
	state->Def = def;	
	state->This = this_;
	state->Args = nargs;
	state->Current = NULL;
	state->At = state->StackSize;
	state->StackSize += def->Count;

	if(def->Code2) {
		int result;

		// Wywolaj funkcje fizyczn�
		result = def->Code2(state);
		if(result < 0) {
			state->Status = result;
			state->ErrorNode = prevCurrent;
			state->ErrorDef = def;
		}
		else {
			state->Returns = result;
		}
	}
	else if(def->Code) {
		// Wykonaj kod
		alExecute_r(state, def, def->Code);

		// Sprawd� warto�c zwr�con� przez drzewo wyra�e� i przywr�� prawid�owy stan
		if(state->Status == AL_STATUS_RETURN) {
			state->Status = AL_STATUS_NONE;
		}
		else if(state->Status == AL_STATUS_CONTINUE || state->Status == AL_STATUS_BREAK) {
			state->Status = AL_STATUS_LOOP_ERROR;
		}
	}
	else {
		state->Status = AL_STATUS_EXECUTE_ERROR;
		state->ErrorNode = prevCurrent;
		state->ErrorDef = def;
	}

	// Zdejmij wszystkie argumenty i zmienne lokalne ze stosu
	alRemove(state, state->Returns, state->Args + def->Count);

	// Odtw�rz stan
	state->Def = prevDef;
	state->This = prevThis;
	state->At = prevAt;
	state->Args = prevArgs;
	state->Current = prevCurrent;
	return state->Status;
}
