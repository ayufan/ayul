//------------------------------------//
//
// node.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-1
//
//------------------------------------//

#include "stdafx.h"

void noderr(alNode *node, char *message, ...) {
	fprintf(stderr, "%s(%i): build error, %s\n", node->File, node->Line, message);
}

alNode *alNewNullNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = (alNode*)malloc(sizeof(alNode));
	memset(newNode, 0, sizeof(alNode));
	newNode->Type = AL_NODE_NULL;
	newNode->Value.Type = AL_LITERAL_AUTO;
	// Zapisz informacje o parsowaniu
	strncpy(newNode->File, yyfile(), sizeof(newNode->File));
	newNode->Line = yyline();
	return newNode;
}

alNode *alNewLiteralNode(int type, void *value) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_LITERAL;
	newNode->Value.Type = type;
	if(value != NULL)
		newNode->Value.Integer = *(int*)value;
	return newNode;
}

alNode *alNewDefNode(alDef *def) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_SYMBOL;
	newNode->Symbol.Value = def;
	return newNode;
}

alNode *alNewSymbolNode(const char *name) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_SYMBOL;
	strncpy(newNode->Symbol.Name, name, sizeof(newNode->Symbol.Name));
	return newNode;
}

alNode *alNewIfNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_IF;
	return newNode;
}

alNode *alNewWhileNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_WHILE;
	return newNode;
}

alNode *alNewDoWhileNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_DO_WHILE;
	return newNode;
}

alNode *alNewForNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_FOR;
	return newNode;
}

alNode *alNewBreakNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_BREAK;
	return newNode;
}

alNode *alNewContinueNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_CONTINUE;
	return newNode;
}

alNode *alNewAssignmentNode(int op, alNode *left, alNode *right) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_ASSIGNMENT;
	newNode->Op = op;
	newNode->Assignment.Left = left;
	newNode->Assignment.Right = right;
	return newNode;
}

alNode *alNewOperatorNode(int op, alNode *left, alNode *right) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_OPERATOR;
	newNode->Op = op;
	newNode->Operator.Left = left;
	newNode->Operator.Right = right;
	return newNode;
}

alNode *alNewCompareNode(int op, alNode *left, alNode *right) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_COMPARE;
	newNode->Op = op;
	newNode->Compare.Left = left;
	newNode->Compare.Right = right;
	return newNode;
}

alNode *alNewOfNode(alNode *this_, alNode *of) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_OF;
	newNode->Of.This = this_;
	newNode->Of.Of = of;
	return newNode;
}

alNode *alNewCallNode(alNode *expr, alNode *args) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_CALL;
	newNode->Call.Expr = expr;
	newNode->Call.Args = args;
	return newNode;
}

alNode *alNewReturnNode(alNode *expr) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_RETURN;
	newNode->Return.Expr = expr;
	return newNode;
}

alNode *alNewEndNode(alNode *expr) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_END;
	newNode->End.Expr = expr;
	return newNode;
}

alNode *alNewArgcNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_ARGC;
	return newNode;
}

alNode *alNewArgvNode(alNode *from, alNode *to) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_ARGV;
	if(from != to) {
		newNode->Argv.From = from;
		newNode->Argv.To = to;
	}
	else {
		newNode->Argv.Expr = from;
	}
	return newNode;
}

alNode *alNewThisNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_THIS;
	return newNode;
}

alNode *alNewIsNode(alNode *expr) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_IS;
	newNode->Is.Expr = expr;
	return newNode;
}

alNode *alNewCountNode(alNode *expr) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_COUNT;
	newNode->Count.Expr = expr;
	return newNode;
}

alNode *alNewForEachNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_FOREACH;
	return newNode;
}

alNode *alNewPostfixNode(int op, alNode *left) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_POSTFIX;
	newNode->Op = op;
	newNode->Postfix.Left = left;
	return newNode;
}

alNode *alNewUnaryNode(int op, alNode *right) {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_UNARY;
	newNode->Op = op;
	newNode->Unary.Right = right;
	return newNode;
}

alNode *alNewTableNode() {
	alNode *newNode;

	// Utw�rz now� ga��� i przypisz do niej dane !
	newNode = alNewNullNode();
	newNode->Type = AL_NODE_TABLE;
	return newNode;
}

void alDestroyNode(alNode *node) {
	if(!node)
		return;

	// Usu� wszystkie dzieci
	if(node->For.Init) {
		alDestroyNode(node->For.Init);
	}
	if(node->For.Expr) {
		alDestroyNode(node->For.Expr);
	}
	if(node->For.Next) {
		alDestroyNode(node->For.Next);
	}
	if(node->For.Loop) {
		alDestroyNode(node->For.Loop);
	}
	if(node->Table.Values) {
		alValue *itor, *next;

		// Iteruj przez wszystkie warto�ci
		for(itor = node->Table.Values; itor; itor = next) {
			next = itor->Next;
			alDestroyNode(itor->ValueNode);
			free(itor);
		}
	}

	// Usu� nast�pn� r�wnoleg�� ga���
	if(node->Next) {
		alDestroyNode(node->Next);
	}

	// Usu� wszystkie zmienne
	if(node->Defs) {
		alDef *def, *next;

		// Usu� wszystkie definicje
		for(def = node->Defs; def; def = next) {
			next = def->Next;

			// Usu� wszystkie dzieci przypisane do zmiennych
			if(def->Code)
				alDestroyNode(def->Code);

			// Usu� wszystkie argumenty przypisane do zmiennych (funkcji)
			if(def->Args)
				free(def->Args);

			// Usu� definicj�
			free(def);
		}
	}
	free(node);
}

void alAddAtEndOfNode(alNode *node, alNode *link) {
	// Podlacz do nastepnej galezi
	if(node != NULL) {
		// Wyszukaj ostatnia pusty element
		while(node->Next != NULL)
			node = node->Next;

		// Przypisz wartosc jako kolejny
		node->Next = link;
	}
}

alDef *alNewNodeDef(alNode *node, int type, const char *name) {
	alDef *newDef;

	// Podepnij zmienna do ostatniej rownoleglej wartosci
	while(node != NULL) {
		if(node->Next == NULL)
			break;
		node = node->Next;
	}

	// Utw�rz now� defincj�
	newDef = malloc(sizeof(alDef));
	if(!newDef)
		return NULL;

	// Wyczy�� defincj�
	memset(newDef, 0, sizeof(alDef));

	// Skopiuj dane
	if(name)
		strncpy(newDef->Name, name, sizeof(alString));
	newDef->Type = type;

	// Dodaj do listy
	newDef->Next = node->Defs;
	node->Defs = newDef;
	return newDef;
}

char* alNewDefArgument(alDef *def, const char *name) {
	int i;

	// Wyszukaj nazwe argumentu
	for(i = 0; i < def->NumArgs; i++) {
		if(!strcasecmp(def->Args[i], name))
			return NULL;
	}

	// Dodaj nowy argument
	def->Args = (alString*)realloc(def->Args, sizeof(alString) * (def->NumArgs + 1));
	return strncpy(def->Args[def->NumArgs++], name, sizeof(alString));
}

int alDefFindInArgs(alDef *def, char *name) {
	int i;

	if(def) {
		for(i = 0; i < def->NumArgs; i++) {
			if(!strcasecmp(def->Args[i], name))
				return i;
		}
	}
	return -1;
}

alDef *alNodeFindInNode(alNode *node, alNode *to, char *name) {
	int global = 0;

	while(node) {
		alNode *current;

		for(current = node; current; current = current->Next) {
			alDef *def;

			for(def = current->Defs; def; def = def->Next) {
				// Sprawd�, czy mamy dost�p do definicji lokalnych
				if(def->Type == T_LOCAL && global)
					continue;

				// Sprawd� nazw� zmiennej
				if(!strcasecmp(def->Name, name))
					return def;
			}
			if(current == to)
				break;
		}
		if(!node->Parent) {
			if(!node->Belongs || !node->Belongs->Belongs) {
				break;
			}

			// Ustaw wyszukiwanie globalne
			global = 1;
			node = node->Belongs->Belongs;
		}
		else {
			node = node->Parent;
		}
		
		// Jesli mamy pierwszy element, to zacznijmy sprawdzanie od pierwszego		
		if(node && node->First)
			node = node->First;

		// Do? Nie mamy ograniczenia
		to = NULL;
	}
	return NULL;
}

int alCheckNode_r(alNode *node, int level, int count, alDef *def, alTree *tree) {
	alNode *current;
	int result = 0;

	for(current = node; current; current = current->Next) {
		alDef *defCurr;

		current->First = node;
		current->Parent = node->Parent;
		current->Belongs = node->Belongs;

		for(defCurr = current->Defs; defCurr; defCurr = defCurr->Next) {
			defCurr->Belongs = current;

			if(defCurr->Code || defCurr->Code2) {
				defCurr->Offset = -1;
				defCurr->Count = 0;
				if(defCurr->Code) {
					defCurr->Code->Belongs = defCurr;
					result += alCheckNode_r(defCurr->Code, 0, 0, defCurr, tree);
				}
			}
			else {
				if(defCurr->Type == T_LOCAL) {
					defCurr->Offset = count++;
				}
				else if(defCurr->Type == T_GLOBAL) {
					defCurr->Offset = tree->Count++;
				}
				else {
					defCurr->Offset = -1;
				}
			}

			if(defCurr->Name[0]) {
				alNode *current2;

				for(current2 = node; current2; current2 = current2->Next) { 
					alDef *defTemp;

					for(defTemp = current2->Defs; defTemp; defTemp = defTemp->Next) {
						if(defTemp != defCurr && !strcasecmp(defTemp->Name, defCurr->Name))
							goto symbol_exists_error;
					}
					if(current2 == current) {
						current2 = NULL;
						break;
					}
				}

				if(0) {
symbol_exists_error:
					noderr(current, "symbol already exists");
					result--;
				}
			}
		}

		def->Count = alMax(def->Count, count);

		if(current->Type != AL_NODE_SYMBOL) {
			if(current->Type == AL_NODE_FOR || current->Type == AL_NODE_DO_WHILE || current->Type == AL_NODE_WHILE)
				level++;

			if(current->For.Init) {
				current->For.Init->Belongs = current->Belongs;
				current->For.Init->Parent = current;
				result += alCheckNode_r(current->For.Init, level, count, def, tree);
			}

			if(current->For.Expr) {
				current->For.Expr->Belongs = current->Belongs;
				current->For.Expr->Parent = current;
				result += alCheckNode_r(current->For.Expr, level, count, def, tree);
			}
			
			if(current->For.Next) {
				current->For.Next->Belongs = current->Belongs;
				current->For.Next->Parent = current;
				result += alCheckNode_r(current->For.Next, level, count, def, tree);
			}

			if(current->For.Loop) {
				current->For.Loop->Belongs = current->Belongs;
				current->For.Loop->Parent = current;
				result += alCheckNode_r(current->For.Loop, level, count, def, tree);
			}
		}

		switch(current->Type) {
			case AL_NODE_IF:
				if(current->If.True && current->If.True->Value.Type == AL_LITERAL_LITERAL && 
					current->If.False && current->If.False->Value.Type == AL_LITERAL_LITERAL) 
					current->Value.Type = AL_LITERAL_LITERAL;
				break;

			case AL_NODE_TABLE:
				if(node->Table.Values) {
					alValue *itor;

					// Iteruj przez wszystkie warto�ci
					for(itor = node->Table.Values; itor; itor = itor->Next) {
						if(!itor->ValueNode)
							continue;

						itor->ValueNode->Parent = current;
						itor->ValueNode->Belongs = current->Belongs;
						result += alCheckNode_r(itor->ValueNode, level, count, def, tree);
					}
				}
				break;

			case AL_NODE_ASSIGNMENT:
				if(current->Assignment.Left->Value.Type != AL_LITERAL_LITERAL) {
					noderr(current, "l-value required");
					result--;
				}
				else {
					if(current->Op == T_GLOBAL) {
						current->Offset = tree->AssCount++;
					}
					current->Value.Type = AL_LITERAL_LITERAL;
				}
				break;

			case AL_NODE_BREAK:
			case AL_NODE_CONTINUE:
				if(level == 0) {
					noderr(current, "loop required");
					result--;
				}
				break;

			case AL_NODE_OF:
				current->Value.Type = AL_LITERAL_LITERAL;
				break;

			case AL_NODE_POSTFIX:
				if(current->Postfix.Left->Value.Type != AL_LITERAL_LITERAL) {
					noderr(current, "l-value required");
					result--;
				}
				break;

			case AL_NODE_UNARY:
				if(current->Op != T_NEGATION) {
					if(current->Unary.Right->Value.Type != AL_LITERAL_LITERAL) {
						noderr(current, "l-value required");
						result--;
					}
					else {
						current->Value.Type = AL_LITERAL_LITERAL;
					}
				}
				break;

			case AL_NODE_SYMBOL: {
					if(!current->Symbol.Value) {
						current->Symbol.Args = alDefFindInArgs(current->Belongs, current->Symbol.Name);
						if(current->Symbol.Args < 0) {
							current->Symbol.Value = alNodeFindInNode(node, current, current->Symbol.Name);
							if(!current->Symbol.Value) {
								noderr(current, "symbol not found");
								result--;
							}
						}
					}
					current->Value.Type = AL_LITERAL_LITERAL;
				}
				break;

			case AL_NODE_THIS:
				if(def->Type == T_GLOBAL) {
					noderr(current, "symbol not allowed");
					result--;
				}
				break;

			case AL_NODE_ARGC:
				current->Value.Type = AL_LITERAL_LITERAL;
				break;
		}
	}
	return result;
}
