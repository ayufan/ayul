%{

//------------------------------------//
//
// syntax.cxx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-3-31
//
//------------------------------------//

#include "stdafx.h"
#include "grammar.h"

#ifdef _MSC_VER
// Treat some previous errors as warnings
#pragma warning(disable : 4244 4267)
#endif

extern int	alCurrentLine;

%}

Identifier	[a-zA-Z_][a-zA-Z0-9_]*
Integer			[0-9]+
Float				[0-9]*"."[0-9]+
String			\"[^\"]*\"
Space				[ \t]+

%%

"local"			{ return T_LOCAL;			}
"global"		{ return T_GLOBAL;		}
"if"				{	return T_IF;				}
"else"			{	return T_ELSE;			}
"while"			{	return T_WHILE;			}
"do"				{	return T_DO;				}
"for"				{	return T_FOR;				}
"return"		{	return T_RETURN;		}
"continue"	{	return T_CONTINUE;	}
"break"			{	return T_BREAK;			}
"null"			{ return T_NULL;			}
"true"			{ yylval.i = 1; return T_BOOLEAN;	}
"false"			{ yylval.i = 0; return T_BOOLEAN;	}
"argc"			{ return T_ARGC;			}
"argv"			{ return T_ARGV;			}
"foreach"		{ return T_FOREACH;		}
"is"				{ return T_IS;				}
"as"				{ return T_AS;				}
"this"			{ return T_THIS;			}

"{"					{	return T_BEGIN;			}
"{{"				{ return T_T_BEGIN;		}
"}}"				{ return T_T_END;			}
"}"					{	return T_END;				}
"("					{	return T_OPEN;			}
")"					{	return T_CLOSE;			}
"["					{	return T_S_OPEN;		}
"]"					{	return T_S_CLOSE;		}
"?"					{ return T_TEST;			}
":"					{ return T_OTHERWISE;	}
";"					{	return T_STATEMENT;	}

","					{	return T_SEPARATOR;	}
"."					{	return T_ACCESSOR;	}
".."				{ return T_TO;				}

"=="				{ return T_EQUAL;					}
"==="				{	return T_EQUALITY;			}
"!="				{	return T_NOT_EQUAL;			}
"<="				{	return T_LESS_EQUAL;		}
">="				{	return T_GREATER_EQUAL;	}
"<"					{	return T_LESS;					}
">"					{	return T_GREATER;				}
"!"					{	return T_NEGATION;			}

"="					{	return T_ASSIGNMENT;							}
"+="				{	return T_ADDITIVE_ASSIGNMENT;			}
"-="				{	return T_SUBTRACTIVE_ASSIGNMENT;	}
"*="				{	return T_MULTIPLY_ASSIGNMENT;			}
"/="				{	return T_DIVIDE_ASSIGNMENT;				}
"%="				{	return T_MODULUS_ASSIGNMENT;			}
"^="				{	return T_POWER_ASSIGNMENT;				}

"+"					{	return T_ADDITION;				}
"-"					{	return T_SUBTRACTION;			}
"*"					{	return T_MULTIPLICATION;	}
"/"					{	return T_DIVIDE;					}
"%"					{	return T_MODULUS;					}
"^"					{	return T_POWER;						}

"#"					{ return T_COUNT;						}

"&&"				{	return T_AND;							}
"||"				{	return T_OR;							}

"++"				{	return T_INCREMENTATION;	}
"--"				{	return T_DECREMENTATION;	}

{Identifier} {	
	strncpy(yylval.id, yytext, sizeof(yylval.id)); 
	return T_IDENTIFIER;		
}
{Integer} {	
	yylval.i = atoi(yytext);
	return T_INTEGER;	
}
{Float} {	
	yylval.f = (float)atof(yytext);
	return T_FLOAT;					
}
{String} {	
	static char escapeSeq[256];
	static int escapeSeqSet = 0;
	char *c, *p, *pp;

	if(escapeSeqSet == 0) {
		memset(escapeSeq, 0, sizeof(escapeSeq));
		escapeSeq['a'] = '\a';
		escapeSeq['b'] = '\b';
		escapeSeq['f'] = '\f';
		escapeSeq['n'] = '\n';
		escapeSeq['r'] = '\r';
		escapeSeq['t'] = '\t';
		escapeSeq['v'] = '\v';
		escapeSeq['\''] = '\'';
		escapeSeq['\"'] = '\"';
		escapeSeq['\\'] = '\\';
		escapeSeq['?'] = '\?';
		escapeSeq['0'] = '\0';
		escapeSeqSet = 1;
	}	
	
	yylval.s = (char*)malloc(strlen(yytext));
	for(p = pp = yylval.s, c = yytext + 1; *c; ) {
		if(*c == '\n')
			alCurrentLine++;

		// Sprawdz sekwencje ucieczki
		if(*c == '\\' && c[1] < sizeof(escapeSeq) && escapeSeq[c[1]] != 0) {
			// Dodaj sekwencje ucieczki
			*p++ = escapeSeq[c[1]];

			// Przesun za sekwencje ucieczk
			c += 2;
		}
		else {
			*p++ = *c++;
		}
	}
	// Dodaj znak konca ciagu przed cudzyslowem
	p[-1] = '\0';
 	return T_STRING;				
}
"//" {
	char c;

  while((c = input()) != '\n' && c != 0);
	alCurrentLine++;
}
"/*" {
	char	c1, c2;

	c1 = input();
	c2 = input();

	if(c1 == '\n')
		alCurrentLine++;

	while(c1 != 0 && c2 != 0) {
		if(c1 == '*' && c2 == '/')
			break;

		if(c1 == '\n')
			alCurrentLine++;

		c1 = c2;
		c2 = input();
	}
}
\r						{}
\n						{ alCurrentLine++; }
{Space}				{}

%%
