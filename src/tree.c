//------------------------------------//
//
// tree.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-20
//
//------------------------------------//

#include "stdafx.h"

void alTreeActivate(alTree *tree) {
	// Przypisz warto�ci domy�lne do podr�cznego stosu
	alCurrentDefIndex = -1;
	alCurrentNodeIndex = -1;
	alCurrentNodes[++alCurrentNodeIndex] = tree->Main.Code;
	alCurrentTree = tree;
}

alTree *alTreeGetActive() {
	return alCurrentTree;
}

alDef* alTreeAddGlobalDef(const char *name) {
	alDef *def;

	// Sprawd�, czy mamy dost�p do drzewa wyra�e�
	if(!alCurrentTree)
		return NULL;

	// Utw�rz now� definicj�
	def = alNewNodeDef(alCurrentTree->Main.Code, T_GLOBAL, name);
	return def;
}

alDef* alTreeAddGlobalCallbackDef(const char *name, alCallback code) {
	alDef *def;
	
	// Utworz now� definicj�
	def = alTreeAddGlobalDef(name);
	if(!def)
		return NULL;

	// Przypisz callbacka
	def->Code2 = code;
	return def;
}

alTree* alTreeNew() {
	alTree *tree;

	// Utw�rz nowe drzewo
	tree = (alTree*)malloc(sizeof(alTree));
	memset(tree, 0, sizeof(alTree));
	tree->Main.Type = T_GLOBAL;
	tree->Main.Code = alNewNullNode();
	tree->Main.Offset = -1;
	alTreeActivate(tree);
	return tree;
}

int alTreeLoadInto(const char *name) {
	int result;

	if(alCurrentNodeIndex < 0 || !alCurrentTree)
		return -1;

	// Otw�rz plik
	yyin = fopen(name, "r");
	if(!yyin) {
		fprintf(stderr, "couldn't open %s\n", name);
		return -1;
	}

	// Skopiuj dane
	alCurrentLine = 1;
	strncpy(alCurrentFile, name, sizeof(alCurrentFile));

	// Wgraj kod do drzewa
	result = yyparse();

	// Zamknij plik
	fclose(yyin);
	yyin = NULL;
	return -result;
}

int alTreeBuild() {
	if(!alCurrentTree)
		return -1;

	// Wyczy�� zawarto�� drzewa
	alCurrentTree->Count = 0;
	alCurrentTree->Main.Count = 0;

	// Sprawd� drzewko
	return alCheckNode_r(alCurrentTree->Main.Code, 0, 0, &alCurrentTree->Main, alCurrentTree);
}

int alTreeDestroy(alTree *tree) {
	if(!tree)
		return -1;

	// Zwolnij drzewko
	alDestroyNode(tree->Main.Code);

	// Zwolnij korze�
	free(tree);

	if(alCurrentTree == tree)
		alCurrentTree = NULL;

	return 0;
}

alDef *alTreeFindGlobalDefInNode(alNode *node, const char *name) {
	alNode *current;

	for(current = node; current; current = current->Next) {
		alDef *def;

		for(def = current->Defs; def; def = def->Next) {
			// Sprawd�, czy mamy dost�p do definicji lokalnych
			if(def->Type != T_GLOBAL)
				continue;

			// Sprawd� nazw� zmiennej
			if(!strcasecmp(def->Name, name))
				return def;
		}
	}
	return NULL;
}

alDef *alTreeFindGlobalDef(alTree *tree, const char *name) {
	// Defincja funkcji "g��wnej"
	if(!name) {
		return &tree->Main;
	}

	// Wyszukaj definicj�
	return alTreeFindGlobalDefInNode(tree->Main.Code, name);
}
