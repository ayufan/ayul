//------------------------------------//
//
// table.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-17
//
//------------------------------------//

#include "stdafx.h"

static void alTableCollect(alTable *table) {
	alValue *itor;
	int i;

	if(!table)
		return;

	// Oznacz wszystkie warto�ci tablicy indeksowanej
	for(i = 0; i < table->IndexedValueCount; i++) {
		if(!table->IndexedValues[i])
			continue;

		// Usu� this
		table->IndexedValues[i]->This = NULL;

		// Oznacz litera�y
		alMemoryMarkAsUsed(table->IndexedValues[i]);
		alLiteralMark(table->IndexedValues[i]);
	}

	// Oznacz wszystkie warto�ci tablicy nieindeksowanej
	for(itor = table->NonIndexedValues.Next; itor != &table->NonIndexedValues; itor = itor->Next) {
		// Usu� this
		itor->Key.This = NULL;
		itor->Key.This = NULL;

		// Oznacz litera�y
		alLiteralMark(&itor->Key);
		alLiteralMark(&itor->Value);
	}
}

alTable* alTableNew() {
	alTable*		table;

	// Utw�rz tablic�
	table = (alTable*)alMemoryAlloc(NULL, sizeof(alTable), (alCollect)alTableCollect, (alFree)alTableDestroy);

	// Utw�rz tablic� indeksowan�
	table->IndexedValueCount = 0;
	table->IndexedValues = NULL;

	// Ustaw tablic� indeksowan�
	table->NonIndexedValues.Next = table->NonIndexedValues.Prev = &table->NonIndexedValues;
	table->NonIndexedValueCount = 0;

	// Zresetuj tablic�
	alTableReset(table);
	return table;
}

int alTableDestroy(alTable* table) {
	alValue *itor, *next;

	if(!table)
		return -1;

	if(table->IndexedValues) {
		int i;

		for(i = 0; i < table->IndexedValueCount; i++) {
			if(!table->IndexedValues[i])
				continue;
			free(table->IndexedValues[i]);
		}
		free(table->IndexedValues);
	}

	// Zwolnij wszystkie warto�ci tablicy nieindeksowanej
	for(itor = table->NonIndexedValues.Next; itor != &table->NonIndexedValues; itor = next) {
		next = itor->Next;
		free(itor);
	}

	alMemoryFree(table);
	return 0;
}

alLiteral* alTableGetRawValue(alTable *table, alLiteral key) {
	alLiteral **value = NULL;
	alValue *itor;
	int i;

	if(!table)
		return NULL;

	// Sprawd�, czy mamy do czynienia z []
	if(key.Type == AL_LITERAL_AUTO) {
		// Szukaj pierwszego wolnego elementu w tablicy
		for(i = 0; i < table->IndexedValueCount; i++) {
			if(!table->IndexedValues[i] || table->IndexedValues[i]->Type == AL_LITERAL_AUTO) {
				// Zapisz kom�rk�
				value = &table->IndexedValues[i];
				break;
			}
		}

		if(!value && table->IndexedValueCount < MAX_TABLE_SIZE) {
			// Zwi�kszamy rozmiar tablicy (dwukrotnie)
			i = table->IndexedValueCount;
			table->IndexedValueCount = alMin(table->IndexedValueCount * 2 + 1, MAX_TABLE_SIZE);
			table->IndexedValues = (alLiteral**)realloc(table->IndexedValues, sizeof(alLiteral*) * table->IndexedValueCount); 

			// Wyczy�� wy�szy nowy tablicy
			memset(&table->IndexedValues[i], 0, sizeof(alLiteral*) * (table->IndexedValueCount - i));

			// Zapisz kom�rk�
			value = &table->IndexedValues[i];
		}
	}
	// Sprawd�, czy mamy do czynienia z tablic� indeksowan�, czy nie?
	else if(key.Type == AL_LITERAL_INTEGER && 0 <= key.Integer && key.Integer < MAX_TABLE_SIZE) {
		// Sprawd� zakres tablicy
		if(key.Integer >= table->IndexedValueCount) {
			// Zwi�ksz rozmiar tablicy do key.Integer
			i = table->IndexedValueCount;
			table->IndexedValueCount = alMin(key.Integer + table->IndexedValueCount / 4 + 1, MAX_TABLE_SIZE);
			table->IndexedValues = (alLiteral**)realloc(table->IndexedValues, sizeof(alLiteral*) * table->IndexedValueCount); 

			// Wyczy�� nowy zakres tablicy
			memset(&table->IndexedValues[i], 0, sizeof(alLiteral*) * (table->IndexedValueCount - i));
		}

		// Zapisz adres kom�rki
		value = &table->IndexedValues[key.Integer];
	}

	// Sprawd�, czy mamy kom�rk�
	if(value) {
		// Sprawd�, czy mamy warto��
		if(!*value) {
			// Utw�rz now� warto��
			*value = (alLiteral*)malloc(sizeof(alLiteral));

			// Wyczy�� kom�rk�
			if(*value)
				memset(*value, 0, sizeof(alLiteral));
			else
				return NULL;
		}
		return *value;
	}

	// Szukaj w tablicy nieindeksowanej
	for(itor = table->NonIndexedValues.Next; itor != &table->NonIndexedValues; itor = itor->Next) {
		// Sprawd�, czy warto�ci s� zgodne co do typu
		if(alLiteralEqq(&key, &itor->Key)) {
			return &itor->Value;
		}
	}

	// Dodaj now� warto��
	itor = (alValue*)malloc(sizeof(alValue));
	itor->Key = key;
	itor->Value = alLiteralCreate();

	// Wepnij element do listy dwustronnej
	itor->Next = table->NonIndexedValues.Next;
	itor->Prev = &table->NonIndexedValues;
	table->NonIndexedValues.Next->Prev = itor;
	table->NonIndexedValues.Next = itor;
	table->NonIndexedValueCount++;
	return &itor->Value;
}

alLiteral alTableGetValue(alTable *table, alLiteral key, alState *state) {
	// Indeksuj tablice "fizycznie"
	return alLiteralCreateLiteral(alTableGetRawValue(table, key));
}

int alTableRemove(alTable *table, alLiteral key) {
	alValue *itor;

	if(!table)
		return -1;

	// Sprawd�, czy mamy do czynienia z tablic� indeksowan�, czy nie?
	if(key.Type == AL_LITERAL_INTEGER) {
		// Sprawd�, czy warto�� mie�ci si� w zakresie tablicy
		if(0 <= key.Integer && key.Integer < MAX_TABLE_SIZE) {
			// Czy mie�cimy si� w zaalokowanym obszarze?
			if(key.Integer < table->IndexedValueCount) {
				// Czy kom�rka istnieje?
				if(table->IndexedValues[key.Integer]) {
					*table->IndexedValues[key.Integer] = alLiteralCreate();
				}
			}
			return 0;
		}
	}

	// Szukaj w tablicy nieindeksowanej
	for(itor = table->NonIndexedValues.Next; itor != &table->NonIndexedValues; itor = itor->Next) {
		// Sprawd�, czy warto�ci s� zgodne co do typu
		if(alLiteralEqq(&key, &itor->Key)) {
			// Wypnij element z listy
			itor->Next->Prev = itor->Prev;
			itor->Prev->Next = itor->Next;
			if(table->EachValue == itor) {
				table->EachValue = itor->Next;
			}
			free(itor);
			table->NonIndexedValueCount--;
			return 0;
		}
	}

	return 1;
}

int alTableCount(alTable *table) {
	int i, count = 0;
	alValue *itor;

	if(!table)
		return -1;

	// Oblicz ilo�� element�w tablicy indeksowanej
	for(i = 0; i < table->IndexedValueCount; i++) {
		if(table->IndexedValues[i] && table->IndexedValues[i]->Type != AL_LITERAL_AUTO)
			count++;
	}

	// Oblicz ilo�� element�w tablicy nieindeksowanej
	for(itor = table->NonIndexedValues.Next; itor != &table->NonIndexedValues; itor = itor->Next) {
		count++;
	}
	return count;
}

int alTableReset(alTable *table) {
	if(!table)
		return -1;

	// Zresetuj zawarto�� tablicy
	table->EachIndex = -1;
	table->EachValue = NULL;
	return 0;
}

int alTableEach(alTable *table, alLiteral *key, alLiteral *value) {
	if(!table)
		return -1;

	// Pobierz nast�pny element
	while(++table->EachIndex < table->IndexedValueCount) {
		// Sprawd�, czy kom�rka jest pusta
		if(!table->IndexedValues[table->EachIndex] || table->IndexedValues[table->EachIndex]->Type == AL_LITERAL_AUTO)
			continue;

		// Zwr�� adres kom�rki indeksowanej
		if(key)
			*key = alLiteralCreateInteger(table->EachIndex);
		if(value)
			*value = *table->IndexedValues[table->EachIndex];
		return 0;
	}

	// Pobierz nast�pny r�wnoleg�y element
	if(!table->EachValue)
		table->EachValue = table->NonIndexedValues.Next;
	else
		table->EachValue = table->EachValue->Next;

	// Skopiuj klucz i warto��
	if(table->EachValue != &table->NonIndexedValues) {
		if(key)
			*key = table->EachValue->Key;
		if(value)
			*value = table->EachValue->Value;
		return 0;
	}

	// Koniec tablicy osi�gni�ty
	return 1;
}
