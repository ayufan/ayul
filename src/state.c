//------------------------------------//
//
// state.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-7
//
//------------------------------------//

#include "stdafx.h"

#define MAX_STATE_STACK_SIZE		65535

int alStateCollect(alState *state) {
	int i;

	if(!state)
		return -1;

	// Sprawd� wszystkie zmienne globalne
	for(i = 0; i < state->GlobalSize; i++) {
		alLiteralMark(&state->Global[i]);
	}

	// Sprawd� wszystkie zmienne lokalne
	for(i = 0; i < state->StackSize; i++) {
		alLiteralMark(&state->Stack[i]);
	}
	return 0;
}

int alStateFree(alState *state) {
	if(state == NULL)
		return -1;

	if(state->Current) {
		fprintf(stderr, "warning: state at run stage\n");
	}
	if(state->Stack) {
		free(state->Stack);
	}
	if(state->Global) {
		free(state->Global);
	}
	if(state->AssGlobal) {
		free(state->AssGlobal);
	}
	return 0;
}

alState* alStateNew(alTree *tree, int stackSize) {
	alState *state;

	if(stackSize <= 0) {
		stackSize = MAX_STATE_STACK_SIZE;
	}

	state = alMemoryAlloc(NULL, sizeof(alState), (alCollect)alStateCollect, (alFree)alStateFree);
	if(!state) {
		fprintf(stderr, "couldn't alloc state\n");
		return NULL;
	}
	state->Tree = tree;
	state->At = -1;
	state->Args = 0;
	state->Returns = 0;
	memset(state, 0, sizeof(alState));

	// Stack
	state->Stack = (alLiteral*)malloc(sizeof(alLiteral) * stackSize);
	if(!state->Stack) {
		fprintf(stderr, "couldn't alloc stack for state\n");
		alMemoryFree(state);
		return NULL;
	}
	memset(state->Stack, 0, sizeof(alLiteral) * tree->Main.Count);
	state->StackSize = 0;
	state->MaxStackSize = stackSize;

	// Global
	state->Global = (alLiteral*)malloc(sizeof(alLiteral) * tree->Count);
	if(!state->Global) {
		fprintf(stderr, "couldn't alloc global stack for state\n");
		free(state->Stack);
		free(state);
		return NULL;
	}
	memset(state->Global, 0, sizeof(alLiteral) * tree->Count);
	state->GlobalSize = tree->Count;

	// Ass Global
	state->AssGlobal = (int*)malloc(sizeof(int) * tree->AssCount);
	if(!state->AssGlobal) {
		fprintf(stderr, "couldn't alloc global ass stack for state\n");
		free(state->Global);
		free(state->Stack);
		alMemoryFree(state);
		return NULL;
	}
	memset(state->AssGlobal, 0, sizeof(int) * tree->AssCount);
	state->AssGlobalSize = tree->AssCount;

	// Oznacz jako g��wna ga���
	alMemorySetRoot(state);
	return state;
}

int alStateDestroy(alState *state) {
	alMemoryFree(state);
	return 0;
}

int alStateGetArgs(alState *state) {
	return state ? state->Args : -1;
}

int alStateGetReturns(alState *state) {
	return state ? state->Returns : -1;
}

alTable* alStateGetThis(alState *state) {
	return state ? state->This : NULL;
}

alStateStatus alStateGetStatus(alState *state) {
	return state ? state->Status : AL_STATUS_ERROR;
}

int alStateGetErrorFile(alState *state, alString file) {
	if(state && state->ErrorNode) {
		strncpy(file, state->ErrorNode->File, sizeof(alString));
		return 0;
	}
	return -1;
}

int alStateGetErrorLine(alState *state) {
	return state && state->ErrorNode ? state->ErrorNode->Line : -1;
}

int alStateGetErrorDef(alState *state, alString def) {
	if(state && state->ErrorDef) {
		strncpy(def, state->ErrorDef->Name, sizeof(alString));
		return 0;
	}
	return -1;
}

int alStateSetStatus(alState *state, alStateStatus status) {
	if(!state)
		return -1;
	state->Status = status;
	return 0;
}

int alStateGetGlobalLiteral(alState *state, alDef *def, alLiteral *value) {
	// Sprawd� dane
	if(!state || !def || !value)
		return -1;

	// Sprawd�, czy to napewno jest zmienna globalna
	if(def->Offset < 0 || def->Type != T_GLOBAL)
		return -1;

	// Pobierz warto��
	*value = state->Global[def->Offset];
	return 0;
}

int alStateSetGlobalLiteral(alState *state, alDef *def, alLiteral value) {
	// Sprawd� dane
	if(!state || !def)
		return -1;

	// Sprawd�, czy to napewno jest zmienna globalna
	if(def->Offset < 0 || def->Type != T_GLOBAL)
		return -1;

	// Ustaw warto��
	state->Global[def->Offset] = value;
	return 0;
}