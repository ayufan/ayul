/* A Bison parser, made by GNU Bison 2.1.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002, 2003, 2004, 2005 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     T_GLOBAL = 258,
     T_LOCAL = 259,
     T_TO = 260,
     T_COUNT = 261,
     T_THIS = 262,
     T_FOREACH = 263,
     T_IS = 264,
     T_AS = 265,
     T_ARGC = 266,
     T_ARGV = 267,
     T_IF = 268,
     T_ELSE = 269,
     T_WHILE = 270,
     T_DO = 271,
     T_FOR = 272,
     T_RETURN = 273,
     T_CONTINUE = 274,
     T_BREAK = 275,
     T_NULL = 276,
     T_BEGIN = 277,
     T_T_BEGIN = 278,
     T_END = 279,
     T_T_END = 280,
     T_OPEN = 281,
     T_CLOSE = 282,
     T_S_OPEN = 283,
     T_S_CLOSE = 284,
     T_STATEMENT = 285,
     T_ACCESSOR = 286,
     T_OR = 287,
     T_AND = 288,
     T_NEGATION = 289,
     T_GREATER = 290,
     T_LESS = 291,
     T_GREATER_EQUAL = 292,
     T_LESS_EQUAL = 293,
     T_NOT_EQUAL = 294,
     T_EQUALITY = 295,
     T_EQUAL = 296,
     T_POWER = 297,
     T_MODULUS = 298,
     T_DIVIDE = 299,
     T_MULTIPLICATION = 300,
     T_SUBTRACTION = 301,
     T_ADDITION = 302,
     T_OTHERWISE = 303,
     T_TEST = 304,
     T_SEPARATOR = 305,
     T_POWER_ASSIGNMENT = 306,
     T_MODULUS_ASSIGNMENT = 307,
     T_DIVIDE_ASSIGNMENT = 308,
     T_MULTIPLY_ASSIGNMENT = 309,
     T_SUBTRACTIVE_ASSIGNMENT = 310,
     T_ADDITIVE_ASSIGNMENT = 311,
     T_ASSIGNMENT = 312,
     T_INCREMENTATION = 313,
     T_DECREMENTATION = 314,
     T_IDENTIFIER = 315,
     T_INTEGER = 316,
     T_BOOLEAN = 317,
     T_FLOAT = 318,
     T_STRING = 319
   };
#endif
/* Tokens.  */
#define T_GLOBAL 258
#define T_LOCAL 259
#define T_TO 260
#define T_COUNT 261
#define T_THIS 262
#define T_FOREACH 263
#define T_IS 264
#define T_AS 265
#define T_ARGC 266
#define T_ARGV 267
#define T_IF 268
#define T_ELSE 269
#define T_WHILE 270
#define T_DO 271
#define T_FOR 272
#define T_RETURN 273
#define T_CONTINUE 274
#define T_BREAK 275
#define T_NULL 276
#define T_BEGIN 277
#define T_T_BEGIN 278
#define T_END 279
#define T_T_END 280
#define T_OPEN 281
#define T_CLOSE 282
#define T_S_OPEN 283
#define T_S_CLOSE 284
#define T_STATEMENT 285
#define T_ACCESSOR 286
#define T_OR 287
#define T_AND 288
#define T_NEGATION 289
#define T_GREATER 290
#define T_LESS 291
#define T_GREATER_EQUAL 292
#define T_LESS_EQUAL 293
#define T_NOT_EQUAL 294
#define T_EQUALITY 295
#define T_EQUAL 296
#define T_POWER 297
#define T_MODULUS 298
#define T_DIVIDE 299
#define T_MULTIPLICATION 300
#define T_SUBTRACTION 301
#define T_ADDITION 302
#define T_OTHERWISE 303
#define T_TEST 304
#define T_SEPARATOR 305
#define T_POWER_ASSIGNMENT 306
#define T_MODULUS_ASSIGNMENT 307
#define T_DIVIDE_ASSIGNMENT 308
#define T_MULTIPLY_ASSIGNMENT 309
#define T_SUBTRACTIVE_ASSIGNMENT 310
#define T_ADDITIVE_ASSIGNMENT 311
#define T_ASSIGNMENT 312
#define T_INCREMENTATION 313
#define T_DECREMENTATION 314
#define T_IDENTIFIER 315
#define T_INTEGER 316
#define T_BOOLEAN 317
#define T_FLOAT 318
#define T_STRING 319




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 35 "grammar.cxx"
typedef union YYSTYPE {
	int				i;
	float			f;
	char*			s;
	alString	id;

	alNode*		n;
	alDef*		d;

	alValue*	vn;
} YYSTYPE;
/* Line 1447 of yacc.c.  */
#line 178 "grammar.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



