//------------------------------------//
//
// stdafx.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-20
//
//------------------------------------//

#include "stdafx.h"

alNode*				alCurrentNodes[MAX_STACK];
alDef*				alCurrentDefs[MAX_STACK];
int						alCurrentNodeIndex;
int						alCurrentDefIndex;
alTree*				alCurrentTree = NULL;
alString			alCurrentFile;
int						alCurrentLine;
int						alCurrentAccessSpecifier;

