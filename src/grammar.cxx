%{
	
//------------------------------------//
//
// grammar.cxx
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-3-31
//
//------------------------------------//

#include "stdafx.h"

#ifdef _MSC_VER
// Treat some previous errors as warnings
#pragma warning(disable : 4244 4267)
#endif

#define YYERROR_VERBOSE
//#define YYDEBUG			1
#define YYPARSE_PARAM	data
#define alloca			malloc

#define alCurrentNode			alCurrentNodes[alCurrentNodeIndex]
#define alPushCurNode(x)	if(alCurrentNodeIndex < MAX_STACK){alCurrentNodes[++alCurrentNodeIndex] = (x);} else {yyerror("node stack overflow"); YYABORT;}
#define alPopCurNode			alCurrentNodes[alCurrentNodeIndex--]
#define alCurrentDef			alCurrentDefs[alCurrentDefIndex]
#define alPushCurDef(x)		if(alCurrentDefIndex < MAX_STACK){alCurrentDefs[++alCurrentDefIndex] = (x);} else {yyerror("def stack overflow"); YYABORT;}
#define alPopCurDef				alCurrentDefs[alCurrentDefIndex--]

%}

%union
{
	int				i;
	float			f;
	char*			s;
	alString	id;

	alNode*		n;
	alDef*		d;

	alValue*	vn;
};

%token					T_GLOBAL T_LOCAL T_TO T_COUNT T_THIS T_FOREACH T_IS T_AS T_ARGC T_ARGV T_IF T_ELSE T_WHILE T_DO T_FOR T_RETURN T_CONTINUE T_BREAK T_NULL
%token					T_BEGIN T_T_BEGIN T_END T_T_END T_OPEN T_CLOSE T_S_OPEN T_S_CLOSE T_STATEMENT T_ACCESSOR
%left 					T_EQUAL T_EQUALITY T_NOT_EQUAL T_LESS_EQUAL T_GREATER_EQUAL T_LESS T_GREATER T_NEGATION T_AND T_OR
%left						T_ADDITION T_SUBTRACTION T_MULTIPLICATION T_DIVIDE T_MODULUS T_POWER
%left						T_SEPARATOR T_TEST T_OTHERWISE
%right					T_ASSIGNMENT T_ADDITIVE_ASSIGNMENT T_SUBTRACTIVE_ASSIGNMENT T_MULTIPLY_ASSIGNMENT T_DIVIDE_ASSIGNMENT T_MODULUS_ASSIGNMENT T_POWER_ASSIGNMENT
%token					T_INCREMENTATION T_DECREMENTATION
%token	<id>		T_IDENTIFIER
%token	<i>			T_INTEGER T_BOOLEAN
%token	<f>			T_FLOAT
%token	<s>			T_STRING

%type		<n>			literal_value if_statement if_optional_else_statement statement return_statement while_statement do_while_statement for_statement foreach_statement
%type		<n>			expression assignment_expression compare_expression compare2_expression compare3_expression compare4_expression compare5_expression operator_expression operator2_expression operator3_expression base_expression declaration_expression optional_expression unary_expression postfix_expression definition definition_list table_expression
%type		<vn>		optional_table_elements table_elements table_element
%type		<id>		function_optional_name
%type		<i>			access_specifier

//bison configuration
%expect		4
%start		ayul

%%

ayul: /* NULL */ statement_list { }

literal_value:
	T_BOOLEAN {
		$$ = alNewLiteralNode(AL_LITERAL_BOOLEAN, &$1);
	}
	| T_INTEGER {
		$$ = alNewLiteralNode(AL_LITERAL_INTEGER, &$1);
	}
	| T_FLOAT {
		$$ = alNewLiteralNode(AL_LITERAL_VALUE, &$1);
	}
	| T_STRING {
 		$$ = alNewLiteralNode(AL_LITERAL_STRING, &$1);
	}
	| T_NULL {
		$$ = alNewLiteralNode(AL_LITERAL_AUTO, NULL);
	}
	| T_IDENTIFIER {
		$$ = alNewSymbolNode($1);
	}
	| T_THIS {
		$$ = alNewThisNode();
	}

base_expression:
	literal_value {
		$$ = $1;
	}
	| T_OPEN expression T_CLOSE {
		$$ = $2;
	}
	| base_expression T_OPEN optional_expression T_CLOSE {
		$$ = alNewCallNode($1, $3);
	}
	| base_expression T_ACCESSOR T_IDENTIFIER {
		char *id = strdup($3);
		$$ = alNewOfNode($1, alNewLiteralNode(AL_LITERAL_STRING, &id));
	}
	| base_expression T_S_OPEN optional_expression T_S_CLOSE {
		$$ = alNewOfNode($1, $3);
	}
	| table_expression {
		$$ = $1;
	}
	| T_ARGC {
		$$ = alNewArgcNode();
	}
	| T_ARGV T_S_OPEN expression T_S_CLOSE {
		$$ = alNewArgvNode($3, $3);
	}
	| T_ARGV T_S_OPEN optional_expression T_TO optional_expression T_S_CLOSE {
		$$ = alNewArgvNode($3, $5);
	}
	| T_IS base_expression {
		$$ = alNewIsNode($2);
	}
	| T_COUNT base_expression {
		$$ = alNewCountNode($2);
	}
	| T_CONTINUE {
		$$ = alNewContinueNode();
	}
	| T_BREAK {
		$$ = alNewBreakNode();
	}
	| if_statement {
		$$ = $1;
	}
	| return_statement {
		$$ = $1;
	}
	| while_statement {
		$$ = $1;
	}
	| do_while_statement {
		$$ = $1;
	}	
	| for_statement {
		$$ = $1;
	}
	| foreach_statement {
		$$ = $1;
	}

declaration_expression:
	definition {
		$$ = $1;
	}
	| base_expression {
		$$ = $1;
	}

postfix_expression:
	declaration_expression {
		$$ = $1;
	}
	| postfix_expression T_INCREMENTATION {
		$$ = alNewPostfixNode(T_INCREMENTATION, $1);
	}
	| postfix_expression T_DECREMENTATION {
		$$ = alNewPostfixNode(T_DECREMENTATION, $1);
	}

unary_expression:
	postfix_expression {
		$$ = $1;
	}
	| T_INCREMENTATION unary_expression {
		$$ = alNewUnaryNode(T_INCREMENTATION, $2);
	}
	| T_DECREMENTATION unary_expression {
		$$ = alNewUnaryNode(T_DECREMENTATION, $2);
	}
	| T_ADDITION unary_expression {
		$$ = alNewUnaryNode(T_ADDITION, $2);
	}
	| T_SUBTRACTION unary_expression {
		$$ = alNewUnaryNode(T_SUBTRACTION, $2);
	}

operator3_expression:
	unary_expression {
		$$ = $1;
	}
	| operator3_expression T_MODULUS operator3_expression {
		$$ = alNewOperatorNode(T_MODULUS, $1, $3);
	}
	| operator3_expression T_POWER operator3_expression {
		$$ = alNewOperatorNode(T_POWER, $1, $3);
	}

operator2_expression:
	operator3_expression {
		$$ = $1;
	}
	| operator2_expression T_MULTIPLICATION operator2_expression {
		$$ = alNewOperatorNode(T_MULTIPLICATION, $1, $3);
	}
	| operator2_expression T_DIVIDE operator2_expression {
		$$ = alNewOperatorNode(T_DIVIDE, $1, $3);
	}

operator_expression:
	operator2_expression {
		$$ = $1;
	}
	| operator_expression T_ADDITION operator_expression {
		$$ = alNewOperatorNode(T_ADDITION, $1, $3);
	}
	| operator_expression T_SUBTRACTION operator_expression {
		$$ = alNewOperatorNode(T_SUBTRACTION, $1, $3);
	}

compare5_expression:
	operator_expression {
		$$ = $1;
	}
	| compare5_expression T_EQUAL compare5_expression {
		$$ = alNewCompareNode(T_EQUAL, $1, $3);
	}
	| compare5_expression T_EQUALITY compare5_expression {
		$$ = alNewCompareNode(T_EQUALITY, $1, $3);
	}
	| compare5_expression T_NOT_EQUAL compare5_expression {
		$$ = alNewCompareNode(T_NOT_EQUAL, $1, $3);
	}
	| compare5_expression T_LESS_EQUAL compare5_expression {
		$$ = alNewCompareNode(T_LESS_EQUAL, $1, $3);
	}
	| compare5_expression T_GREATER_EQUAL compare5_expression {
		$$ = alNewCompareNode(T_GREATER_EQUAL, $1, $3);
	}
	| compare5_expression T_LESS compare5_expression {
		$$ = alNewCompareNode(T_LESS, $1, $3);
	}
	| compare5_expression T_GREATER compare5_expression {
		$$ = alNewCompareNode(T_GREATER, $1, $3);
	}

compare4_expression:
	compare5_expression {
		$$ = $1;
	}
	| T_NEGATION compare4_expression {
		$$ = alNewUnaryNode(T_NEGATION, $2);
	}

compare3_expression:
	compare4_expression {
		$$ = $1;
	}
	| compare3_expression T_AND compare3_expression {
		$$ = alNewCompareNode(T_AND, $1, $3);
	}

compare2_expression:
	compare3_expression {
		$$ = $1;
	}
	| compare2_expression T_OR compare2_expression {
		$$ = alNewCompareNode(T_OR, $1, $3);
	}

compare_expression:
	compare2_expression {
		$$ = $1;
	}
	| compare_expression T_TEST compare_expression T_OTHERWISE compare_expression {
		$$ = alNewIfNode();
		$$->If.Expr = $1;
		$$->If.True = $3;
		$$->If.False = $5;
	}

assignment_expression:
	compare_expression {
		$$ = $1;
	}
	| assignment_expression T_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_ASSIGNMENT, $1, $3);
	}
	| assignment_expression T_ADDITIVE_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_ADDITIVE_ASSIGNMENT, $1, $3);
	}
	| assignment_expression T_SUBTRACTIVE_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_SUBTRACTIVE_ASSIGNMENT, $1, $3);
	}
	| assignment_expression T_MULTIPLY_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_MULTIPLY_ASSIGNMENT, $1, $3);
	}
	| assignment_expression T_DIVIDE_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_DIVIDE_ASSIGNMENT, $1, $3);
	}
	| assignment_expression T_MODULUS_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_MODULUS_ASSIGNMENT, $1, $3);
	}
	| assignment_expression T_POWER_ASSIGNMENT assignment_expression {
		$$ = alNewAssignmentNode(T_POWER_ASSIGNMENT, $1, $3);
	}

expression:
	assignment_expression {
		$$ = $1;
	}
	| expression T_SEPARATOR expression {
		alAddAtEndOfNode($$ = $1, $3);
	}

function_argument_list:	
	function_argument_list T_SEPARATOR T_IDENTIFIER {
		alNewDefArgument(alCurrentDef, $3);
	}
	| T_IDENTIFIER {
		alNewDefArgument(alCurrentDef, $1);
	}

function_optional_argument_list:
	function_argument_list {
	}
	| /* NULL */ {
	}

function_optional_name:
	T_IDENTIFIER {
		strncpy($$, $1, sizeof($$));
	}
	| /* NULL */ {
		$$[0] = 0;
	}

access_specifier:
	T_LOCAL {
		$$ = alCurrentAccessSpecifier = T_LOCAL;
	}
	| T_GLOBAL {
		$$ = alCurrentAccessSpecifier = T_GLOBAL;
	}

definition_list:
	T_IDENTIFIER {
		$$ = alNewDefNode(alNewNodeDef(alCurrentNode, alCurrentAccessSpecifier, $1));
	}
	| definition_list T_SEPARATOR T_IDENTIFIER {
		alAddAtEndOfNode($$ = $1, alNewDefNode(alNewNodeDef(alCurrentNode, alCurrentAccessSpecifier, $3)));
	}

definition:
	access_specifier definition_list {
		$$ = $2;
	}
	| access_specifier definition_list T_ASSIGNMENT expression {
		$$ = alNewAssignmentNode($1, $2, $4);
	}
	| access_specifier function_optional_name {
		alPushCurDef($<d>$ = alNewNodeDef(alCurrentNode, $1, $2));
		alPushCurNode($<d>$->Code = alNewNullNode());
	}
	T_OPEN function_optional_argument_list T_CLOSE T_BEGIN statement_list T_END {
		assert(alPopCurDef == $<d>3);
		assert(alPopCurNode == $<d>3->Code);
		$$ = alNewDefNode($<d>3);
	}

table_element:
	assignment_expression {
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreate();
		$$->ValueNode = $1;
	}
	| T_BOOLEAN T_ASSIGNMENT assignment_expression {
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreateBoolean($1);
		$$->ValueNode = $3;
	}
	| T_INTEGER T_ASSIGNMENT assignment_expression {
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreateInteger($1);
		$$->ValueNode = $3;
	}
	| T_FLOAT T_ASSIGNMENT assignment_expression {
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreateValue($1);
		$$->ValueNode = $3;
	}
	| T_STRING T_ASSIGNMENT assignment_expression {
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreateString($1);
		$$->ValueNode = $3;
	}
	| T_IDENTIFIER T_ASSIGNMENT assignment_expression {
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreateString(strdup($1));
		$$->ValueNode = $3;
	}
	| access_specifier T_IDENTIFIER T_OPEN {
		alPushCurDef($<d>$ = alNewNodeDef(alCurrentNode, $1, NULL));
		alPushCurNode($<d>$->Code = alNewNullNode());
	} 
	function_optional_argument_list T_CLOSE T_BEGIN statement_list T_END {
		assert(alPopCurDef == $<d>4);
		assert(alPopCurNode == $<d>4->Code);
		$$ = malloc(sizeof(alValue));
		if(!$$)
			YYABORT;
		memset($$, 0, sizeof(alValue));
		$$->Key = alLiteralCreateString(strdup($2));
		$$->ValueNode = alNewDefNode($<d>4);
	}

table_elements:
	table_element table_elements {
		$$ = $1;
		$$->Next = $2;
		$2->Prev = $$;
	}
	| table_element {
		$$ = $1;
	}

optional_table_elements:
	table_elements {
		$$ = $1;
	}
	| /* NULL */ {
		$$ = NULL;
	}

table_expression:
	T_T_BEGIN { 
		alPushCurNode($$ = alNewTableNode());
	} 
	optional_table_elements T_T_END {
		assert(alPopCurNode == ($$ = $<n>2));
		$$->Table.Values = $3;
	}

if_optional_else_statement: 
	T_ELSE statement {
		$$ = $2;
	}
	| /* NULL */ {
		$$ = NULL;
	}

if_statement: 
	T_IF { 
		$$ = alNewNullNode();
		alPushCurNode($$->Null.Expr = alNewIfNode());
	} 
	assignment_expression statement if_optional_else_statement {
		$$ = $<n>2;
		assert(alPopCurNode == $$->Null.Expr);
		$$->Null.Expr->If.Expr = $3;
		$$->Null.Expr->If.True = alNewEndNode($4);
		$$->Null.Expr->If.False = alNewEndNode($5);
	}

while_statement:
	T_WHILE T_OPEN { 
		$$ = alNewNullNode();
		alPushCurNode($$->Null.Expr = alNewWhileNode());
	} 
	expression T_CLOSE statement {
		$$ = $<n>3;
		assert(alPopCurNode == $$->Null.Expr);
		$$->Null.Expr->While.Expr = $4;
		$$->Null.Expr->While.Loop = alNewEndNode($6);
	}

do_while_statement:
	T_DO statement T_WHILE T_OPEN { 
		$$ = alNewNullNode();
		alPushCurNode($$->Null.Expr = alNewDoWhileNode());
	} 
	expression T_CLOSE {
		$$ = $<n>5;
		assert(alPopCurNode == $$->Null.Expr);
		$$->Null.Expr->DoWhile.Loop = alNewEndNode($2);
		$$->Null.Expr->DoWhile.Expr = $6;
	}

for_statement:
	T_FOR T_OPEN  { 
		$$ = alNewNullNode();
		alPushCurNode($$->Null.Expr = alNewForNode());
	} 
	optional_expression T_STATEMENT expression T_STATEMENT optional_expression T_CLOSE statement {
		$$ = $<n>3;
		assert(alPopCurNode == $$->Null.Expr);
		$$->Null.Expr->For.Init = alNewEndNode($4);
		$$->Null.Expr->For.Expr = $6;
		$$->Null.Expr->For.Next = alNewEndNode($8);
		$$->Null.Expr->For.Loop = alNewEndNode($10);
	}

foreach_statement:
	T_FOREACH T_OPEN { 
		$$ = alNewNullNode();
		alPushCurNode($$->Null.Expr = alNewForEachNode());
	} 
	expression T_AS expression T_CLOSE statement {
		$$ = $<n>3;
		assert(alPopCurNode == $$->Null.Expr);
		$$->Null.Expr->ForEach.Table = $4;
		$$->Null.Expr->ForEach.As = $6;
		$$->Null.Expr->ForEach.Loop = alNewEndNode($8);
	}

optional_expression:
	expression {
		$$ = $1;
	}
	| /* NULL */ {
		$$ = NULL;
	}

return_statement:
	T_RETURN T_STATEMENT {
		$$ = alNewReturnNode(NULL);
	}
	| T_RETURN expression {
		$$ = alNewReturnNode($2);
	}

statement:
	expression {
		$$ = alNewEndNode($1);
	}
	| T_BEGIN { 
		$$ = alNewNullNode(); 
		alPushCurNode($$->Null.Expr = alNewNullNode());
	}
	statement_list T_END {
		assert(alPopCurNode == ($$ = $<n>2)->Null.Expr);
	}
	| T_STATEMENT {
		$$ = NULL;
	}

statement_list:
	statement_list statement {
		alAddAtEndOfNode(alCurrentNode, $2);
	}
	| /* NULL */ {
	}

%%
