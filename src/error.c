//------------------------------------//
//
// error.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-20
//
//------------------------------------//

#include "stdafx.h"

char *yyfile() {
	return alCurrentFile;
}	

int yyline() {
	return alCurrentLine;
}

void yyerror(char *message) {
	fprintf(stderr, "%s(%i): %s\n", yyfile(), yyline(), message);
}

int yywrap() {
	return 1;
}
