//------------------------------------//
//
// memory.c
//
// Author: ayufan (ayufan[at]o2.pl)
// Project: ayul
// Date: 2006-4-17
//
//------------------------------------//

#include "stdafx.h"

static alData		Data = {AL_MAGIC1, &Data, AL_MAGIC2, &Data, 0};
static alData*	DataStack[2][MAX_STACK];
static int			DataStackSize[2];
static int			DataStackIndex = 0;

alData *alMemoryGetData(void *buf) {
	alData*	data;

	// Sprawd�, czy podane zosta�y jakie� dane
	if(!buf)
		return NULL;

	// Pobierz adres struktury
	data = &((alData*)buf)[-1];

	// Sprawd�, czy to jest nasza "magiczna" struktura
#ifdef WIN32
	__try {
		if(data->Magic1 == AL_MAGIC1 && data->Magic2 == AL_MAGIC2)
			return data;
	}
	__except(1) {
	}
#else
	if(data->Magic1 == AL_MAGIC1 && data->Magic2 == AL_MAGIC2)
		return data;
#endif
	return NULL;
}

void *alMemoryAlloc(void *buf, int size, alCollect collect, alFree free) {
	alData *data;

	if(size <= 0)
		return NULL;

	// Pobierz dane
	if(data = alMemoryGetData(buf)) {
		// Wypnij dane z listy
		data->Prev->Next = data->Next;
		data->Next->Prev = data->Prev;
	}
	else if(buf) {
		fprintf(stderr, "invalid data passed to memory alloc function !!\n");
	}

	// Utw�rz nowe dane
	data = (alData*)realloc(data, sizeof(alData) + size);
	if(!data)
		return NULL;

	// Zapisz informacje o alokacji
	data->Magic1 = AL_MAGIC1;
	data->Magic2 = AL_MAGIC2;
	data->Collect = collect;
	data->Free = free;

	// Wepnij do listy
	data->Next = Data.Next;
	data->Prev = &Data;
	Data.Next->Prev = data;
	Data.Next = data;

	// Pobierz fizyczny adres danych
	return &data[1];
}

int alMemoryFree(void *buf) {
	alData *data;

	// Pobierz dane
	if(data = alMemoryGetData(buf)) {
		// Wypnij dane z listy
		data->Prev->Next = data->Next;
		data->Next->Prev = data->Prev;

		// Zniszcz dane
		free(data);
		return 0;
	}
	else if(buf) {
		fprintf(stderr, "invalid data passed to memory free function !!\n");
	}
	return -1;
}

alMark alMemoryGetMark(void *buf) {
	alData *data;

	if(data = alMemoryGetData(buf)) {
		// Pobierz oznaczenie przypisane do bufora
		return data->Mark;
	}
	return -1;
}

int alMemorySetNone(void *buf) {
	alData *data;

	if(data = alMemoryGetData(buf)) {
		// Oznacz jako korze�
		data->Mark = AL_MARK_NONE;
		return 0;
	}
	return -1;
}

int alMemorySetUsed(void *buf) {
	alData *data;

	if(data = alMemoryGetData(buf)) {
		// Oznacz jako korze�
		data->Mark = AL_MARK_USED;
		return 0;
	}
	return -1;
}

int alMemorySetRoot(void *buf) {
	alData *data;

	if(data = alMemoryGetData(buf)) {
		// Oznacz jako korze�
		data->Mark = AL_MARK_ROOT;
		return 0;
	}
	return -1;
}

int alMemoryMarkAsUsed(void *buf) {
	alData *data;

	if(data = alMemoryGetData(buf)) {
		if(data->Mark == AL_MARK_NONE) {
			// Oznacz jako u�yty
			data->Mark = AL_MARK_USED;

			// Dodajmy w�ze� do listy
			if(DataStackSize[DataStackIndex] < MAX_STACK) {
				DataStack[DataStackIndex][DataStackSize[DataStackIndex]++] = data;
			}	
		}
		return 0;
	}
	return -1;
}

int alMemoryCollect() {
	alData *itor, *next;
	int i, count = 0;

	// Iteruj przez wszystkie bloki oznaczaj�c je jako nieu�yte
	for(itor = Data.Next; itor != &Data; itor = itor->Next) {
		if(itor->Mark != AL_MARK_ROOT) {
			itor->Mark = AL_MARK_NONE;
		}
	}

	// Wyczy�� zawarto�� stosu
	DataStackIndex = 0;
	DataStackSize[DataStackIndex] = 0;

	// W pierwszej kolejno�ci oznacz wszystkie w�z�y g��wne
	for(itor = Data.Next; itor != &Data; itor = itor->Next) {
		if(itor->Mark == AL_MARK_NONE)
			continue;

		// Wywo�aj sprawdzanie tego co znajduje si� w w�zle
		if(itor->Collect) 
			itor->Collect(&itor[1]);

		// U�yty w�ze� oznacz jako sprawdzony, bo "korze�" napewno jest sprawdzony
		if(itor->Mark == AL_MARK_USED)
			itor->Mark = AL_MARK_CHECKED;
	}

	// Sprawdzamy dopuki wszystkiego nie sprawdzimy
	while(DataStackSize[DataStackIndex] != 0) {
		// Wyczy�� zawarto�� stosu
		DataStackIndex = !DataStackIndex;
		DataStackSize[DataStackIndex] = 0;

		// Sprawdzamy to co wcze�niej
		for(i = 0; i < DataStackSize[!DataStackIndex]; i++) {
			itor = DataStack[!DataStackIndex][i];

			// Wywo�aj sprawdzanie tego co znajduje si� w w�zle
			if(itor->Collect) 
				itor->Collect(&itor[1]);

			// Oznacz w�ze� jako sprawdzony
			itor->Mark = AL_MARK_USED;
		}
	}

	// Iteruj przez wszystkie bloki usuwaj�c nieu�ywane bloki
	for(itor = Data.Next; itor != &Data; itor = next) {
		// Pobierz nast�pnik
		next = itor->Next;

		// Sprawd�, czy blok jest u�ywany
		if(itor->Mark != AL_MARK_NONE)
			continue;

		// Usu� blok
		if(itor->Free) 
			itor->Free(&itor[1]);
		else
			alMemoryFree(&itor[1]);
		count++;
	}
	return count;
}

char *alMemoryStrDup(const char *text) {
	// Sprawd� dane wej�ciowe
	if(!text)
		return NULL;

	// Utw�rz obszar pami�ci i skopiuj dane
	return strcpy((char*)alMemoryAlloc(NULL, (int)strlen(text) + 1, NULL, NULL), text);
}

int alMemoryCount() {
	alData *itor;
	int count = 0;

	// Iteruj przez wszystkie bloki zliczaj�c je
	for(itor = Data.Next; itor != &Data; itor = itor->Next) {
		count++;
	}
	return count;
}
